$(document).ready(function () {
  // username
  var validateUsername = $('#validateUsername');
  $('#username').blur(function () {
    $('#validateUsername').removeClass('available').removeClass('error').addClass('thinking').text('');
    $.ajax({
      type: "POST",
      url: "/ajax/username_taken",
      data: "username="+$("#username").val(),
      success: function(msg){
          if(msg=="true")
          {
              $("#validateUsername")
                .removeClass('thinking')
                .addClass('available');
          }
          else if(msg=="tooshort")
          {
              $("#validateUsername")
                .removeClass('thinking')
                .addClass('error')
                .text('Username must be at least 4 characters');
          }
          else if(msg=="chars")
          {
              $("#validateUsername")
                .removeClass('thinking')
                .addClass('error')
                .text('Username can only contain alphanumeric and underscore');
          }
          else if(msg=="taken")
          {
              $("#validateUsername")
                .removeClass('thinking')
                .addClass('error')
                .text('Username is already taken');
          }
          else
          {
              $("#validateUsername")
                .removeClass('thinking')
                .addClass('error');
          }
      }
    });
  });
  
  // email
  var validateEmail = $('#validateEmail');
  $('#email').blur(function () {
    $('#validateEmail').removeClass('available').removeClass('error').addClass('thinking').text('');
    $.ajax({
      type: "POST",
      url: "/ajax/email_validate",
      data: "email="+$("#email").val(),
      success: function(msg){
          if(msg=="true")
          {
              $("#validateEmail")
                .removeClass('thinking')
                .addClass('available');
          }
          else if(msg=="taken")
          {
              $("#validateEmail")
                .removeClass('thinking')
                .addClass('error')
                .text('This email address is already taken');
          }
          else
          {
              $("#validateEmail")
                .removeClass('thinking')
                .addClass('error')
                .text('Must be a valid email address');
          }
      }
    });
  });
  
  // password validate
  var validatePass = $('#validatePass');
  $('#password').blur(function () {
    $('#validatePass').removeClass('available').removeClass('error').addClass('thinking').text('');
    $.ajax({
      type: "POST",
      url: "/ajax/password_validate",
      data: "pass="+$("#password").val(),
      success: function(msg){
          if(msg=="true")
          {
              $("#validatePass")
                .removeClass('thinking')
                .addClass('available');
          }
          else
          {
              $("#validatePass")
                .removeClass('thinking')
                .addClass('error')
                .text('Password must be 4-20 characters long');
          }
      }
    });
  });
  
  // password match
  var validatePassword = $('#validatePassword');
  $('#confirm_password').blur(function() {
      $('#validatePassword').removeClass('available').removeClass('error').addClass('thinking').text('');
      password = $('#password').val();
      passconfirm = $('#confirm_password').val();

      if(password == passconfirm)
      {
          $('#validatePassword').removeClass('thinking').addClass('available');
      }
      else
      {
          $('#validatePassword')
            .removeClass('thinking')
            .addClass('error')
            .text('Password does not match');
      }
  });
});