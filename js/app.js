$(document).ready(function() {

    // nav menu
    $('.user-dropdown').click(function() {
        $('#usernav').toggleClass('open');
    });

    // notifications
    $('.top-notify').slideDown(200).delay(4000).slideUp(200);
    $('.alert-box .close').click(function(e) {
        e.preventDefault();
        $(this).parent('.alert-box').slideUp(200);
    });

    // search animation
    $('.search-toggle').click(function(e) {
        $('.topbar-search').addClass('active');
        $('.search-input').focus();
        e.preventDefault();
    });
    $('.search-close').click(function(e) {
        $('.topbar-search').removeClass('active');
        $('.search-input').focusout();
        e.preventDefault();
    });

    // beer description
    $('.beer-desc-more').click(function(e) {
        if ( $(this).html() == 'Show more' ) {
            $(this).html(' Show less');
        } else {
            $(this).html('Show more');
        }
        $(this).siblings('.ellipsis').toggle();
        $(this).siblings('.full-desc').toggleClass('hide');
        e.preventDefault();
    });

    // tab notes
    $('.tab-list').on('click', '.show-notes', function() {
        var notesContainer = $(this).parents('.tab-item').find('.notes-container');
        var notes = '';
        if ( notesContainer.find( '.notes span' ).text() != 'No notes for this beer.' ) {
            notes = notesContainer.find('.notes span').text();
        }
        ga('send', 'event', 'Tab Item', 'Show notes', notes);
        $('.notes-container').removeClass('open');
        $(this).parents('.tab-item').find('.notes-container').addClass('open');
    });
    $('.tab-list').on('click', '.close-notes', function() {
        $('.notes-container').removeClass('open');
    });

    // show auth modals
    var urlGet = location.search.replace('?', '').split('=');
    if ( urlGet[0] === "auth" ) {
        if ( urlGet[1] === "signup" ) {
            $('#signup-modal').foundation('reveal', 'open', '/auth/signup');
        }
        else if ( urlGet[1] === "fuser" ) {
            $('#signup-modal').foundation('reveal', 'open', '/auth/set_username');
        }
        else {
            $('#login-modal').foundation('reveal', 'open', '/auth/login');
        }
    }

    // logout
    $('#nav-logout').on('click', function(e) {
        e.preventDefault();

        var logoutUrl = window.location.origin+'/logout';
        window.location.href = logoutUrl;
    });

		// donation banner
		// if ( ! $('.upsell.global').hasClass( 'hide' ) ) {
		// 	var bannerHeight = $('.upsell.global' ).outerHeight();
		// 	$( '.topbar' ).css('top', bannerHeight);
		// 	$( 'body' ).css( 'padding-top', bannerHeight + 84 );
		// }

		$( '.upsell.global .close' ).click( function() {
			$( '.topbar' ).animate( { top: 0 }, 200);
			$( 'body' ).animate( { paddingTop: 84 }, 200 );
		});

});

// Add a beer to your tab
var autocompleteTimer = null;
function styleAutocomplete( input ) {
	var currentVal = $(input).val();
	var styles = null;
	$.getJSON("/js/components/beerStyles.json", function(data) {
		styles = data.data;

		var searchOptions = {
			keys: ['name', 'shortName']
		}
		var fuse = new Fuse(styles, searchOptions);
		var results = fuse.search( currentVal );
		var autocomplete = [];

		for(var i = 0; i < results.length; i++) {
			var template = '<div class="autocomplete-opt-container"><a class="autocomplete-option" href="" data-id="'+results[i].id+'">'+results[i].name+'</a></div>';
			autocomplete.push(template);
		}

		if (autocomplete.length > 0) {
			$('#style_autocomplete .autocomplete-opt-container').remove();

			$.each( autocomplete, function(i, val) {
				$('#style_autocomplete').append(val).fadeIn(200);
			});
		}
	});
}

$(document)
.on('keyup', '#submit_style', function() {
	var input = $(this);
	styleAutocomplete( input );
})
.on('blur', '#submit_style', function() {
  $('#style_autocomplete').fadeOut(200);
})
.on('click', '#style-autocomplete-container .autocomplete-option', function(e) {
  e.preventDefault();
  var acValue = $(this).text();
  var acId = $(this).data('id');

  $('#submit_style').val(acValue);
  $('#submit_style_id').val(acId);

  $('#submit-beer-form .autocomplete').html('').fadeOut(200);
});

// remove from tab
$('.remove-from-tab').on('click', function() {
    var beerId = $(this).data('beerid');
    var addParams = $(this).data('params');
    var postData = {
        beer_id: beerId
    }

    $.ajax({
      type: "POST",
      url: "/user/remove_from_tab",
      data: postData,
      success: function(response){
        if (response == 'success') {
            // notification
            var notification = '<div class="alert-box success top-notify"><span class="icon icon-check"></span><span class="text">Successfully removed from your tab!</span></div>';

            // change "remove" to "add"
            $('.remove-from-tab[data-beerId="'+beerId+'"]').attr({'href': '/user/add_form?'+addParams, 'data-reveal-id': 'add-modal', 'data-reveal-ajax': 'true'}).addClass('add-to-tab').removeClass('outline remove-from-tab').html('Add to Tab');
        }
        else {
            // notification
            var notification = '<div class="alert-box error top-notify"><span class="icon icon-close"></span><span class="text">'+response+'</span></div>';
        }

        $('body').append(notification);
        setTimeout(function() {
            $('.top-notify').delay(4000).slideUp(200);
        });
      },
      error: function() {
        var notification = '<div class="alert-box error">Hmm, this couldn\'t be removed. Please refresh the page and try again.</div>';

        $('body').append(notification);
        setTimeout(function() {
            $('.top-notify').delay(4000).slideUp(200);
        });
      }
    });
});

$(document).on('click', '#fab', function(e) {
	ga('send', 'event', 'Add Beer', 'Click FAB');
});
