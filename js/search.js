$(document).ready(function() {

  $('.show-more').click(function() {
    var searchPage = $(this).data('page');
    var searchQuery = $(this).data('query');
    var searchTotal = $(this).data('pages');

    // hide button, show spinner, increase page
    searchPage++;
    $(this).hide().attr('data-page', searchPage);
    $('.cssload-container').show();

    // search more
    $.ajax({
      type: "GET",
      url: "/search/search_more",
      data: "q="+searchQuery+'&p='+searchPage,
      success: function(html){
        // show button only if more results possible
        if ( searchPage < searchTotal ) {
          $('.cssload-container').hide();
          $('.search_more').show();
        } else {
          $('.cssload-container, .search_more').detach();
        }

        // append search results
        $('#submit-beer').before(html);
      }
    });
  });

});

var autocompleteTimer = null;
function breweryAutocomplete( input ) {
	$('#submit_brewery_id').val('');
	var currentVal = $(input).val();

	if ( currentVal != '' && currentVal.length > 4 ) {
    var data = 'q='+currentVal;

    $.ajax({
      type: "GET",
      url: "/autocomplete/brewery",
      data: data,
      success: function(response){
        var res = JSON.parse(response);
        var autocomplete = [];

        for(var i = 0; i < res.length; i++) {
          var template = '<div class="autocomplete-opt-container"><a class="autocomplete-option" href="" data-id="'+res[i].id+'">'+res[i].name+'</a></div>';
          autocomplete.push(template);
        }

        if (autocomplete.length > 0) {
          $('#brewery_autocomplete .autocomplete-opt-container').remove();

          $.each( autocomplete, function(i, val) {
            $('#brewery_autocomplete').append(val).fadeIn(200);
          });
        }
      }
    });
  } else {
		$('#brewery_autocomplete .autocomplete-opt-container').remove();
	}
}

function styleAutocomplete( input ) {
	var currentVal = $(input).val();
	var styles = null;
	$.getJSON("/js/components/beerStyles.json", function(data) {
		styles = data.data;

		var searchOptions = {
			keys: ['name', 'shortName']
		}
		var fuse = new Fuse(styles, searchOptions);
		var results = fuse.search( currentVal );
		var autocomplete = [];

		for(var i = 0; i < results.length; i++) {
			var template = '<div class="autocomplete-opt-container"><a class="autocomplete-option" href="" data-id="'+results[i].id+'">'+results[i].name+'</a></div>';
			autocomplete.push(template);
		}

		if (autocomplete.length > 0) {
			$('#style_autocomplete .autocomplete-opt-container').remove();

			$.each( autocomplete, function(i, val) {
				$('#style_autocomplete').append(val).fadeIn(200);
			});
		}
	});
}

$(document)
.on('keyup', '#submit_style', function() {
	var input = $(this);
	styleAutocomplete( input );
})
.on('blur', '#submit_style', function() {
  $('#style_autocomplete').fadeOut(200);
})
.on('click', '#style-autocomplete-container .autocomplete-option', function(e) {
  e.preventDefault();
  var acValue = $(this).text();
  var acId = $(this).data('id');

  $('#submit_style').val(acValue);
  $('#submit_style_id').val(acId);

  $('#submit-beer-form .autocomplete').html('').fadeOut(200);
});

$(document)
.on('keyup', '#submit_brewery', function() {
	var input = $(this);

	if ( autocompleteTimer ) {
		window.clearTimeout( autocompleteTimer );
	}
	autocompleteTimer = window.setTimeout( function() {
			autocompleteTimer = null;
			breweryAutocomplete( input );
	}, 300 );
})

.on('blur', '#submit_brewery', function() {
  $('#brewery_autocomplete').fadeOut(200);
})

.on('click', '#brewery-autocomplete-container .autocomplete-option', function(e) {
  e.preventDefault();
  var acValue = $(this).text();
  var acId = $(this).data('id');

  $('#submit_brewery').val(acValue);
  $('#submit_brewery_id').val(acId);

  $('#submit-beer-form .autocomplete').html('').fadeOut(200);
});
