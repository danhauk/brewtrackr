// upload avatar
$('#avatar-form').on('submit', function(e) {

  e.preventDefault();
  var formData = new FormData(this);
  var username = $('#username').val();

	ga('send', 'event', 'Account', 'Update avatar', username);

  $.ajax({
    type:'POST',
    url: $(this).attr('action'),
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    success:function(response){
      if ( response.indexOf( username ) > -1 ) {
        // show success notification
        var notificationText = 'Looking good! Your photo has been updated.';
        showNotification( notificationText, 'success' );

        // show new image
        $('.settings-avatar').css('background-image', 'url('+response+')');
        $('.topbar .avatar').attr('src', response);
        $('.cssload-container').hide();
      }
      else {
        // show error notification
        showNotification( response, 'error' );
      }

      // hide spinner
      $('.cssload-container').hide();
    },
    error: function(data){
      console.log("error");
      console.log(data);
    }
  });
});

$('input#avatar').change(function() {
  $('.cssload-container').show();

  // validate size before trying to upload
  var filesize = this.files[0].size;

  if ( filesize > 8388608 ) {
    // file too big, show error notification
    var notification = 'Your file must be less than 8MB.';
    showNotification(notification, 'error');

    // hide spinner
    $('.cssload-container').hide();
  } else {
    $('#avatar-form').submit();
  }
});

function showNotification(notification, success) {
  var topNotify;

  if ( success === 'success' ) {
    topNotify = '<div class="alert-box success top-notify"><span class="icon icon-check"></span><span class="text">' + notification + '</span></div>';
  } else {
    topNotify = '<div class="alert-box error top-notify"><span class="icon icon-close"></span><span class="text">' + notification + '</span></div>'
  }

  // append notification
  $('body').append(topNotify);
  $('.top-notify').delay(4000).slideUp(200)
  setTimeout(function() {
    $('.top-notify').remove();
  }, 4500);
}

// update other settings
$('#settings-form').on('submit', function(e) {
  e.preventDefault();

  $(this).find('input[type="submit"]').attr('disabled', 'disabled');
  $('#email').removeClass('error');

  var formData = {
    email: $('#email').val(),
    bio: $('#bio').val()
  }

  $.ajax({
    type:'POST',
    url: $(this).attr('action'),
    data: formData,
    success:function(response){
      if ( response == 'email taken' ) {
        var notification = '<div class="alert-box error top-notify"><span class="icon icon-close"></span><span class="text">That email address is already registered.</span></div>';

        $('#email').addClass('error');

        // append notification
        $('body').append(notification);
        $('.top-notify').delay(4000).slideUp(200)
        setTimeout(function() {
          $('.top-notify').remove();
        }, 4500);
      }
      else if ( response != 'success' ) {
        var notification = '<div class="alert-box error top-notify"><span class="icon icon-close"></span><span class="text">' + response + '</span></div>';

        $('#email').addClass('error');

        // append notification
        $('body').append(notification);
        $('.top-notify').delay(4000).slideUp(200)
        setTimeout(function() {
          $('.top-notify').remove();
        }, 4500);
      }
      else {
        var notification = '<div class="alert-box success top-notify"><span class="icon icon-check"></span><span class="text">Profile updated!</span></div>';

        // append notification
        $('body').append(notification);
        $('.top-notify').delay(4000).slideUp(200)
        setTimeout(function() {
          $('.top-notify').remove();
        }, 4500);
      }

      // enable button
      $('#settings-form input[type="submit"]').removeAttr('disabled');
    }
  });
});

// change password
$('#password-form').on('submit', function(e) {
  e.preventDefault();

  $(this).find('input[type="submit"]').attr('disabled', 'disabled');
  $("#password-errors").remove();

  var formData = {
    old: $('#old').val(),
    new: $('#new').val(),
    new_confirm: $('#new_confirm').val()
  }

  $.ajax({
    type:'POST',
    url: $(this).attr('action'),
    data: formData,
    success:function(response){
      if ( response != 'success' ) {
        var notification = '<div class="alert-box error" id="password-errors">'+response+'</div>';

        // append notification
        $(notification).insertBefore('#password-form');

        // scroll to password form
        var passwordPos = $('#password').offset().top;
        var scrollTo = passwordPos-($('.topbar').outerHeight()+10) + 'px';
        $("html, body").animate({ scrollTop: scrollTo }, 500);
      }
      else {
        var notification = '<div class="alert-box success top-notify"><span class="icon icon-check"></span><span class="text">Password changed!</span></div>';

        // append notification
        $('body').append(notification);
        $('.top-notify').delay(4000).slideUp(200)
        setTimeout(function() {
          $('.top-notify').remove();
        }, 4500);
      }

      // enable button & clear inputs
      $("#password-form input[type='password']").val("");
      $('#password-form input[type="submit"]').removeAttr('disabled');
    }
  });
});
