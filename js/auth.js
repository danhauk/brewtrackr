// signup form ajax
$('#register-form').on('submit', function(e) {
  // Stop form from submitting normally
  e.preventDefault();

	var validate = formValidate( $(this) );
	if ( validate.error != undefined ) {
		// not valid input
		$('#signup-submit').attr('disabled', 'disabled');
	  $('#auth-error').html( validate.error.message ).fadeIn(200);
	} else {
		// valid input, let's submit the form
		// disable button and hide any errors
	  $('#signup-submit').attr('disabled', 'disabled');
	  $('#auth-error').html("").fadeOut(200);

	  var formAction = $(this).attr('action');
	  var username = $(this).find('#username').val();
	  var email = $(this).find('#email').val();
	  var password = $(this).find('#password').val();
	  var formData = {
	    username: username,
	    email: email,
	    password: password
	  };

	  $.ajax({
	    type: "POST",
	    url: formAction,
	    data: formData,
	    success: function(response){
	      var res = JSON.parse(response);
	      var resArray = $.makeArray(res);

	      if ( Object.keys(resArray[0]) != 'success' ) {
	        $("#auth-error").html(res.error.message).fadeIn(200);

	        $('#signup-submit').removeAttr('disabled');
	      }
	      else {
	        ga('send', 'event', 'Account', 'Create', username);

	        setTimeout(function() {
	          location.reload();
	        }, 500);
	      }
	    }
	  });
	}
});

// login form ajax
$('#login-form').on('submit', function(e) {
  // Stop form from submitting normally
  e.preventDefault();

  // disable button and hide any errors
  $('#login-submit').attr('disabled', 'disabled');
  $('#auth-error').html("").fadeOut(200);

  var formAction = $(this).attr('action');
  var identity = $(this).find('#identity').val();
  var password = $(this).find('#password').val();
  var remember = $(this).find('#remember:checked').length;
  var formData = {
    identity: identity,
    password: password,
    remember: remember
  };

  $.ajax({
    type: "POST",
    url: formAction,
    data: formData,
    success: function(response){
      var res = JSON.parse(response);
      var resArray = $.makeArray(res);

      if ( res.error != undefined ) {
        $("#auth-error").html(res.error).fadeIn(200);

        $('#login-submit').removeAttr('disabled');
      }
      else {
        location.reload();
      }
    }
  });
});

// check form has values
function formValidate( form ) {
	var valid;
	form.find('input').each( function() {
		if ( this.value == null || this.value == '' ) {
			valid = {
				error: {
					message: 'Looks like you forgot something.'
				}
			};
			return;
		}
	} );

	if ( valid == null ) {
		// all fields are entered, check email
		var email = form.find('#email').val();
		var reg = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,7})+$/;
		if ( reg.test(email) ) {
			valid = { success: true };
		} else {
			valid = {
				error: {
					message: 'I think you missed something in your email address.'
				}
			};
		}
	}

	return valid;
}

// check username
$('#username-form #username, #register-form #username').on('keyup', function(e) {
	var username = $(this).val();

	setTimeout( function() {
		$.ajax({
			type: "POST",
			url: "/auth/check_username",
			data: { username: username },
			success: function( response ) {
				var res = JSON.parse( response );
				if ( res.error != undefined ) {
					$("#auth-error").html(res.error.message).fadeIn(200);
					$('#username-submit, #signup-submit').attr('disabled', 'disabled');
				} else {
					$("#auth-error").html('').fadeOut(200);
					$('#username-submit, #signup-submit').removeAttr('disabled');
				}
			}
		});
	}, 300 );
});

// check email address
$('#register-form #email').on('keyup', function(e) {
	var email = $(this).val();

	setTimeout( function() {
		$.ajax({
			type: "POST",
			url: "/auth/check_email",
			data: { email: email },
			success: function( response ) {
				var res = JSON.parse( response );
				if ( res.error != undefined ) {
					$("#auth-error").html(res.error.message).fadeIn(200);
					$('#signup-submit').attr('disabled', 'disabled');
				} else {
					$("#auth-error").html('').fadeOut(200);
					$('#signup-submit').removeAttr('disabled');
				}
			}
		});
	}, 300 );
});

// set username form ajax
$('#username-form').on('submit', function(e) {
  // Stop form from submitting normally
  e.preventDefault();

  // disable button and hide any errors
  $('#username-submit').attr('disabled', 'disabled').val('Saving username...');
  $('#auth-error').html("").fadeOut(200);

  var formAction = $(this).attr('action');
  var formData = {
    username: $(this).find('#username').val(),
		facebook_id: $(this).find('#facebook_id').val(),
  };

  $.ajax({
    type: "POST",
    url: formAction,
    data: formData,
    success: function(response){
      var res = JSON.parse(response);
      if ( res.error != undefined ) {
        $("#auth-error").html(res.error.message).fadeIn(200);

        $('#username-submit').removeAttr('disabled').val('Save Username');
      }
      else {
        location.reload();
      }
    }
  });
});
