$(document).ready(function() {

	// photo upload
	$('input#addphoto').change(function() {
	  // validate size before trying to upload
		var file = this.files[0];
	  var filesize = file.size;
		var reader = new FileReader();
		var imageData = '';

		ga('send', 'event', 'Photo Upload', 'File added');

	  if ( filesize > 8388608 ) {
	    // file too big, show error notification
	    var error = 'Your file must be less than 8MB.';
	    $( '#photo-upload-error' ).text( error ).fadeIn( 200 );
			ga('send', 'event', 'Photo Upload', 'Error', error);
	  } else {
			reader.onload = function() {
				imageData = reader.result;
				$( '.photo-input' ).hide();
				$( '.photo-preview' ).css( {backgroundImage: 'url('+imageData+')'} ).show();
				ga('send', 'event', 'Photo Upload', 'Success');
			}
			reader.readAsDataURL(file);
	  }
	});

	$('.photo-preview').not('.no-remove').on( 'click', function(e) {
		e.preventDefault();
		$( '#addphoto' ).val('').clone(true);
		$(this).removeAttr('style').hide();
		$( '.photo-input' ).show();
	});

  var moreCurrentPage = $('.show-more').data('page');

  $('.show-more').click(function() {
    var moreButton = $(this);
    var totalPages = moreButton.data('pages');
    var username = moreButton.data('user');

		ga('send', 'event', 'Tab', 'Show More', 'Page ' + moreCurrentPage);

    // hide button, show spinner, increase page number
    moreCurrentPage++;
    moreButton.detach().attr('data-page', moreCurrentPage);
    $('.cssload-container').show();

    // show more
    $.ajax({
      type: "GET",
      url: "/user/tab_more",
      data: 'p='+moreCurrentPage+'&u='+username,
      success: function(html){
        // show button only if more results possible
        if ( moreCurrentPage < totalPages ) {
          $('.cssload-container').hide();
          moreButton.appendTo('.show-more-beers');
        } else {
          $('.cssload-container, .show-more').remove();
        }

        // append results
        $('.tab-list').append(html);
      },
      error: function() {
        var notification = '<div class="alert-box error">There was an error fetching more beers. Please refresh the page and try again.</div>';

        $('.cssload-container, .show-more').remove();

        $('.show-more-beers').append(notification);
      }
    });
  });

  // tab sorting
  $('.tab-sort--icon').click(function() {
    $('.tab-sort').toggleClass('open');
  });

  $('.tab-sort--option').click(function(e) {
    e.preventDefault();

    var href = $(this).attr('href'),
        sortOpt = $(this).data('sortopt'),
        username = $('.main-nav .user-dropdown').attr('alt');

		ga('send', 'event', 'Tab', 'Sort Tab', sortOpt);

    setTimeout(function() {
      window.location.href = href;
    }, 300);
	});
	
	$('#submit-beer-form').on('submit', function(e) {
		$('#edit-tab-submit').attr({
			value: 'Saving to your tab...',
			disabled: 'disabled'
		});
	});

	$('#edit-tab-form').on('submit', function(e) {
			e.preventDefault();

			var formID = $(this).attr( 'id' );

			// disable button and show loader
			$('#edit-tab-error').text('').hide();
			$('#edit-tab-submit').attr({
				value: 'Saving changes...',
				disabled: 'disabled'
			});

			// get values for interactions
			var rowId = $(this).find('#row_id').val(),
					beerId = $(this).find('#beer_id').val(),
					notes = $(this).find('#notes').val(),
					rating = $(this).find('#rating').val(),
					isFavorite = $(this).find('#favorite:checked').length,
					beerName = $(this).find('#edit-beer_name').val(),
					breweryName = $(this).find('#edit-brewery_name').val(),
					styleName = $(this).find('#submit_style').val();

			var formAction = $(this).attr('action');
			var formData = new FormData(this);

			ga('send', 'event', 'Edit Beer', 'Save', rowId);

			$.ajax({
					type: "POST",
					url: formAction,
					data: formData,
					cache: false,
			    contentType: false,
			    processData: false,
					success: function(response){
						var res = JSON.parse( response );
						if ( res.error != undefined ) {
							// show error notification
							var errorMessage = res.error;
							$( '#edit-tab-error' ).text( errorMessage ).fadeIn(200);

							ga('send', 'event', 'Edit Beer', 'Error', rowId + ': ' + errorMessage);

							// enable button
							$('#edit-tab-submit').attr({
								value: 'Save Changes'
							}).removeAttr('disabled');
						} else {

							if ( formID == 'submit-tab-form' ) {
								var notification = '<div class="alert-box success top-notify"><span class="icon icon-check"></span><span class="text">Thanks for adding ' + beer_name + '. You are awesome!</span></div>';
							} else {
								var notification = '<div class="alert-box success top-notify"><span class="icon icon-check"></span><span class="text">Successfully updated your tab!</span></div>';
							}

							var tabRow = '#tab-'+rowId;

							var ratingImg = rating ? rating : 0;
							$(tabRow).find('.notes span').text(notes);
							$(tabRow).find('.tab-rating img').attr({
								src: '/styles/img/rating-' + ratingImg + '.svg'
							});

							$(tabRow).find('.tab-info--beer').text(beerName);
							$(tabRow).find('.tab-info--brewery').text(breweryName);
							if ( $(tabRow).find('.tab-info--style').length ) {
								$(tabRow).find('.tab-info--style').text(styleName);
							} else {
								$(tabRow).find('.tab-info').append('<h6 class="tab-info--style subheader">'+styleName+'</h6>');
							}
							$(tabRow).find('.tab-photo img').attr('src', res.photo_url);

							var favFlag = '<span class="tab-fav-flag"></span>';
							if (isFavorite) {
									$(tabRow).append(favFlag);
							} else {
									$(tabRow + ' .tab-fav-flag').remove();
							}

							ga('send', 'event', 'Edit Beer', 'Success', rowId);

							$('#edit-modal, #submit-modal').foundation('reveal', 'close');
						}

							$('body').append(notification);
							setTimeout(function() {
									$('.top-notify').delay(4000).slideUp(200);
							});
					},
					error: function() {
							var notification = '<div class="alert-box error">Hmm, this couldn\'t be updated. Please refresh the page and try again.</div>';

							$('body').append(notification);
							setTimeout(function() {
									$('.top-notify').delay(4000).slideUp(200);
							});
					}
			});
	});

});

$(document).on('click', '#edit-tab--edit-beer', function(e) {
	e.preventDefault();
	var rowId = $('#edit-tab-form #row_id').val();
	ga('send', 'event', 'Edit Beer', 'Edit beer details', rowId);
	$('#edit-tab--beer-details').slideDown(200);
});

// Submit beer form
$(document).on('click', '#submit-show-more-detail', function(e) {
	e.preventDefault();
	var beerName = $('#submit-beer-form #submit_name').val();
	ga('send', 'event', 'Add Beer', 'More Details', beerName);

	$('#submit-more-detail').slideDown(200);
});

$(document).on('click', '#submit-beer-continue', function(e) {
	e.preventDefault();

	$(this).attr({
		disabled: 'disabled'
	});

	var beerName = $('#submit-beer-form').find('#submit_name').val();
	$('#add-beer-name').text( beerName );

	var valid = validateForm();

	if ( valid !== true ) {
		$('.alert-box').text('').hide();
		$('#' + valid.error + '-error').text( valid.message ).fadeIn(200);
		ga('send', 'event', 'Error', 'Add Beer', 'Missing '+valid.error+' name');
		$('#submit-beer-continue').removeAttr('disabled');
	} else {
		ga('send', 'event', 'Add Beer', 'Continue', beerName);

		$('.alert-box').text('').fadeOut(200);
		$('#submit-container').fadeOut(200);
		setTimeout(function() {
			$('#add-container').fadeIn(200);
		}, 200);
	}
});

function validateForm() {
	var beerVal = $('#submit_name').val(),
			breweryVal = $('#submit_brewery').val();

	if ( beerVal == null || beerVal == '' ) {
		return {
			error: 'beer',
			message: 'Please enter a beer name'
		};
	}

	if ( breweryVal == null || breweryVal == '' ) {
		return {
			error: 'brewery',
			message: 'Please enter a brewery name'
		};
	}

	return true;
}

// Edit tab item
$(document).on('click', '.tab-list .edit-beer-link', function() {
	var rowId = $(this).data('rowid');
	ga('send', 'event', 'Edit Beer', 'Edit', rowId.toString());
});

// Rating
$(document).on('change', '#submit-tab-form #rating', function(e) {
	var rating = $(this).val();
	ga('send', 'event', 'Add Beer', 'Rating', rating);
});
$(document).on('change', '#edit-tab-form #rating', function(e) {
	var rowId = $('#edit-tab-form #row_id').val();
	ga('send', 'event', 'Edit Beer', 'Rating', rowId);
});

// Favorite switch
$(document).on('click', '#submit-tab-form .fav .switch-paddle', function(e) {
	ga('send', 'event', 'Add Beer', 'Toggle Fav');
});
$(document).on('click', '#edit-tab-form .fav .switch-paddle', function(e) {
	var rowId = $('#edit-tab-form #row_id').val();
	ga('send', 'event', 'Edit Beer', 'Toggle Fav', rowId);
});

// Photo
$(document).on('change', '#submit-tab-form input#addphoto', function(e) {
	ga('send', 'event', 'Add Beer', 'Add Photo');
});
$(document).on('change', '#edit-tab-form input#addphoto', function(e) {
	var rowId = $('#edit-tab-form #row_id').val();
	ga('send', 'event', 'Edit Beer', 'Add Photo', rowId);
});

// Notes
$(document).on('blur', '#submit-tab-form #notes', function(e) {
	ga('send', 'event', 'Add Beer', 'Add Notes');
});
$(document).on('blur', '#edit-tab-form #notes', function(e) {
	var rowId = $('#edit-tab-form #row_id').val();
	ga('send', 'event', 'Edit Beer', 'Edit Notes', rowId);
});
