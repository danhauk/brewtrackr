var authData = firebase.getAuth();
var isAdmin = false;

function checkAdmin(authData, callback) {
  firebase.child('userProfile/'+authData.uid).once('value', function(snapshot) {
    if (snapshot.val().isAdmin) {
      this.callback(true);
    } else {
      this.callback(false);
    }
  }, {callback: callback});
}

$(document).ready(function() {
  checkAdmin(authData, function(returnAdmin) {
    if (returnAdmin) {
      console.log("Admin!");
      isAdmin = true;
    } else {
      console.log("Not admin!");
    }
  })
});