<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head><title>Hello from BrewTrackr</title></head>
<body>

	<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#f8f7f4">
		<tr>
			<td width="100%" bgcolor="#393939">
				<div style="max-width:600px; padding:15px; margin:0 auto">
					<a href="<?php echo site_url(); ?>"><img src="<?php echo site_url('img/brewtrackr-2.0.png'); ?>" alt="BrewTrackr" height="60"/></a>
				</div>
			</td>
		</tr>
		<tr>
  			<td align="left" width="100%" style="font: 13px/18px Arial, Helvetica, sans-serif">
  				<div style="max-width:600px; padding:15px; margin: 20px auto; font: normal 18px/1.5 Arial, Helvetica, sans-serif; color: #444;">
  					<?php echo $content; ?>

					<p>Cheers and happy drinking,<br>
					Dan<br>
					Founder of BrewTrackr</p>
  				</div>
  			</td>
		</tr>
	</table>

</body>
</html>
