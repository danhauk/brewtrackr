<p>Hi, <?php echo $username;?>!</p>
<p>Welcome to BrewTrackr, a simple way for craft beer enthusiasts to keep track of the beer you drink. Never again forget what you've already tried or if you liked it!</p>

<p><a href="<?php echo site_url($username); ?>" style="color: #fff; background: #00C08B; display: inline-block; margin: 0 auto; padding: 20px 40px; text-decoration: none; border-radius: 3px;">Start your tab!</a></p>

<p>Once you start adding beers to your tab, you can quickly find your favorites with ratings and personal tasting notes. Mark your notes public to leave a review for the community to see or keep it to yourself.</p>
<p>It's up to you how you track your beer!</p>
<p>If you need help with anything, I'm here for you. Just reply here or send me an email to hello@brewtrackr.com. You can also get in touch on <a href="https://twitter.com/brewtrackr">Twitter</a> or <a href="https://facebook.com/brewtrackr">Facebook</a>, if that's your flavor.</p>
