<?php
if ( current_url() == base_url() ) {
  $is_homepage = true;
} else {
  $is_homepage = false;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <?php if ($this->uri->segment(1) == 'beer'): ?>
      <?php
        if ( isset( $info['images']['medium'] ) ) {
          $img_url = $info['images']['medium'];
        }
        elseif ( isset( $info['labels']['medium'] ) ) {
          $img_url = $info['labels']['medium'];
        }
        else {
          $img_url = '/img/no-beer.png';
        }

        if ( isset( $info['description'] ) ) {
          $twitter_desc = $info['description'];
        } else {
          $twitter_desc = 'BrewTrackr is a site where craft beer enthusiasts keep tabs on the variety of beer they drink. Easily find your favorites. Rate what you try and write personal tasting notes to share publicly or keep to yourself.';
        }
      ?>
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="@brewtrackr" />

		<meta property="og:site_name" content="BrewTrackr" />
    <meta property="og:url" content="<?php echo current_url(); ?>" />
    <meta property="og:title" content="<?php echo $title; ?>" />
    <meta property="og:description" content="<?php echo $twitter_desc; ?>" />
    <meta property="og:image" content="<?php echo $img_url; ?>" />
			<?php if ( defined('ENVIRONMENT') && ENVIRONMENT === 'development' ): ?>
			<meta property="fb:app_id" content="929844737102372"/>
			<?php else: ?>
			<meta property="fb:app_id" content="154593144627539"/>
			<?php endif; ?>
    <?php endif; ?>

    <title><?php if ( isset($title) ) { echo ($this->uri->segment(1) == 'beer' ? $title.' on ' : $title.' | '); } ?>BrewTrackr</title>

    <meta name="description" content="Where craft beer enthusiasts keep tabs on the variety of beer they drink. Easily find your favorites. Rate what you try and write personal tasting notes to share publicly or keep to yourself.">
    <meta name="keywords" content="beer, craft, brew, micro, track, web app, social, taste, rate, review, recommend, discover" />

    <?php if ( $is_homepage ): ?>
      <meta property="og:image" content="/img/facebook-share-image-1200x630.png" />
    <?php endif; ?>

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="shortcut icon" href="/img/favicon.ico" />

		<!-- Google Analytics -->
		<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-27353672-1', 'auto');
		ga('send', 'pageview');
    <?php if ( $this->ion_auth->logged_in() ): ?>
    ga('set', 'userId', '<?php echo strval( $this->ion_auth->user()->row()->id ); ?>');
    <?php endif; ?>
		</script>

    <!-- Stylin' -->
    <link href='https://fonts.googleapis.com/css?family=Fira+Sans:400,500,300' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/styles/foundation.min.css" />
    <link rel="stylesheet" href="/styles/simple-line-icons.css" />
    <link rel="stylesheet" href="/styles/display.css" />

		<!-- Android web app -->
		<meta name="theme-color" content="#393939" />
		<meta name="application-name" content="BrewTrackr">
		<link rel="icon" sizes="36x36" href="/img/android-icon-36.png">
		<link rel="icon" sizes="48x48" href="/img/android-icon-48.png">
		<link rel="icon" sizes="72x72" href="/img/android-icon-72.png">
		<link rel="icon" sizes="96x96" href="/img/android-icon-96.png">
		<link rel="icon" sizes="144x144" href="/img/android-icon-144.png">
		<link rel="icon" sizes="192x192" href="/img/android-icon-192.png">

    <!-- iOS web app -->
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="apple-touch-icon" sizes="180x180" href="/img/app-icon-180.png">
    <link rel="apple-touch-icon" sizes="167x67" href="/img/app-icon-167.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/img/app-icon-152.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/img/app-icon-120.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/img/app-icon-76.png">

    <script src="/js/vendor/modernizr.js"></script>
    <script src="/js/vendor/jquery.js"></script>
    <script src="https://js.stripe.com/v3/"></script>
</head>

<body class="<?php echo ( $is_homepage ? 'homepage' : '' ); ?>">

<?php if ($this->session->flashdata('show_login')): ?>
<script type="text/javascript">
$(document).ready(function() {
  $('#login-modal').foundation('reveal', 'open', '/login');
});
</script>
<?php endif; ?>

<?php if ( !$this->ion_auth->logged_in() ): ?>
<div class="reveal-modal small login-modal" id="signup-modal" data-reveal aria-labelledby="modal-title" aria-hidden="true" role="dialog"></div>
<div class="reveal-modal small login-modal" id="login-modal" data-reveal aria-labelledby="modal-title" aria-hidden="true" role="dialog"></div>
<?php endif; ?>

<?php if($this->session->flashdata('notify_success')): ?>
<div class="alert-box success top-notify hide">
  <span class="icon icon-check"></span>
  <span class="text"><?php echo $this->session->flashdata('notify_success'); ?></span>
</div>
<?php endif; ?>

<?php if($this->session->flashdata('notify_error')): ?>
<div class="alert-box error top-notify hide">
  <span class="icon icon-close"></span>
  <span class="text"><?php echo $this->session->flashdata('notify_error'); ?></span>
</div>
<?php endif; ?>

    <nav class="topbar<?php echo ($this->ion_auth->logged_in() ? ' logged-in' : ' logged-out'); ?>" data-topbar role="navigation">
      <h1 class="logo left"><a href="/">BrewTrackr</a></h1>

      <?php if ($this->ion_auth->logged_in()): ?>
      <a href="/beer/submit" data-reveal-id="submit-modal" data-reveal-ajax="true" class="fab" id="fab">+</a>

      <ul class="right no-bullet main-nav">
        <?php $user = $this->ion_auth->user()->row(); ?>
        <li>
          <?php if ( $user->photo_url ){
            $user_image_url = $user->photo_url;
          } else {
            $user_image_url = '/img/no-user.svg';
          } ?>
          <a class="user-dropdown" data-dropdown="usernav" aria-controls="usernav" aria-expanded="false"><img src="<?php echo $user_image_url; ?>" class="avatar" alt="<?php echo $user->username; ?>"/></a>

          <ul id="usernav" class="f-dropdown user-dropdown-menu" data-dropdown-content aria-hidden="true" tabindex="-1">
            <li><a href="/"><span class="icon icon-book-open"></span> My Tab</a></li>
            <li><a href="/settings"><span class="icon icon-settings"></span> Settings</a></li>
            <?php if($this->ion_auth->is_admin()): ?>
            <li class="divider"></li>
            <li><a href="/admin"><span class="icon icon-rocket"></span> Admin Dashboard</a></li>
            <?php endif; ?>
						<li class="divider"></li>
						<li><a href="/support" class="support-link"><span class="icon icon-trophy"></span> Support BrewTrackr</a></li>
            <li class="divider"></li>
            <li class="logout"><a href="/logout" id="nav-logout"><span class="icon icon-power"></span> Logout</a></li>
          </ul>
        </li>
      </ul>

      <?php else: ?>

      <div class="right topbar-buttons">
        <a href="/login" data-reveal-id="login-modal" data-reveal-ajax="true" class="button round tiny outline button-login">Login</a>
        <a href="/signup" data-reveal-id="signup-modal" data-reveal-ajax="true" class="button round tiny button-signup <?php echo ( $is_homepage ? 'outline' : '' ); ?>">Sign up</a>
      </div>
      <?php endif; ?>
    </nav>
