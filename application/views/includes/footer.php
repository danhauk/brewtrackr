
    <div class="container" id="footer">
        <div class="row">
            <div class="medium-10 medium-centered columns">
                <p class="links">
                <SCRIPT TYPE="text/javascript">
    <!--

    emailE=('hello@' + 'brewtrackr.com')
    document.write('<a href="mailto:' + emailE + '">')

     //-->
    </script>
                Contact</a> &nbsp;&middot;&nbsp;
                <a href="/privacy">Privacy</a> &nbsp;&middot;&nbsp;
                <a href="/tos">Terms of Use</a>
                <p class="copyright">&copy; 2012-<?php echo date('Y'); ?> BrewTrackr. Cheers! &middot; <a href="https://facebook.com/brewtrackr">Facebook</a> &middot; <a href="https://twitter.com/brewtrackr">Twitter</a></p>
            </div>
        </div>
    </div> <!-- /container -->

		<div class="reveal-modal small tab-modal" id="submit-modal" data-reveal aria-labelledby="modal-title" aria-hidden="true" role="dialog"></div>

<?php if ( defined('ENVIRONMENT') && ENVIRONMENT === 'development' ): ?>
    <span class="label label-warning" style="position:fixed; bottom:0; right:0">Page rendered in <strong>{elapsed_time}</strong> seconds</span>
<?php endif; ?>

<script src="/js/vendor/fastclick.js"></script>
<script src="/js/foundation.min.js"></script>
<script src="/js/app.js"></script>

<script>
  $(document).foundation();
</script>

<?php if ( $this->ion_auth->logged_in() && ENVIRONMENT == 'production' ) {
  $user = $this->ion_auth->user()->row();
  $username = $user->username;
  $user_email = $user->email;
  $twitter_username = ( null != $this->session->userdata("twitter_username") ? $this->session->userdata("twitter_username") : "");
  $facebook_name = ( null != $this->session->userdata("facebook_name") ? $this->session->userdata("facebook_name") : "");
?>

<div class="alert-box upsell global hide">
  <span class="icon icon-trophy"></span>
  <span class="text"><strong>Like Brewtrackr?</strong> You can help keep the site up and running with a secure one-time donation. <a href="/support">Donate now</a>.</span>
	<a href="javascript:;" class="close">
		<span class="icon-close"></span>
		<span class="close-label">Cancel</span>
	</a>
</div>

<?php } ?>

</body>
</html>
