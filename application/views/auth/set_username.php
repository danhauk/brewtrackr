<div class="row login-container signup-container">
  <h3 class="text-center modal-title">Create an account</h3>

    <h4>Welcome, <?php echo $_SESSION['facebook_name']; ?>!</h4>
    <p>We're glad you're here. Let's set up your BrewTrackr account.</p>

    <div class="alert-box error hide" id="auth-error" data-alert></div>

    <form action="/auth/save_username" id="username-form" class="auth-modal-form" method="post">
      <p>
        <label for="username" class="auth-modal-label">Username:</label>
        <input type="text" name="username" id="username" autocorrect="off" autocapitalize="off" autocomplete="off"/>
        <span class="form-validate username-valid hide"></span>
      </p>

			<input type="hidden" name="facebook_id" value="<?php echo $_SESSION['facebook_id']; ?>" id="facebook_id"/>
      <input type="submit" class="button radius expand" id="username-submit" value="Save Username" disabled="disabled" />

    </form>
</div>

<a class="close-reveal-modal" aria-label="Close">&#215;</a>

<script type="text/javascript" src="/js/auth.js"></script>
