<div class="row login-container">
  <div class="medium-6 medium-centered large-4 columns">
    <h4>Login to BrewTrackr</h4>

    <?php if ( $message_error ): ?>
    <div class="alert-box error" data-alert>
      <?php echo $message_error;?>
      <a href="#" class="close">&times;</a>
    </div>
    <?php endif; ?>

    <?php echo form_open("/login");?>

      <div class="form-group">
        <label for="identity">Username</label>
        <?php echo form_input($identity);?>
      </div>

      <div class="form-group">
        <label for="password" class="left">Password</label>
        <small class="right"><a href="forgot_password" tabindex="4"><?php echo lang('login_forgot_password');?></a></small>
        <?php echo form_input($password);?>
      </div>

      <div class="form-group">
        <label for="remember"><input type="checkbox" id="remember" name="remember"> Remember me</label>
      </div>

      <p><input type="submit" class="button radius" value="Login" tabindex="3"/></p>

    <?php echo form_close();?>
  </div>
</div>