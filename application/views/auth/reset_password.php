<div class="row login-container">
  <div class="medium-6 medium-centered large-4 columns">
    <h4>Change Password</h4>

    <?php if ( $message ): ?>
    <div class="alert-box error" data-alert>
      <?php echo $message;?>
      <a href="#" class="close">&times;</a>
    </div>
    <?php endif; ?>

    <?php echo form_open('auth/reset_password/' . $code);?>

		<p>
			<label for="new_password"><?php echo sprintf(lang('reset_password_new_password_label'), $min_password_length);?></label>
			<?php echo form_input($new_password);?>
		</p>

		<p>
			<?php echo lang('reset_password_new_password_confirm_label', 'new_password_confirm');?>
			<?php echo form_input($new_password_confirm);?>
		</p>

		<?php echo form_input($user_id);?>
		<?php echo form_hidden($csrf); ?>

		<p><input type="submit" class="button radius" value="Change Password"/></p>

	<?php echo form_close();?>

  </div>
</div>