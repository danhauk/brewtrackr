<div class="row login-container signup-container">
  <div class="medium-6 large-4 medium-centered columns">
    <h4>Sign up</h4>

    <?php if ( $message_error ): ?>
    <div class="alert-box error" data-alert>
      <?php echo $message_error;?>
      <a href="#" class="close">&times;</a>
    </div>
    <?php endif; ?>

    <?php echo form_open($this->uri->uri_string(), array('id' => 'register-form'));?>

      <p>
        <label for="username">Username</label>
        <input type="text" name="username" id="username" value="<?php echo set_value('username'); ?>" autocomplete="off" />
        <span class="form-validate username-valid hide"></span>
      </p>

      <p>
        <label for="email">Email address</label>
        <input type="email" name="email" id="email" value="<?php echo set_value('email'); ?>" />
        <span class="form-validate email-valid hide"></span>
      </p>

      <p>
        <label for="password">Password</label>
        <input type="password" name="password" id="password" />
      </p>

      <?php $data = array('name' => 'signup', 'class' => 'button radius'); ?>
      <p><?php echo form_submit($data, 'Sign up');?></p>

    <?php echo form_close();?>
  </div>
</div>

<script type="text/javascript">
$("#register-form").submit(function(e) {
  var form = this;

  e.preventDefault();


  setTimeout( function () {
    form.submit();
  }, 500);
});
</script>
