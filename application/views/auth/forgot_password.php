<div class="row login-container">
	<div class="medium-6 large-4 medium-centered columns">
		<h4>Forgot your password?</h4>
		<p>No worries, we all forget sometimes! Please enter your username and we will email a link to reset your password.</p>

		<?php if ( $message ): ?>
		<div class="alert-box error radius" data-alert>
			<?php echo $message;?>
			<a href="#" class="close">&times;</a>
		</div>
		<?php endif; ?>

		<?php echo form_open("/forgot_password");?>

		      <p>
		      	<label for="identity">Username:</label>
		      	<?php echo form_input($identity);?>
		      </p>

		      <p><input type="submit" class="button radius" value="Send my reset email" /></p>

		<?php echo form_close();?>
	</div>
</div>