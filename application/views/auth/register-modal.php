<div class="row login-container signup-container">
    <h3 class="text-center modal-title">Create an account</h3>

		<?php if ( $this->session->userdata('facebook_auth_error') ): ?>

    <div class="alert-box error" id="facebook-auth-error" data-alert><?php echo $this->session->userdata('facebook_auth_error'); ?></div>

    <?php else: ?>
    <a href="/auth/facebook" class="button radius expand auth-facebook" id="facebook-login">Login with Facebook</a>

    <div class="auth-separator text-center"><span class="auth-separator-text">OR</span></div>
    <?php endif; ?>

    <div class="alert-box error hide" id="auth-error" data-alert></div>

    <?php echo form_open('/auth/signup_modal', array('id' => 'register-form', 'class' => 'auth-modal-form'));?>

      <p>
        <label for="username" class="auth-modal-label">Username</label>
        <input type="text" name="username" id="username" value="<?php echo set_value('username'); ?>" autocomplete="off" />
        <span class="form-validate username-valid hide"></span>
      </p>

      <p>
        <label for="email" class="auth-modal-label">Email address</label>
        <input type="email" name="email" id="email" value="<?php echo set_value('email'); ?>" />
        <span class="form-validate email-valid hide"></span>
      </p>

      <p>
        <label for="password" class="auth-modal-label">Password</label>
        <input type="password" name="password" id="password" />
      </p>

      <?php $data = array('name' => 'signup', 'class' => 'button radius expand', 'id' => 'signup-submit'); ?>
      <p>
        <?php echo form_submit($data, 'Sign up', 'disabled="disabled"');?>
        <span class="auth-modal-small">Already have an account? <a href="/auth/login" data-reveal-id="login-modal" data-reveal-ajax="true">Log in</a></span>
      </p>

    <?php echo form_close();?>
</div>

<a class="close-reveal-modal" aria-label="Close">&#215;</a>

<script type="text/javascript" src="/js/auth.js"></script>
