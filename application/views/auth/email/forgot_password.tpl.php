<p>Hi, <?php echo $identity;?>!</p>
<p>It looks like you need a password reset on BrewTrackr. If you didn't request this, just ignore the email and your password won't change.</p>

<p><a href="<?php echo site_url('/auth/reset_password/'.$forgotten_password_code); ?>" style="color: #fff; background: #00C08B; display: inline-block; margin: 0 auto; padding: 20px 40px; text-decoration: none; border-radius: 3px;">Reset your password</a></p>