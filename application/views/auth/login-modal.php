<div class="row login-container">
    <h3 class="text-center modal-title">Login to BrewTrackr</h3>

    <?php if ( $this->session->userdata('facebook_auth_error') ): ?>

    <div class="alert-box error" id="facebook-auth-error" data-alert><?php echo $this->session->userdata('facebook_auth_error'); ?></div>

    <?php else: ?>
    <a href="/auth/facebook" class="button radius expand auth-facebook" id="facebook-login">Login with Facebook</a>

    <div class="auth-separator text-center"><span class="auth-separator-text">OR</span></div>
    <?php endif; ?>

    <div class="alert-box error hide" id="auth-error" data-alert></div>

    <?php echo form_open("/auth/login_modal", array('id' => 'login-form', 'class' => 'auth-modal-form'));?>

      <p>
        <label for="identity" class="auth-modal-label">Username</label>
        <input type="text" name="identity" id="identity" tabindex="1" autocorrect="off" autocapitalize="off" autocomplete="off">
      </p>

      <p>
        <label for="password" class="auth-modal-label left">Password</label>
        <small class="right"><a href="forgot_password" tabindex="5"><?php echo lang('login_forgot_password');?></a></small>
        <input type="password" name="password" id="password" tabindex="2">
      </p>

      <p>
        <label for="remember"><input type="checkbox" id="remember" name="remember" tabindex="3"> Remember me</label>
      </p>

      <p>
        <input type="submit" class="button radius expand" id="login-submit" value="Login" tabindex="4"/>
        <span class="auth-modal-small">Need an account? <a href="/auth/signup" data-reveal-id="signup-modal" data-reveal-ajax="true">Sign Up</a></span>
      </p>

    <?php echo form_close();?>
  </div>
</div>

<a class="close-reveal-modal" aria-label="Close">&#215;</a>

<script type="text/javascript" src="/js/auth.js"></script>
