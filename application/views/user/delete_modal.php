<h4>You are about to delete your account!</h4>
<p>If you continue, all of your BrewTrackr data will be lost forever. This action cannot be undone and we can't get it back for you later.</p>

<div class="alert-box error hide" id="delete-account-error" data-alert></div>

<form action="/user/delete_confirm" method="post" id="delete-account-form">
  <hr>
  <strong>Confirm this action</strong><br>
  Please type your username to delete your account

  <input type="text" name="usernameDelete" id="usernameDelete" />

  <div class="text-right">
    <a class="button tertiary radius small close-modal" id="close-modal">Don't Delete</a>
    <input type="submit" class="button radius small button-scary" id="delete-account-submit" disabled="disabled" value="Yes, Delete Account" />
  </div>
</form>

<a class="close-reveal-modal" aria-label="Close">&#215;</a>

<script type="text/javascript" src="/js/vendor/jquery.js"></script>
<script src="/js/foundation.min.js"></script>
<script type="text/javascript">
$("#close-modal").on("click", function() {
  $('#settings-modal').foundation('reveal', 'close');
});

$("input#usernameDelete").on("input", function() {
  var input = this.value,
      match = $("#username").val();

  if(input === match) {
    $("#delete-account-submit").removeAttr("disabled");
  }
  else {
    $("#delete-account-submit").attr("disabled", "disabled");
  }
});

$("#delete-account-form").on("submit", function(e) {
  e.preventDefault();
  var username = $("#usernameDelete").val();

  ga('send', 'event', 'Account', 'Delete', username);

  $("#delete-account-submit").val("Deleting " + username + "...").attr("disabled", "disabled");

  var postData = {
    username: username
  }

  $.ajax({
    type:'POST',
    url: $(this).attr('action'),
    data: postData,
    success:function(response){
      if ( response != 'success' ) {
        $('#delete-account-error').html(response).fadeIn(200);

        $("#delete-account-submit").val("Yes, Delete Account").attr("disabled", "disabled");
      }
      else {
        setTimeout(function() {
          window.location.href = window.location.origin+"/logout";
        }, 1000);
      }
    }
  });
});
</script>
