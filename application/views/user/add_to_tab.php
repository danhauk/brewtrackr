<script type="text/javascript" src="/js/jquery.barrating.min.js"></script>
<script type="text/javascript" src="/js/tab.js"></script>
<script type="text/javascript">
$(function() {
    var half_ratings = false;

    if ( $('#rating').hasClass('premium') ) {
        $('#rating').barrating({
            showSelectedRating: false,
            wrapperClass: 'half-ratings'
        });
    }
    else {
        $('#rating').barrating({
            showSelectedRating: false
        });
    }

    // tooltip for activate
    $('.tab-share-activate').hover(function() {
      var tooltip = $(this).find('span');
      var tipHeight = tooltip.outerHeight();
      var tipTop = (tipHeight/2)-32 + 'px';

      tooltip.css("top", tipTop).fadeIn();
    }, function() {
      $(this).find('span').fadeOut();
    });
});
</script>
  <form action="/user/add_to_tab" method="post" id="tab-modal-form" enctype="multipart/form-data">
        <h3 class="text-center">Rate <?php echo $beer_name; ?></h3>

        <?php if ( $premium_user ): ?>
        <select id="rating" name="rating" class="premium">
            <option value=""></option>
            <option value=".5">.5</option>
            <option value="1">1</option>
            <option value="1.5">1.5</option>
            <option value="2">2</option>
            <option value="2.5">2.5</option>
            <option value="3">3</option>
            <option value="3.5">3.5</option>
            <option value="4">4</option>
            <option value="4.5">4.5</option>
            <option value="5">5</option>
        </select>
        <?php else: ?>
        <select id="rating" name="rating">
            <option value=""></option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
        </select>
        <?php endif; ?>

				<div class="comments">
					<textarea name="comment" id="comment" rows="3" placeholder="Optional tasting notes"></textarea>
					<?php if ( $premium_user ): ?>
					<div class="photo-input">
						<input type="file" id="addphoto" name="addphoto" class="photo-upload">
						<a href="javascript:;" class="photo-upload-button">
							<span class="icon-camera"></span>
							<span class="photo-upload-text">Add photo</span>
						</a>
					</div>
					<div class="photo-preview"></div>
					<?php endif; ?>
					<div class="comment-public">
						<label><input type="checkbox" name="public" id="public" <?php echo ($this->session->userdata('comment_public') != false ? 'checked' : ''); ?> /> Make it public</label>
					</div>
				</div>

				<?php if ( $premium_user ): ?>
				<div id="photo-upload-error" class="alert-box error hide"></div>
				<?php endif; ?>

    <input type="hidden" name="beer_id" id="beer_id" value="<?php echo $beer_id; ?>"/>
      <input type="hidden" name="beer_name" id="beer_name" value="<?php echo $beer_name; ?>"/>
      <input type="hidden" name="brewery_name" id="brewery_name" value="<?php echo $brewery_name; ?>"/>
      <input type="hidden" name="style" id="style" value="<?php echo $style; ?>"/>
      <input type="hidden" name="beer_image" id="beer_image" value="<?php echo $beer_image; ?>" />
      <input type="hidden" name="beer_description" id="beer_description" value="<?php echo $beer_description; ?>" />
      <input type="hidden" name="beer_abv" id="beer_abv" value="<?php echo $beer_abv; ?>" />
      <input type="hidden" name="beer_ibu" id="beer_ibu" value="<?php echo $beer_ibu; ?>" />

    <div class="fav clearfix">
      <div class="switch tiny right">
        <input class="switch-input" id="favorite" type="checkbox" name="favorite">
        <label class="switch-paddle" for="favorite">
          <span class="show-for-sr">Favorite</span>
        </label>
      </div>
      <label class="left favorite-label" for="favorite">Save as a favorite</label>
    </div>

		<div id="add-tab-error" class="alert-box error hide"></div>

    <input type="submit" class="button submit radius expand" id="add-tab-submit" value="Add to tab"/>
  </form>

  <a class="close-reveal-modal" aria-label="Close">&#215;</a>
