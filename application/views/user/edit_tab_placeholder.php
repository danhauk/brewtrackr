<div id="edit-tab">
	<div class="cssload-container"><div class="cssload-wheel"></div></div>
</div>

<script type="text/javascript">
var id = <?php echo $_GET['id']; ?>;

$.ajax({
  type: "GET",
  url: "/user/edit_form_show",
  data: "id="+id
})
	.done(function(html) {
		$('.cssload-container').hide();
		$('#edit-tab').append(html);
	});
</script>