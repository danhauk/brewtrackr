<div class="row">
  <h3 class="text-center modal-title">How about some free stickers?</h3>

  <div class="modal-section--full-width">
    <img src="/img/stickers.jpg" width="100%" class="swag-modal--image" />
  </div>

  <p>We're upgrading our system, and just by logging in you helped us out with migrating your account. As a small way to say "Thank You", where should we send your stickers?</p>

  <div class="alert-box error hide" id="swag-modal-error" data-alert></div>

  <form action="/user/swag_submit" method="post" id="swag-modal-form">
    <p>
      <label for="name" class="auth-modal-label">Name</label>
      <input type="text" name="name" id="name" />
    </p>

    <p>
      <label for="address" class="auth-modal-label">Mailing Address</label>
      <textarea name="address" id="address" cols="4"></textarea>
    </p>

    <p class="text-center">
      <input type="submit" class="button radius expand" id="swag-modal-submit" value="Send Me Stickers!" />

      <a href="javascript:;" id="close-modal">No thanks, I don't want stickers</a>
    </p>
  </form>
</div>

<a class="close-reveal-modal" aria-label="Close">&#215;</a>

<script type="text/javascript">
var authData = firebase.getAuth();
var stickersRef = firebase.child('stickers/'+authData.uid);

$("#close-modal").on('click', function() {
  stickersRef.update({
    declined: true,
    gotStickers: true
  });

  $("#swag-modal").foundation('reveal', 'close');
});

$("#swag-modal-form").on("submit", function(e) {
  e.preventDefault();
  $("#swag-modal-submit").attr("disabled", "disabled");
  $('#swag-modal-error').hide();

  var name = $('#name').val(),
      address = $('#address').val();

  if (!address.trim()) {
    $('#swag-modal-error').html("Oops! We can't send stickers if we don't have an address.").fadeIn(200);
    $("#swag-modal-submit").removeAttr("disabled");
  } else {
    stickersRef.update({
      name: name.trim(),
      address: address.trim(),
      gotStickers: true
    }, function(error) {
      if (error) {
        $('#swag-modal-error').html(error.message).fadeIn(200);
        $("#swag-modal-submit").removeAttr("disabled");
      } else {
        $("#swag-modal-form").html("<h4>Thanks! Your stickers are on the way!");
        setTimeout(function() {
          window.location.reload();
        }, 3000);
      }
    })
  }

});
</script>