<script type="text/javascript" src="/js/jquery.barrating.min.js"></script>
<script type="text/javascript" src="/js/vendor/fuse.min.js"></script>
<script type="text/javascript" src="/js/tab.js"></script>
<script type="text/javascript">
$(function() {
    var half_ratings = false;

    if ( $('#rating').hasClass('premium') ) {
        $('#rating').barrating({
            showSelectedRating: false,
            wrapperClass: 'half-ratings'
        });
    }
    else {
        $('#rating').barrating({
            showSelectedRating: false
        });
    }
});
</script>
<form action="/user/edit_tab" method="post" id="edit-tab-form" enctype="multipart/form-data">
	<h3 class="text-center"><?php echo $tab_info->row()->beer_name; ?></h3>

  <?php if ( $premium_user ): ?>
  <select id="rating" name="rating" class="premium">
      <option value="" <?php if($tab_info->row()->rating == 0) { echo 'selected';} ?>></option>
      <?php
      for ( $i = .5; $i <= 5; $i+=.5 ) {
          echo '<option value="' . $i . '"';
          if ( $i == $tab_info->row()->rating ) {
              echo ' selected';
          }
          echo '>' . $i.'</option>';
      }
      ?>
  </select>
  <?php else: ?>
  <select id="rating" name="rating">
      <option value="" <?php if($tab_info->row()->rating == 0) { echo 'selected';} ?>></option>
      <?php
      for ( $i = 1; $i <= 5; $i++ ) {
          echo '<option value="' . $i . '"';
          if ( $i == $tab_info->row()->rating ) {
              echo ' selected';
          }
          echo '>' . $i.'</option>';
      }
      ?>
  </select>
  <?php endif; ?>

	<div class="comments">
  	<textarea name="notes" id="notes" rows="3" placeholder="Optional tasting notes"><?php echo $tab_info->row()->notes; ?></textarea>

		<?php if ( ! ( isset($tab_info->row()->photo_url) ) ): ?>
		<div class="photo-input">
			<input type="file" id="addphoto" name="addphoto" class="photo-upload">
			<a href="javascript:;" class="photo-upload-button">
				<span class="icon-camera"></span>
				<span class="photo-upload-text">Add photo</span>
			</a>
		</div>
		<div class="photo-preview"></div>
		<?php else: ?>
		<div class="photo-preview no-remove" style="background-image:url(<?php echo $tab_info->row()->photo_url; ?>); display:block"></div>
		<?php endif; ?>

  </div>

	<div id="photo-upload-error" class="alert-box error hide"></div>

	<input type="hidden" name="row_id" id="row_id" value="<?php echo $tab_info->row()->id; ?>"/>
	<input type="hidden" name="beer_id" id="beer_id" value="<?php echo $tab_info->row()->beer_id; ?>"/>

  <div class="fav clearfix">
    <div class="switch tiny right">
      <input class="switch-input" id="favorite" type="checkbox" name="favorite" <?php echo ($tab_info->row()->favorite ? 'checked' : ''); ?>>
      <label class="switch-paddle" for="favorite">
        <span class="show-for-sr">Favorite</span>
      </label>
    </div>
    <label class="left favorite-label" for="favorite">Save as a favorite</label>
  </div>

	<div class="hide" id="edit-tab--beer-details">
    <label for="submit_name" class="auth-modal-label">Beer Name</label>
    <input type="text" name="beer_name" id="edit-beer_name" autocapitalize="words" autocorrect="off" autocomplete="off" required value="<?php echo $tab_info->row()->beer_name; ?>" />
		<div class="alert-box error hide" id="beer-error" data-alert></div>

    <label for="submit_brewery" class="auth-modal-label">Brewery Name</label>
    <input type="text" name="brewery_name" id="edit-brewery_name" autocapitalize="words" autocorrect="off" autocomplete="off" required value="<?php echo $tab_info->row()->brewery_name; ?>" />
		<div class="alert-box error hide" id="brewery-error" data-alert></div>

    <p>
    	<label for="submit_abv" class="auth-modal-label">ABV</label>
      <input type="text" name="abv" id="edit-abv" value="<?php echo $tab_info->row()->abv; ?>" />
    </p>
    <p>
      <label for="submit_ibu" class="auth-modal-label">IBU</label>
      <input type="text" name="ibu" id="edit-ibu" value="<?php echo $tab_info->row()->ibu; ?>" />
    </p>

    <label for="submit_style" class="auth-modal-label">Style</label>
		<div class="autocomplete-container" id="style-autocomplete-container">
			<input type="text" id="submit_style" value="<?php echo ( $tab_info->row()->beer_style ) ? $tab_info->row()->beer_style : null; ?>" autocorrect="off" autocomplete="off" placeholder="Search for a style" />
			<div class="autocomplete hide" id="style_autocomplete"></div>
			<input type="hidden" name="submit_style_id" id="submit_style_id" value="" />
    </div>
		<div class="alert-box error hide" id="style-error" data-alert></div>

    <p>
      <label for="submit_description" class="auth-modal-label">Commercial Description</label>
      <textarea rows="6" name="description" id="edit-description"><?php echo $tab_info->row()->description; ?></textarea>
    </p>
	</div>

	<div id="edit-tab-error" class="alert-box error hide"></div>

	<div class="edit-tab--actions">
		<a href="javascript:;" class="button tertiary radius edit-tab--edit-beer" id="edit-tab--edit-beer">Edit Beer</a>
  	<input type="submit" class="button submit radius" id="edit-tab-submit" value="Save Changes"/>
	</div>
</form>

<a class="close-reveal-modal" aria-label="Close">&#215;</a>
