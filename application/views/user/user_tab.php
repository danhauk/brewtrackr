<?php
$is_me = false;
$is_favs = false;

if ( $this->ion_auth->logged_in() && $this->ion_auth->user()->row()->id === $user_info->row()->id ) {
	$is_me = true;
}

if ( $this->uri->segment(2) == 'favorites' ) {
	$is_favs = true;
}
?>
<div class="row">
	<div class="small-12 medium-10 medium-centered columns">
		<div class="card small-full user-profile">
			<div class="row small-collapse">
				<div class="small-4 medium-2 columns avatar-container">
					<?php if ( $user_info->row()->photo_url ){
						$user_image_url = $user_info->row()->photo_url;
					} else {
						$user_image_url = '/img/no-user.svg';
					} ?>
					<img src="<?php echo $user_image_url; ?>" class="avatar img-responsive" />
				</div>
				<div class="small-8 medium-7 columns bio">
					<h3><?php echo $user_info->row()->username; ?></h3>
					<?php
					if ( $user_info->row()->bio ) {
						echo '<p>' . nl2br( $user_info->row()->bio ) . '</p>';
					}
					?>
				</div>
				<div class="small-12 medium-3 columns tab-summary">
					<div>
						<?php echo $tab_totals['total_beers']; ?>
						<span>beers</span>
					</div>
					<div>
						<?php echo $tab_totals['total_favs']; ?>
						<span>favs</span>
					</div>
				</div>
			</div>

			<div class="row small-collapse user-profile-subnav">
				<div class="small-12 medium-8 columns">
					<div class="tab-sort">
						<a href="/<?php echo $username; ?>"<?php echo ($is_favs ? '' : ' class="active"'); ?>><?php echo $sort_by; ?></a>
						<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;" xml:space="preserve" class="tab-sort--icon">
							<polygon points="20,9 12,17 4,9 5.414,7.586 12,14.172 18.586,7.586 	"/>
						</svg>
						<ul class="tab-sort--dropdown">
							<li>
								<a href="/<?php echo $username; ?>?sort=recent<?php echo (isset($s) ? "&s={$s}" : '' ); ?>" class="tab-sort--option" data-sortOpt="recent">Recently Added</a>
							</li>
							<li>
								<a href="/<?php echo $username; ?>?sort=rating-high<?php echo (isset($s) ? "&s={$s}" : '' ); ?>" class="tab-sort--option" data-sortOpt="rating-high">Highest Rating</a>
							</li>
							<li>
								<a href="/<?php echo $username; ?>?sort=rating-low<?php echo (isset($s) ? "&s={$s}" : '' ); ?>" class="tab-sort--option" data-sortOpt="rating-low">Lowest Rating</a>
							</li>
							<li>
								<a href="/<?php echo $username; ?>?sort=a-z<?php echo (isset($s) ? "&s={$s}" : '' ); ?>" class="tab-sort--option" data-sortOpt="a-z">Sort A-Z</a>
							</li>
							<li>
								<a href="/<?php echo $username; ?>?sort=z-a<?php echo (isset($s) ? "&s={$s}" : '' ); ?>" class="tab-sort--option" data-sortOpt="z-a">Sort Z-A</a>
							</li>
							<li>
								<a href="/<?php echo $username; ?>?sort=brewery<?php echo (isset($s) ? "&s={$s}" : '' ); ?>" class="tab-sort--option" data-sortOpt="brewery">Sort by Brewery</a>
							</li>
						</ul>
					</div>
					<a href="/<?php echo $username; ?>/favorites"<?php echo ($is_favs ? ' class="active"' : ''); ?>>Favorites</a>
				</div>
				<div class="small-12 medium-4 columns">
					<form class="search-tab-form" action="/<?php echo $username; ?>" method="get">
						<input type="search" name="s" autocorrect="off" autocapitalize="off" placeholder="Search tab" value="<?php echo (isset($s) ? $s : '' ); ?>">
						<span class="icon-magnifier"></span>
					</form>
				</div>
			</div>
		</div>

			<div class="tab-list small-full card">
				<?php foreach ( $tab->result() as $row ): ?>
				<?php $beer_url = $row->beer_id . '-' . url_title( $row->beer_name, '-' ); ?>
				<div class="row small-collapse tab-item" id="tab-<?php echo $row->id; ?>">
					<div class="small-3 medium-2 tab-photo columns">
						<?php
						if ( $row->photo_url ) {
							$beer_image = $row->photo_url;
						} else if ( $row->beer_image ) {
							$beer_image = $row->beer_image;
						} else {
							$beer_image = '/img/no-beer.png';
						}
						?>
						<img src="<?php echo $beer_image; ?>" />
					</div>

					<div class="small-9 medium-7 tab-info columns">
						<h4 class="stronghead tab-info--beer"><?php echo $row->beer_name; ?></h4>
						<h6 class="tab-info--brewery"><?php echo $row->brewery_name; ?></h6>

						<?php if ( $row->beer_style ): ?>
						<h6 class="subheader tab-info--style"><?php echo $row->beer_style; ?></h6>
						<?php endif; ?>
					</div>

					<?php if ( $is_me ): // this is the logged in user's tab ?>
					<div class="small-9 small-offset-3 medium-3 medium-offset-0 columns tab-personal">
						<div class="small-12 columns tab-rating">
							<img src="/styles/img/rating-<?php echo $row->rating; ?>.svg" height="30" width="110"/>
						</div>

						<div class="small-12 columns tab-notes">
							<a href="javascript:;" class="show-notes"><span class="icon-notebook"></span> Notes</a>
							<a href="/user/edit_form?id=<?php echo $row->id; ?>" data-reveal-id="edit-modal" data-reveal-ajax="true" data-rowid="<?php echo $row->id; ?>" class="edit-beer-link"><span class="icon-pencil"></span> Edit</a>
						</div>
					</div>

					<div class="notes-container">
						<div class="notes">
							<?php if ($row->notes): ?>
							<span><?php echo nl2br( $row->notes ); ?></span>

							<?php else: ?>
							<span style="color:rgba(255,255,255,0.5)">No notes for this beer.</span>

							<?php endif; ?>

							<a href="javascript:;" class="close-notes">&times;</a>
						</div> <!-- notes -->
					</div> <!-- notes-container -->

					<?php else: // this is not the logged in user's tab ?>

					<div class="small-9 small-offset-3 medium-3 medium-offset-0 columns tab-personal">
						<div class="small-6 medium-12 columns tab-rating">
							<img src="/styles/img/rating-<?php echo $row->rating; ?>.svg" height="30" width="110"/>
						</div>
					</div>

					<?php endif; ?>

					<?php if ( $row->favorite ): ?>
					<span class="tab-fav-flag"></span>
					<?php endif; ?>
				</div>
				<?php endforeach; ?>
			</div>

			<?php if ( !$is_favs && !isset($_GET['s']) && $num_pages > 1 && $current_page <= $num_pages ): ?>
			<div class="show-more-beers small-full card">
				<a href="javascript:;" data-page="<?php echo $current_page; ?>" data-pages="<?php echo $num_pages; ?>" data-total="<?php echo $tab_totals['total_beers']; ?>" data-user="<?php echo $user_info->row()->username; ?>" class="button expand show-more">Show more</a>
				<div class="cssload-container hide">
					<div class="cssload-wheel"></div>
				</div>
			</div>
			<?php endif; ?>
		</div> <!-- user-profile -->
	</div> <!-- col-md-10 -->
</div> <!-- /row -->

<div class="reveal-modal small tab-modal" id="edit-modal" data-reveal aria-labelledby="modal-title" aria-hidden="true" role="dialog"></div>

<script type="text/javascript" src="/js/tab.js"></script>
