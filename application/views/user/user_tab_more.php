<?php
$is_me = false;

if ( $this->ion_auth->logged_in() && $this->ion_auth->user()->row()->id === $user_info->row()->id ) {
	$is_me = true;
}
?>

	<?php foreach ( $tab->result() as $row ): ?>
	<?php $beer_url = $row->beer_id . '-' . url_title( $row->beer_name, '-' ); ?>
	<div class="row small-collapse tab-item" id="tab-<?php echo $row->id; ?>">
		<div class="small-3 medium-2 tab-photo columns">
			<a href="/beer/<?php echo $beer_url; ?>">
				<?php
				if ( $row->beer_image ) {
					$beer_image = $row->beer_image;
				} else {
					$beer_image = '/img/no-beer.png';
				}
				?>
				<img src="<?php echo $beer_image; ?>" />
			</a>
		</div>

		<div class="small-9 medium-7 tab-info columns">
			<h4 class="stronghead"><?php echo $row->beer_name; ?></h4>
			<h6><?php echo $row->brewery_name; ?></h6>

			<?php if ( $row->beer_style ): ?>
			<h6 class="subheader"><?php echo $row->beer_style; ?></h6>
			<?php endif; ?>
		</div>

		<?php if ( $is_me ): // this is the logged in user's tab ?>
		<div class="small-9 small-offset-3 medium-3 medium-offset-0 columns tab-personal">
			<div class="small-6 medium-12 columns tab-rating">
				<a href="/user/edit_form?id=<?php echo $row->id; ?>" data-reveal-id="edit-modal" data-reveal-ajax="true">
					<img src="/styles/img/rating-<?php echo $row->rating; ?>.svg" height="30" width="110"/>
				</a>
			</div>

			<div class="small-6 medium-12 columns tab-notes">
				<a href="javascript:;" class="show-notes"><span class="icon-notebook"></span> Notes</a>
			</div>
		</div>

		<div class="notes-container">
			<div class="notes">
				<?php if ($row->notes): ?>
				<span><?php echo nl2br( $row->notes ); ?></span>

				<?php else: ?>
				<span style="color:rgba(255,255,255,0.5)">No notes for this beer. Click rating to edit.</span>

				<?php endif; ?>
				<a href="javascript:;" class="close-notes">&times;</a>
			</div> <!-- notes -->
		</div> <!-- notes-container -->

		<?php else: // this is not the logged in user's tab ?>

		<div class="small-9 small-offset-3 medium-3 medium-offset-0 columns tab-personal">
			<div class="small-6 medium-12 columns tab-rating">
				<img src="/styles/img/rating-<?php echo $row->rating; ?>.svg" height="30" width="110"/>
			</div>
		</div>

		<?php endif; ?>

		<?php if ( $row->favorite ): ?>
		<span class="tab-fav-flag"></span>
		<?php endif; ?>
	</div>
	<?php endforeach; ?>
