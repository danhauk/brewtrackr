<h3 class="text-center">Oh no!</h3>
<p class="text-center">It looks like you might not be logged in. Try refreshing the page to log in again. Sorry about that! :(</p>

<a class="close-reveal-modal" aria-label="Close">&#215;</a>