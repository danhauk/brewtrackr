<?php
$is_me = false;

if ( $this->ion_auth->logged_in() && $this->ion_auth->user()->row()->id === $user_info->row()->id ) {
	$is_me = true;
}
?>
<div class="row">
	<div class="small-12 medium-10 medium-centered columns">
		<div class="card small-full user-profile">
			<div class="row small-collapse">
				<div class="small-4 medium-2 columns avatar-container">
					<?php if ( $user_info->row()->photo_url ){
						$user_image_url = $user_info->row()->photo_url;
					} else {
						$user_image_url = '/img/no-user.svg';
					} ?>
					<img src="<?php echo $user_image_url; ?>" class="avatar img-responsive" />
				</div>
				<div class="small-8 medium-7 columns bio">
					<h3><?php echo $user_info->row()->username; ?></h3>
					<?php
					if ( $user_info->row()->bio ) {
						echo '<p>' . nl2br( $user_info->row()->bio ) . '</p>';
					}
					?>
				</div>
				<div class="small-12 medium-3 columns tab-summary">
					<div>
						0
						<span>beers</span>
					</div>
					<div>
						0
						<span>favs</span>
					</div>
				</div>
			</div>

			<div class="row small-collapse user-profile-subnav">
				<div class="small-12 medium-6 columns">
					<a href="/<?php echo $username; ?>"<?php if(!isset($_GET['sort']) && !isset($_GET['favorites'])) { echo ' class="active"'; } ?>>Recent</a>
					<a href="?favorites=1"<?php if(isset($_GET['favorites']) == '1') { echo ' class="active"'; } ?>>Favorites</a>
				</div>
				<div class="small-12 medium-6 columns">
					<form class="search-tab-form" action="" method="get">
						<input type="search" name="s" autocorrect="off" autocapitalize="off" placeholder="Search <?php echo $username; ?>'s tab">
						<span class="icon-magnifier"></span>
					</form>
				</div>
			</div>
		</div>

		<?php if ( $is_me ): ?>
			<div class="tab-list tab-empty card text-center">
				<span class="tab-empty-image"></span>
				<h2>What are you drinking?</h2>
				<p>Add your first beer to your tab to start keeping track. Rate it so you can easily find your favorites.</p>
				<a href="/beer/submit" data-reveal-id="submit-modal" data-reveal-ajax="true" class="button secondary radius">Add a beer</a>
			</div>
		<?php else: ?>
			<div class="tab-list tab-empty card text-center">
				<span class="tab-empty-image"></span>
				<h2>Oh no!</h2>
				<p><?php echo $user_info->row()->username; ?> hasn't added anything yet! Buy them a beer next time you see them.</p>
			</div>
		<?php endif; ?>
		</div> <!-- user-profile -->
	</div> <!-- col-md-10 -->
</div> <!-- /row -->

<div class="reveal-modal small tab-modal" id="edit-modal" data-reveal aria-labelledby="modal-title" aria-hidden="true" role="dialog"></div>

<script type="text/javascript" src="/js/tab.js"></script>
