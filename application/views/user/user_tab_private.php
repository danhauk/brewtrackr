<?php
$is_me = false;
?>
<div class="row">
	<div class="small-12 medium-10 medium-centered columns">
			<div class="tab-list tab-empty card text-center">
				<span class="tab-empty-image"></span>
				<h2>This account is private.</h2>
			</div>
		</div> <!-- user-profile -->
	</div> <!-- col-md-10 -->
</div> <!-- /row -->

<div class="reveal-modal small tab-modal" id="edit-modal" data-reveal aria-labelledby="modal-title" aria-hidden="true" role="dialog"></div>

<script type="text/javascript" src="/js/tab.js"></script>
