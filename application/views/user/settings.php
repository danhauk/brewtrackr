<div class="row">
	<div class="medium-8 medium-centered columns">
		<div class="card clearfix">
			<div class="small-12 columns">

				<h6 class="section-heading">Profile</h6>
				<div class="settings-username">
					<label for="username">Username</label>
					<input type="text" disabled="disabled" value="<?php echo $info->row()->username; ?>" id="username"/>

					<div class="settings-avatar avatar" style="background-image:url(<?php echo $info->row()->photo_url; ?>)">
						<div class="avatar-upload-container">
							<span class="avatar-edit">
								<span class="icon-camera"></span>
								<span class="avatar-edit-text">Upload Photo</span>
							</span>
							<form action="user/upload_avatar" id="avatar-form" method="post" enctype="multipart/form-data">
								<input type="file" id="avatar" name="avatar" class="avatar-upload">
							</form>
						</div>
						<div class="cssload-container hide"><div class="cssload-wheel"></div></div>
					</div>
				</div>

				<form action="/user/update" method="post" id="settings-form">
					<label for="email">Email address</label>
					<input type="email" name="email" value="<?php echo $info->row()->email; ?>" id="email"/>

					<label for="bio">Short bio</label>
					<textarea name="bio" class="form-control" rows="2" id="bio"><?php echo $info->row()->bio; ?></textarea>

					<input type="submit" class="button radius settings-submit" value="Save Profile"/>
				</form>

				<h6 class="section-heading" id="password">Change Password</h6>
				<form action="/settings/password" method="post" id="password-form">
					<label for="old">Old Password</label>
					<input type="password" name="old" id="old"/>

					<label for="new">New Password</label>
					<input type="password" name="new" id="new"/>

					<label for="new_confirm">Confirm Password</label>
					<input type="password" name="new_confirm" id="new_confirm"/>

					<input type="submit" class="button radius settings-submit" value="Save Password"/>
				</form>

				<h6 class="section-heading">Export Data</h6>
				<p>Export your BrewTrackr tab as a CSV file to take it with you.</p>
				<a class="button radius" href="/settings/export">Export Tab</a>
			</div> <!-- small-12 -->
		</div> <!-- card -->

		<div class="card clearfix">
			<div class="small-12 columns settings-delete">
				<a href="/user/delete" data-reveal-id="settings-modal" data-reveal-ajax="true" class="link-scary" id="delete-account">Delete my account</a>
			</div>
		</div>
	</div> <!-- col-md-10 -->
</div> <!-- /row -->

<div class="reveal-modal small" id="settings-modal" data-reveal aria-labelledby="modal-title" aria-hidden="true" role="dialog"></div>

<script src="/js/settings.js" type="text/javascript"></script>
