<div class="row comment highlight new small-collapse" id="comment-<?php echo $comment->row()->id; ?>">
	<div class="small-1 columns comment-avatar">
		<a href="/<?php echo $comment->row()->username; ?>"><img src="<?php echo $comment->row()->user_photo; ?>" class="avatar"/></a>
	</div>
	<div class="small-11 columns comment-text">
		<h5 class="stronghead"><?php echo $comment->row()->username; ?></h5>
		<p><?php echo nl2br( $comment->row()->comment ); ?></p>
	</div>
</div>
