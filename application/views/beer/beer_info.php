<div class="row beer-info">
  <div class="large-10 large-centered columns">
    <div class="small-full card">
      <div class="row beer-main-details">
        <div class="small-3 columns beer-img">
          <?php
          if ( isset( $info['images']['medium'] ) ) {
            $img_url = $info['images']['medium'];
          }
          elseif ( isset( $info['labels']['medium'] ) ) {
            $img_url = $info['labels']['medium'];
          }
          elseif ( isset( $info['img'] ) ) {
            $img_url = $info['img'];
          }
          else {
            $img_url = '/img/no-beer.png';
          }
          ?>
          <img src="<?php echo $img_url; ?>" />
        </div>
        <div class="small-9 medium-6 columns">
          <h3 class="stronghead"><?php echo $beer_name; ?></h3>
          <?php if ( isset( $brewery_name ) ): ?>
            <h6><?php echo $brewery_name; ?></h6>
          <?php endif; ?>

          <?php if ( $style ): ?>
          <h6 class="subheader"><?php echo $style; ?></h6>
          <?php endif; ?>

          <div class="show-for-medium-up">
            <ul class="beer-secondary-details">
            <?php
              echo ( isset($info['abv']) ? '<li>'.$info['abv'].'% ABV</li>' : '<li>No ABV</li>' );
              echo ( isset($info['ibu']) ? '<li>'.$info['ibu'].' IBU</li>' : '<li>No IBU</li>' );
            ?>
          </ul>

            <?php if ( isset( $info['description'] ) ): ?>
            <p class="description">
              <?php
              if ( strlen($info['description']) > 200 ) {
                $desc_truncated = substr( $info['description'], 0, 199 );
                $full_description = substr( $info['description'], 199 );

                echo nl2br($desc_truncated) . '<span class="ellipsis">... </span>';
                echo '<span class="full-desc hide">' . nl2br($full_description) . '</span>';
                echo '<a href="#" class="beer-desc-more">Show more</a>';
              }
              else {
                echo $info['description'];
              }
              ?>
            </p>
            <?php endif; ?>
          </div>
        </div>
        <div class="small-12 medium-3 columns">
          <?php
          $add_id = 'id='.$beer_id;
          $add_name = 'beer_name='.urlencode( $beer_name );
          $add_brewery = 'brewery_name='.(isset($brewery_name) ? urlencode($brewery_name) : '');
          $add_style = 'style='.(isset($style) ? urlencode($style) : '');
          $add_image = 'beer_image='.$img_url;
          $add_description = 'beer_description='.(isset($info['description']) ? urlencode($info['description']) : '');
          $add_abv = 'abv='.(isset($info['abv']) ? $info['abv'] : null);
          $add_ibu = 'ibu='.(isset($info['ibu']) ? $info['ibu'] : null);
          $add_form_params = $add_id.'&'.$add_name.'&'.$add_brewery.'&'.$add_style.'&'.$add_image.'&'.$add_description.'&'.$add_abv.'&'.$add_ibu;
          ?>

          <?php if ( $is_in_tab ): ?>
          <a href="javascript:;" class="button outline radius expand tab-action remove-from-tab" data-beerId="<?php echo $beer_id; ?>" data-params="<?php echo $add_form_params; ?>">Remove from Tab</a>

          <?php elseif ( $this->ion_auth->logged_in() ): ?>
          <a href="/user/add_form?<?php echo $add_form_params; ?>" class="button radius expand tab-action add-to-tab" data-reveal-id="add-modal" data-beerId="<?php echo $beer_id; ?>" data-params="<?php echo $add_form_params; ?>" data-reveal-ajax="true">Add to Tab</a>

          <?php else: ?>
          <a href="/signup" data-reveal-id="signup-modal" data-reveal-ajax="true" class="button radius expand tab-action">Add to Tab</a>

          <?php endif; ?>
        </div>
      </div> <!-- /beer-main-details -->

      <div class="row hide-for-medium-up">
        <div class="small-12 columns">
          <ul class="beer-secondary-details">
            <?php
              echo ( isset($info['abv']) ? '<li>'.$info['abv'].'% ABV</li>' : '<li>No ABV</li>' );
              echo ( isset($info['ibu']) ? '<li>'.$info['ibu'].' IBU</li>' : '<li>No IBU</li>' );
            ?>
          </ul>

          <?php if ( isset( $info['description'] ) ): ?>
          <p class="description">
            <?php
            if ( strlen($info['description']) > 200 ) {
              $desc_truncated = substr( $info['description'], 0, 199 );
              $full_description = substr( $info['description'], 199 );

              echo nl2br($desc_truncated) . '<span class="ellipsis">... </span>';
              echo '<span class="full-desc hide">' . nl2br($full_description) . '</span>';
              echo '<a href="#" class="beer-desc-more">Show more</a>';
            }
            else {
              echo $info['description'];
            }
            ?>
          </p>
          <?php endif; ?>
        </div> <!-- /small-12 -->
      </div> <!-- /row -->
    </div> <!-- /card -->

    <div class="small-full card" id="comments">
      <h4>Reviews and Comments</h4>

      <?php if ( $comments->num_rows() > 0 ): ?>
        <?php foreach ( $comments->result() as $comment ): ?>
        <?php if ( $comment->user_photo ){
          $user_image_url = $comment->user_photo;
        } else {
          $user_image_url = '/img/no-user.svg';
        } ?>
        <div class="row comment small-collapse" id="comment-<?php echo $comment->id; ?>">
          <div class="small-2 medium-1 columns comment-avatar">
            <a href="/<?php echo $comment->username; ?>"><img src="<?php echo $user_image_url; ?>" class="avatar"/></a>
          </div>
          <div class="small-10 medium-11 columns comment-text">
            <h5 class="stronghead"><?php echo $comment->username; ?></h5>
            <p><?php echo nl2br( $comment->comment ); ?></p>
            <?php
              if( $this->ion_auth->logged_in() && ( $comment->user_id === $this->ion_auth->user()->row()->id || $this->ion_auth->user()->row()->username === 'danhauk' ) ): ?>
              <p><a href="/review/delete?id=<?php echo $comment->id; ?>" data-reveal-id="add-modal" data-reveal-ajax="true" class="comment-delete">Remove public review</a></p>
            <?php endif; ?>
          </div>
        </div>
        <?php endforeach; ?>
      <?php else: ?>
      <div class="row comment" id="comments-empty">
        <div class="small-12 columns">
          <p>No one has reviewed this yet. Add it to your tab to make a comment.</p>
        </div>
      </div>
      <?php endif; ?>
    </div>
  </div> <!-- /small-12 -->

</div> <!-- /beer-info -->

<?php if ( $this->ion_auth->logged_in() ): ?>
<div class="reveal-modal small tab-modal" id="add-modal" data-reveal aria-labelledby="modal-title" aria-hidden="true" role="dialog"></div>
<?php endif; ?>
