<h4>Remove this public review?</h4>
<p>Your review will still be available as a private note in your tab.</p>

<a class="button radius small button-scary" id="delete-comment-confirm" data-review-id="<?php echo $review_id; ?>">Remove my public review</a>
<a class="button tertiary radius small close-modal" id="close-modal">Cancel</a>

<a class="close-reveal-modal" aria-label="Close">&#215;</a>

<script type="text/javascript" src="/js/vendor/jquery.js"></script>
<script src="/js/foundation.min.js"></script>
<script type="text/javascript">
$("#close-modal").on("click", function() {
  $('#add-modal').foundation('reveal', 'close');
});

$('#delete-comment-confirm').on('click', function(e) {
  e.preventDefault();

  var reviewId = $(this).data('review-id');
  var postData = {
    reviewId: reviewId
  };

  $.ajax({
    type: "POST",
    url: '/review/do_delete',
    data: postData,
    success: function(response){
      if (response == 'success') {
        var notification = '<div class="alert-box success top-notify"><span class="icon icon-check"></span><span class="text">Public review removed.</span></div>';

        var reviewRow = '#comment-'+reviewId;
        $(reviewRow).remove();
      }
      else {
        var notification = '<div class="alert-box error top-notify"><span class="icon icon-close"></span><span class="text">'+response+'</span></div>';
      }

      $('#add-modal').foundation('reveal', 'close');

      $('body').append(notification);
      setTimeout(function() {
        $('.top-notify').delay(4000).slideUp(200);
      });
    },
    error: function() {
      var notification = '<div class="alert-box error">This review couldn\'t be removed. Please refresh the page and try again.</div>';
        
      $('body').append(notification);
      setTimeout(function() {
        $('.top-notify').delay(4000).slideUp(200);
      });
    }
  });
});
</script>