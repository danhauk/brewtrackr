<form action="/user/add_to_tab" method="post" enctype="multipart/form-data" id="submit-beer-form">
	<div class="row" id="submit-container">
	  <h3 class="text-center modal-title">Add a beer</h3>
    <div class="row">
      <div class="small-12 columns">
        <label for="submit_name" class="auth-modal-label">Beer Name</label>
        <input type="text" name="submit_name" id="submit_name" autocapitalize="words" autocorrect="off" autocomplete="off" required />
				<div class="alert-box error hide" id="beer-error" data-alert></div>
      </div>
    </div>

    <div class="row">
      <div class="small-12 columns">
        <label for="submit_brewery" class="auth-modal-label">Brewery Name</label>
        <input type="text" name="submit_brewery" id="submit_brewery" autocapitalize="words" autocorrect="off" autocomplete="off" required />
				<div class="alert-box error hide" id="brewery-error" data-alert></div>
      </div>
    </div>

		<div class="row">
			<div class="small-12 columns">
				<a href="javascript:;" id="submit-show-more-detail" class="submit-form--show-more-link">Add ABV, IBU, style and description</a>
			</div>
		</div>

		<div id="submit-more-detail" class="hide">
	    <div class="row">
	      <div class="small-6 columns">
	        <p>
	          <label for="submit_abv" class="auth-modal-label">ABV</label>
	          <input type="text" name="submit_abv" id="submit_abv" />
	        </p>
	      </div>
	      <div class="small-6 columns">
	        <p>
	          <label for="submit_ibu" class="auth-modal-label">IBU</label>
	          <input type="text" name="submit_ibu" id="submit_ibu" />
	        </p>
	      </div>
	    </div>

	    <div class="row">
	      <div class="small-12 columns">
	        <label for="submit_style" class="auth-modal-label">Style</label>
					<div class="autocomplete-container" id="style-autocomplete-container">
						<input type="text" id="submit_style" value="" autocorrect="off" autocomplete="off" placeholder="Search for a style" />
						<div class="autocomplete hide" id="style_autocomplete"></div>
						<input type="hidden" name="submit_style_id" id="submit_style_id" value="" />
	        </div>
					<div class="alert-box error hide" id="style-error" data-alert></div>
	      </div>
	    </div>

	    <div class="row">
	      <div class="small-12 columns">
	        <p>
	          <label for="submit_description" class="auth-modal-label">Commercial Description</label>
	          <textarea rows="6" name="submit_description" id="submit_description"></textarea>
	        </p>
	      </div>
	    </div>
		</div>

		<div class="alert-box error hide" id="submit-error" data-alert></div>

    <p>
			<input type="hidden" id="submit_user_id" name="user_id" value="<?php echo $user_id; ?>" />
			<a href="javascript:;" class="button radius expand" id="submit-beer-continue">Continue to rating and notes</a>
		</p>
	</div>

	<!-- Add to tab -->
	<div class="hide" id="add-container">
    <h3 class="text-center">Rate <span id="add-beer-name">this beer</span></h3>

		<?php if ( $premium_user ): ?>
    <select id="rating" name="rating" class="premium">
        <option value=""></option>
        <option value=".5">.5</option>
        <option value="1">1</option>
        <option value="1.5">1.5</option>
        <option value="2">2</option>
        <option value="2.5">2.5</option>
        <option value="3">3</option>
        <option value="3.5">3.5</option>
        <option value="4">4</option>
        <option value="4.5">4.5</option>
        <option value="5">5</option>
    </select>
    <?php else: ?>
    <select id="rating" name="rating">
        <option value=""></option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
    </select>
    <?php endif; ?>

		<div class="comments">
			<textarea name="notes" id="notes" rows="3" placeholder="Optional tasting notes"></textarea>
			<div class="photo-input">
				<input type="file" id="addphoto" name="addphoto" class="photo-upload">
				<a href="javascript:;" class="photo-upload-button">
					<span class="icon-camera"></span>
					<span class="photo-upload-text">Add photo</span>
				</a>
			</div>
			<div class="photo-preview"></div>
		</div>
		<div id="photo-upload-error" class="alert-box error hide"></div>

    <div class="fav clearfix">
      <div class="switch tiny right">
        <input class="switch-input" id="favorite" type="checkbox" name="favorite">
        <label class="switch-paddle" for="favorite">
          <span class="show-for-sr">Favorite</span>
        </label>
      </div>
      <label class="left favorite-label" for="favorite">Save as a favorite</label>
    </div>

		<div id="edit-tab-error" class="alert-box error hide"></div>

    <input type="submit" class="button submit radius expand right" id="edit-tab-submit" value="Save To Tab"/>
	</div>
</form>

<a class="close-reveal-modal" aria-label="Close">&#215;</a>

<script type="text/javascript" src="/js/jquery.barrating.min.js"></script>
<script type="text/javascript" src="/js/vendor/fuse.min.js"></script>
<script type="text/javascript" src="/js/tab.js"></script>
<script type="text/javascript">
$(function() {
    var half_ratings = false;

    if ( $('#rating').hasClass('premium') ) {
        $('#rating').barrating({
            showSelectedRating: false,
            wrapperClass: 'half-ratings'
        });
    }
    else {
        $('#rating').barrating({
            showSelectedRating: false
        });
    }
});
</script>
