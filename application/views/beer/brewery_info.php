<div class="row">
	<div class="small-12 columns">
		<div class="panel card">
			<div class="row">
				<div class="small-12 medium-6 medium-push-6 columns ">
					<?php 
					if ( isset( $brewery_info['images'] ) ) {
						$img_url = $brewery_info['images']['large'];
					}
					else {
						$img_url = 'http://placehold.it/256/256';
					}
					?>
					<img src="<?php echo $img_url; ?>" />
				</div>
				<div class="small-12 medium-6 medium-pull-6 columns">
					<h2><?php echo $brewery_info['name']; ?></h2>
					<h4>Boston, Massachussetts</h4>
					<p><strong>Established:</strong> <?php echo $brewery_info['established']; ?><br>
						<strong>Website:</strong> <a href="<?php echo $brewery_info['website']; ?>"><?php echo $brewery_info['website']; ?></a></p>
				</div>
			</div> <!-- /row -->

			<?php if ( isset( $brewery_info['description'] ) ): ?>
			<div class="row">
				<div class="small-12 columns">
					<hr/>
					<p><?php echo substr( nl2br ( $brewery_info['description'] ), 0, 300 ) . '...'; ?> <a href="#" class="label secondary">show more</a></p>
				</div>
			</div>
			<?php endif; ?>

		</div> <!-- /card -->
	</div> <!-- /medium-9 -->

	<div class="small-12 columns">
		<div class="row">
		<?php foreach ( $brewery_beers as $beer ): ?>
			<div class="small-12 medium-6 large-3 columns" style="overflow:hidden;">
				<div class="card">
					<?php 
					if ( isset( $beer['labels'] ) ) {
						$img_url = $beer['labels']['medium'];
					}
					else {
						$img_url = 'http://placehold.it/64/64';
					}
					?>
					<a href="/beer/<?php echo $beer['id']; ?>"><img src="<?php echo $img_url; ?>"/></a>
					<h5 style="white-space:nowrap; text-overflow:ellipsis"><a href="/beer/<?php echo $beer['id']; ?>"><?php echo $beer['name']; ?></a></h5>
					<?php if ( isset( $beer['style']['name'] ) ): ?>
					<small style="white-space:nowrap; text-overflow:ellipsis"><?php echo $beer['style']['name']; ?></small>
					<?php endif; ?>
				</div>
			</div>
		<?php endforeach; ?>
		</div>
	</div>
</div>