<div class="fourohfour fourohfour-beer">
  <div class="row fourohfour-content">
    <div class="small-12 medium-8 large-6 medium-centered columns text-center">
      <h1 class="stronghead fourohfour-heading">Error 404</h2>
      <h3 class="fourohfour-subheading">Oh no! It looks like you're trying to find a beer that doesn't exist.</h3>
      <p class="fourohfour-secondary">Try searching for your beer:</p>
      <form action="/search" method="get">
        <div class="row collapse">
          <div class="small-12 medium-8 columns">
            <input type="search" name="q" autocomplete="off" autocorrect="off" autocapitalize="off">
          </div>
          <div class="small-12 medium-4 columns">
            <input type="submit" class="button radius expand postfix" value="Search">
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
