<div class="row">
        <div class="small-12 columns full-page-document">
            <h1>BrewTrackr Privacy Policy</h1>
            
            <div class="content">
                <h2>Your privacy is critically important to us. At BrewTrackr we have a few fundamental principles:</h2>
                <ul>
                    <li>We don't ask you for personal information unless we truly need it. (We couldn't care less about your gender or job title.)</li>
                    <li>We don't share your personal information with anyone except to comply with the law, develop our products, or protect our rights.</li>
                    <li>We don't store personal information on our servers unless required for the on-going operation of one of our services.</li>
                </ul>
                <p>Below is our privacy policy which incorporates these goals:</p>
                <p>If you have questions about deleting or correcting your personal data please <a href="/contact">contact us</a>.</p>
                <p>It is BrewTrackr's policy to respect your privacy regarding any information we may collect while operating our website.</p>
                
                <h3>Website Visitors</h3>
                <p>Like most website operators, BrewTrackr collects non-personally-identifying information of the sort that web browsers and servers typically make available, such as the browser type, language preference, referring site, and the date and time of each visitor request. BrewTrackr's purpose in collecting non-personally identifying information is to better understand how BrewTrackr's visitors use its website. From time to time, BrewTrackr may release non-personally-identifying information in the aggregate, e.g., by publishing a report on trends in the usage of its website.</p>
                <p>BrewTrackr also collects potentially personally-identifying information like Internet Protocol (IP) addresses for logged in users. BrewTrackr only discloses logged in user IP addresses under the same circumstances that it uses and discloses personally-identifying information as described below.</p>
                
                <h3>Gathering of Personally-Identifying Information</h3>
                <p>Certain visitors to BrewTrackr's websites choose to interact with BrewTrackr in ways that require BrewTrackr to gather personally-identifying information. The amount and type of information that BrewTrackr gathers depends on the nature of the interaction. For example, we ask visitors who sign up to provide a username and email address. Those who engage in transactions with BrewTrackr are asked to provide additional information, including as necessary the personal and financial information required to process those transactions. In each case, BrewTrackr collects such information only insofar as is necessary or appropriate to fulfill the purpose of the visitor's interaction with BrewTrackr. BrewTrackr does not disclose personally-identifying information other than as described below. And visitors can always refuse to supply personally-identifying information, with the caveat that it may prevent them from engaging in certain website-related activities.</p>
                
                <h3>Aggregated Statistics</h3>
                <p>BrewTrackr may collect statistics about the behavior of visitors to its website. For instance, BrewTrackr may monitor the most popular pages on the website. BrewTrackr may display this information publicly or provide it to others. However, BrewTrackr does not disclose personally-identifying information other than as described below.</p>
                
                <h3>Protection of Certain Personally-Identifying Information</h3>
                <p>BrewTrackr discloses potentially personally-identifying and personally-identifying information only to those of its employees, contractors and affiliated organizations that (i) need to know that information in order to process it on BrewTrackr's behalf or to provide services available at BrewTrackr's website, and (ii) that have agreed not to disclose it to others. Some of those employees, contractors and affiliated organizations may be located outside of your home country; by using BrewTrackr's website, you consent to the transfer of such information to them. BrewTrackr will not rent or sell potentially personally-identifying and personally-identifying information to anyone. Other than to its employees, contractors and affiliated organizations, as described above, BrewTrackr discloses potentially personally-identifying and personally-identifying information only in response to a subpoena, court order or other governmental request, or when BrewTrackr believes in good faith that disclosure is reasonably necessary to protect the property or rights of BrewTrackr, third parties or the public at large. If you are a registered user of the BrewTrackr website and have supplied your email address, BrewTrackr may occasionally send you an email to tell you about new features, solicit your feedback, or just keep you up to date with what's going on with BrewTrackr and our products. However, we expect to keep this type of email to a minimum. If you send us a request (for example via a support email or via one of our feedback mechanisms), we reserve the right to publish it in order to help us clarify or respond to your request or to help us support other users. BrewTrackr takes all measures reasonably necessary to protect against the unauthorized access, use, alteration or destruction of potentially personally-identifying and personally-identifying information.</p>
                
                <h3>Cookies</h3>
                <p>A cookie is a string of information that a website stores on a visitor's computer, and that the visitor's browser provides to the website each time the visitor returns. BrewTrackr uses cookies to help BrewTrackr identify and track visitors, their usage of BrewTrackr website, and their website access preferences. BrewTrackr visitors who do not wish to have cookies placed on their computers should set their browsers to refuse cookies before using BrewTrackr's website, with the drawback that certain features of BrewTrackr's website may not function properly without the aid of cookies.</p>
                
                <h3>Business Transfers</h3>
                <p>If BrewTrackr, or substantially all of its assets were acquired, or in the unlikely event that BrewTrackr goes out of business or enters bankruptcy, user information would be one of the assets that is transferred or acquired by a third party. You acknowledge that such transfers may occur, and that any acquirer of BrewTrackr may continue to use your personal information as set forth in this policy.</p>
                
                <h3>Ads</h3>
                <p>Ads appearing on our website may be delivered to users by advertising partners, who may set cookies. These cookies allow the ad server to recognize your computer each time they send you an online advertisement to compile information about you or others who use your computer. This information allows ad networks to, among other things, deliver targeted advertisements that they believe will be of most interest to you. This Privacy Policy covers the use of cookies by BrewTrackr and does not cover the use of cookies by any advertisers.</p>
                
                <h3>Privacy Policy Changes</h3>
                <p>Although most changes are likely to be minor, BrewTrackr may change its Privacy Policy from time to time, and in BrewTrackr's sole discretion. BrewTrackr encourages visitors to frequently check this page for any changes to its Privacy Policy. Your continued use of this site after any change in this Privacy Policy will constitute your acceptance of such change.</p>
                
                <h3>Last Modification</h3>
                <p>This Privacy Policy was last modified on September 15, 2015.</p>
                <p>This Privacy Policy is available under a <a href="http://creativecommons.org/licenses/by-sa/2.5/">Creative Commons Sharealike</a> license, and was repurposed for BrewTrackr based on <a href="http://automattic.com/">Automattic's</a> Privacy Policy.</p>
            </div> <!-- /content -->
        </div> <!-- /full-page-document -->
</div> <!-- /main-content -->