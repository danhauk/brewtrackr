<div class="bleed-header donate left-align">
	<div class="row">
		<div class="medium-8 medium-centered columns">
			<h2>Help keep the lights on.</h2>
			<h4>Support development of new features, mobile apps, and offset operation costs with a generous one-time donation.</h4>
			<p>Hi! My name is Dan Hauk. BrewTrackr is a passion project completely paid for out of my own pocket. By supporting the site you can ensure it will stick around for a while and improve with added features and native apps.</p>
			<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
				<input type="hidden" name="cmd" value="_s-xclick">
				<input type="hidden" name="hosted_button_id" value="3RDNKZEHWND9N">
				<input type="submit" class="button radius tertiary" value="Donate securely via PayPal"/>
				<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
			</form>
		</div>
	</div>
</div>

<div class="reveal-modal small" id="settings-modal" data-reveal aria-labelledby="modal-title" aria-hidden="true" role="dialog"></div>
