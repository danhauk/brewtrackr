<div class="bleed-header donate left-align">
	<div class="row">
		<div class="medium-8 medium-centered columns">
			<h2>Cheers!</h2>
			<h4>I really appreciate your support for BrewTrackr and can't thank you enough.
				<?php if ( isset($amount) ) {
					echo "Your donation of $${amount} will really help.";
				} ?>
			</h4>
			<?php if ( isset($amount) ): ?>
				<p>
					As a way to show my gratitude, I'd like to send you some free stickers. If you'd like to get a couple in the mail, just let me know where to send them.<br><br>
					<a href="https://danhauk.typeform.com/to/KzIaRg" class="button radius tertiary" target="_blank">Get Stickers!</a>
				</p>
			<?php endif; ?>
		</div>
	</div>
</div>

<div class="reveal-modal small" id="settings-modal" data-reveal aria-labelledby="modal-title" aria-hidden="true" role="dialog"></div>

<script type="text/javascript">
<?php if (ENVIRONMENT == 'development'): ?>
var publicKey = 'pk_test_vKonLo2d5ciHFR0dKflencMw';
<?php else: ?>
var publicKey = 'pk_live_ay9om4iRBzLDWtNWPq8ubQBM';
<?php endif; ?>
var stripe = Stripe( publicKey );
var elements = stripe.elements();

var cardNumber = elements.create('cardNumber');
var cardExpiry = elements.create('cardExpiry');
var cardCvc = elements.create('cardCvc');
var postalCode = elements.create('postalCode');

cardNumber.mount('#card-number');
cardExpiry.mount('#card-expire');
cardCvc.mount('#card-cvc');
postalCode.mount('#card-postal');

// Create a token or display an error the form is submitted.
var form = document.getElementById('payment-form');
form.addEventListener('submit', function(event) {
  event.preventDefault();

  stripe.createToken(cardNumber).then(function(result) {
    if (result.error) {
      // Inform the user if there was an error
      var errorElement = document.getElementById('card-errors');
      errorElement.textContent = result.error.message;
    } else {
      // Send the token to your server
      stripeTokenHandler(result.token);
    }
  });
});

function stripeTokenHandler(token) {
  // Insert the token ID into the form so it gets submitted to the server
  var form = document.getElementById('payment-form');
  var hiddenInput = document.createElement('input');
  hiddenInput.setAttribute('type', 'hidden');
  hiddenInput.setAttribute('name', 'stripeToken');
  hiddenInput.setAttribute('value', token.id);
  form.appendChild(hiddenInput);

  // Submit the form
  form.submit();
}
</script>
