<div class="value-prop">
	<div class="row">
		<div class="small-12 medium-8 medium-centered columns">
			<h2>Your personal beer list.</h2>
			<h4>Easily keep track of everything you drink. Quickly find your favorites with ratings and tasting notes.</h4>
			<a href="/auth/signup" data-reveal-id="signup-modal" data-reveal-ajax="true" class="button round large get-started" id="get-started-top">Start your tab</a>
		</div>
	</div>
</div>

<div class="homepage-explain">
    <div class="row medium-collapse homepage-feature">
      <div class="homepage-feature-content feature-image-right">
        <img src="/img/landing/landing-track-2x.png">
      </div>
      <div class="homepage-feature-content homepage-feature-text">
        <h2 class="feature">Easily track your beer</h2>
        <p>As a beer enthusiast, trying to remember the variety of beer you drink can be daunting. Forget clumsy methods like spreadsheets or notebooks. BrewTrackr makes it simple to keep tabs on your beer.</p>
      </div>
    </div>

    <div class="row medium-collapse homepage-feature">
      <div class="homepage-feature-content feature-image-left">
        <img src="/img/landing/landing-favorites-2x.png" class="add-shadow">
      </div>
      <div class="homepage-feature-content homepage-feature-text">
        <h2 class="feature">Save your favorites</h2>
        <p>What was the name of that delicious IPA? Other apps make it hard. BrewTrackr makes it easy. Your favorites are always a click away so you never forget again.</p>
      </div>
    </div>

    <div class="row medium-collapse homepage-feature">
      <div class="homepage-feature-content feature-image-right">
        <img src="/img/landing/landing-notes-demo.gif" class="add-shadow">
      </div>
      <div class="homepage-feature-content homepage-feature-text">
        <h2 class="feature">Add personal notes</h2>
        <p>Write down personal tasting notes along with your rating to remind yourself how you liked it. Keep these notes to yourself or share them with the community.</p>
      </div>
    </div>

    <div class="row medium-collapse homepage-feature">
      <div class="homepage-feature-content feature-image-mobile">
        <img src="/img/landing/landing-mobile-2x.png">
      </div>
      <div class="homepage-feature-content homepage-feature-text">
        <h2 class="feature">Take it with you</h2>
        <p>An app to track your beer isn't any good if you can't use it while out at the bar with friends. With a fully responsive and snappy web interface, BrewTrackr works perfectly on your smartphone.</p>
      </div>
    </div>
</div>

<div class="homepage-cta">
  <div class="row">
    <div class="small-12 columns text-center">
      <h3>What are you waiting for?</h3>
      Open up a tab now and start remembering your beer!
      <p><a href="/auth/signup" data-reveal-id="signup-modal" data-reveal-ajax="true" class="button large round" id="get-started-bottom">Get started</a></p>
    </div>
  </div>
</div>
