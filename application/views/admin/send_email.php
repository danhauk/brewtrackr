<div>
  <h2 class="text-center modal-title">Send an email to everyone</h2>
</div>

<div class="row">
  <div class="large-8 large-centered columns">
		<form action="/admin/send_email" method="post">
			<p>
				<label>Subject</label>
				<input type="text" name="subject" />
			</p>

			<p>
				<label>Content</label>
				<textarea name="content" rows="30"></textarea>
			</p>

			<p>
				<input type="submit" class="button radius" value="Send" />
			</p>
		</form>
	</div>
</div>
