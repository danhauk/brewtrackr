<div>
  <h2 class="text-center modal-title">Migrate a beer submission</h2>
</div>

<div class="row">
  <div class="large-8 large-centered columns">
    <div class="row">
      <div class="small-12 columns">
        <form action="" method="post" id="submit-beer-form">
          <p>
            <label for="submit_uid" class="auth-modal-label">UID</label>
            <input type="text" name="submit_uid" id="submit_uid" />
          </p>

          <p>
            <label for="submit_createdAt" class="auth-modal-label">createdAt</label>
            <input type="text" name="submit_createdAt" id="submit_createdAt" />
          </p>

          <hr />

          <div class="row">
            <div class="small-12 columns">
              <label for="submit_name" class="auth-modal-label">Beer Name</label>
              <input type="text" name="submit_name" id="submit_name" autocapitalize="words" autocorrect="off" autocomplete="off" />
            </div>
          </div>

          <div class="row">
            <div class="small-12 columns">
              <label for="submit_brewery" class="auth-modal-label">Brewery Name</label>
              <div class="autocomplete-container">
                <input type="text" name="submit_brewery" id="submit_brewery" autocapitalize="words" autocorrect="off" autocomplete="off" />
                <input type="hidden" name="submit_brewery_id" id="submit_brewery_id" value="" />
                <div class="autocomplete hide" id="brewery_autocomplete"></div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="small-6 columns">
              <p>
                <label for="submit_abv" class="auth-modal-label">ABV</label>
                <input type="text" name="submit_abv" id="submit_abv" />
              </p>
            </div>
            <div class="small-6 columns">
              <p>
                <label for="submit_ibu" class="auth-modal-label">IBU</label>
                <input type="text" name="submit_ibu" id="submit_ibu" />
              </p>
            </div>
          </div>

          <div class="row">
            <div class="small-12 columns">
              <p>
                <label for="submit_style" class="auth-modal-label">Style</label>
                <select name="submit_style" id="submit_style" class="chosen-select" data-placeholder="Choose a style">
                  <option></option>
                  <?php foreach($styles as $style) {
                    echo "<option value={$style['id']}>{$style['name']}</option>";
                  } ?>
                </select>
              </p>
            </div>
          </div>

          <div class="row">
            <div class="small-12 columns">
              <p>
                <label for="submit_description" class="auth-modal-label">Description</label>
                <textarea rows="6" name="submit_description" id="submit_description"></textarea>
              </p>
            </div>
          </div>

          <p><input type="submit" class="button radius expand" id="submit-beer-submit" value="Submit" /></p>
        </form>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript" src="/js/vendor/chosen/chosen.jquery.min.js"></script>
<script type="text/javascript" src="/js/tab.js"></script>
<script type="text/javascript" src="/js/search.js"></script>
<script type="text/javascript">
$(document).ready(function() {
  $('.chosen-select').chosen({width: "100%"});
});
</script>

<script type="text/javascript">
$(document).on('submit', '#submit-beer-form', function(e) {
  e.preventDefault();
  $('#submit-beer-submit').attr({
    value: 'Submitting...',
    disabled: 'disabled'
  });

  var postData = {
    beer_name: $(this).find('#submit_name').val(),
    beer_image: '/img/no-beer.png',
    brewery: $('#submit_brewery_id').val(),
    brewery_name: $(this).find('#submit_brewery').val(),
    abv: $(this).find('#submit_abv').val(),
    ibu: $(this).find('#submit_ibu').val(),
    styleId: $(this).find('#submit_style').val(),
    style: $(this).find('#submit_style option:selected').text(),
    description: $(this).find('#submit_description').val(),
    uid: $(this).find('#submit_uid').val(),
    createdAt: $(this).find('#submit_createdAt').val()
  };

  getUserId(postData.uid, function(userId) {
    if (userId) {
      postData.user_id = userId;

      $.ajax({
        type: "POST",
        url: "/admin/do_submit",
        data: postData,
        success: function(response){
          var res = JSON.parse(response);

          console.log(res);

          if (res.status == 'success') {
            postData.beer_id = res.data.id;

            addTabFirebase(postData);

            $('#submit-beer-submit').removeAttr('disabled').attr('value', 'Submit');
            $('#submit-beer-form input, #submit-beer-form textarea').val('');
          }
        }
      });
    }
  });

  function getUserId(uid, callback) {
    var authData = firebase.getAuth();
    var user_id;

    if (authData.uid !== 'aeffb823-fdcf-4ea6-bf2e-f3e9eb2b9038') {
      return;
    }

    var userRef = firebase.child('userProfile/' + uid);

    userRef.once('value', function(snapshot) {
      var userId = snapshot.val().userId;
      this.callback(userId);
    }, {callback: callback});
  }

  function addTabFirebase(postData) {
    var authData = firebase.getAuth();
    if (authData.uid !== 'aeffb823-fdcf-4ea6-bf2e-f3e9eb2b9038') {
      return;
    }

    var tabData = {
      beerId: postData.beer_id,
      beerImage: postData.beer_image,
      beerName: postData.beer_name,
      beerStyle: postData.style,
      breweryName: postData.brewery_name,
      createdAt: Firebase.ServerValue.TIMESTAMP,
      favorite: 0,
      notes: '',
      rating: 0
    };

    var tabRef = firebase.child('tab/' + postData.uid);

    tabRef.once('value', function(snapshot) {
      var existsInTab = false;

      snapshot.forEach(function(beerSnapshot) {
        var beerId = beerSnapshot.child('beerId').val();

        if (beerId === postData.beer_id) {
          existsInTab = true;
          return true;
        }
      });

      if (!existsInTab) {
        tabRef.push(tabData);
      }
    });
  }
});
</script>