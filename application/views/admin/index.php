<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title><?php if ( isset($title) ) { echo $title . ' - '; } ?>BrewTrackr</title>
    
    <meta name="description" content="Where craft beer enthusiasts keep tabs on the variety of beer they drink. Easily find your favorites. Rate what you try and write personal tasting notes to share publicly or keep to yourself.">
    <meta name="keywords" content="beer, craft, brew, micro, track, web app, social, taste, rate, review, recommend, discover" />
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="shortcut icon" href="/img/favicon.ico" />
    
    <!-- Stylin' -->
    <link href='https://fonts.googleapis.com/css?family=Fira+Sans:400,500,300' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/styles/foundation.min.css" />
    <link rel="stylesheet" href="/styles/simple-line-icons.css" />
    <link rel="stylesheet" href="/styles/display.css" />
    <link rel="stylesheet" href="/brewadmin/css/styles.css" />

    <!-- Icons -->
    <link rel="icon" sizes="192x192" href="/img/android-icon-192.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/img/app-icon-180.png">
    <link rel="apple-touch-icon" sizes="167x67" href="/img/app-icon-167.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/img/app-icon-152.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/img/app-icon-120.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/img/app-icon-76.png">

    <script src="https://cdn.firebase.com/js/client/2.4.0/firebase.js"></script>
    <script type="text/javascript">var firebase = new Firebase("https://brwtk.firebaseio.com/");</script>
</head>
<body>

  <div id="admin"></div>

  <script type="text/javascript" src="/brewadmin/js/app.js"></script>

</body>
</html>