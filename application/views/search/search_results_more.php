
		<?php for ($i = 0; $i < count($results['data']); $i++): ?>
			<?php
			$result = $results['data'][$i];
			$beer_url = $result['id'] . '-' . url_title( $result['name'], '-' );
			?>

			<div class="search-result clearfix">
				<div class="small-3 medium-2 columns results-thumbnail">
					<?php
						if ( isset( $result['images']['medium'] ) ) {
							$img_url = $result['images']['medium'];
						}
						elseif ( isset( $result['labels']['medium'] ) ) {
							$img_url = $result['labels']['medium'];
						}
						else {
							$img_url = '/img/no-beer.png';
						}
					?>
					<a href="/beer/<?php echo $beer_url; ?>"><img src="<?php echo $img_url; ?>" /></a>
				</div>
				<div class="small-9 medium-10 columns results-info">
					<div class="medium-8 columns">
				    <a href="/beer/<?php echo $beer_url; ?>">
				    	<h4><?php echo $result['name']; ?></h4>

				    	<?php if( isset($result['breweries'][0]) ): ?>
				    	<h6><?php echo $result['breweries'][0]['name']; ?></h6>
				    	<?php endif; ?>

				    	<span class="results-meta">
					    	<?php
					    	echo ( isset($result['style']['name']) ? '<span>'.$result['style']['name'].'</span>' : '' );
					    	echo ( isset($result['abv']) ? '<span>'.$result['abv'].'% ABV</span>' : '' );
					    	echo ( isset($result['ibu']) ? '<span>'.$result['ibu'].' IBU</span>' : '' );
					    	?>
					    </span>
						</a>
					</div>
					<div class="medium-4 columns">
						<?php
							$add_id = 'id='.$result['id'];
							$add_name = 'beer_name='.urlencode($result['nameDisplay']);
							$add_brewery = 'brewery_name='.(isset($result['breweries'][0]) ? urlencode($result['breweries'][0]['name']) : '');
							$add_style = 'style='.(isset($result['style']['name']) ? urlencode($result['style']['name']) : '');
							$add_image = 'beer_image='.$img_url;
							$add_description = 'beer_description='.(isset($result['description']) ? urlencode($result['description']) : '');
							$add_abv = 'abv='.(isset($result['abv']) ? $result['abv'] : null);
							$add_ibu = 'ibu='.(isset($result['ibu']) ? $result['ibu'] : null);
							$add_form_params = $add_id.'&'.$add_name.'&'.$add_brewery.'&'.$add_style.'&'.$add_image.'&'.$add_description.'&'.$add_abv.'&'.$add_ibu;
						?>

						<?php if ( in_array( $result['id'], $in_tab ) ): ?>
							<a href="javascript:;" class="button outline radius expand tab-action remove-from-tab" data-beerId="<?php echo $result['id']; ?>" data-params="<?php echo $add_form_params; ?>">Remove from Tab</a>
						<?php else: ?>
							<a href="/user/add_form?<?php echo $add_form_params; ?>" class="button radius expand tab-action add-to-tab" data-reveal-id="add-modal" data-beerId="<?php echo $result['id']; ?>" data-params="<?php echo $add_form_params; ?>" data-reveal-ajax="true">Add to Tab</a>
						<?php endif; ?>
					</div>
				</div>
			</div>

		<?php endfor; ?>
