<div class="row">
	<div class="medium-10 medium-centered columns">

		<?php if( isset($results['data']) ): ?>

		<div class="search-results-container small-full card">
		<?php for ($i = 0; $i < count($results['data']); $i++): ?>
			<?php
			$result = $results['data'][$i];
			$beer_url = $result['id'] . '-' . url_title( $result['nameDisplay'], '-' );
			?>

			<div class="search-result clearfix">
				<div class="small-3 medium-2 columns results-thumbnail">
					<?php
						if ( isset( $result['images']['medium'] ) ) {
							$img_url = $result['images']['medium'];
						}
						elseif ( isset( $result['labels']['medium'] ) ) {
							$img_url = $result['labels']['medium'];
						}
						else {
							$img_url = '/img/no-beer.png';
						}
					?>
					<a href="/beer/<?php echo $beer_url; ?>"><img src="<?php echo $img_url; ?>" /></a>
				</div>
				<div class="small-9 medium-10 columns results-info">
					<div class="medium-8 columns">
				    <a href="/beer/<?php echo $beer_url; ?>">
				    	<h4><?php echo $result['nameDisplay']; ?></h4>

				    	<?php if( isset($result['breweries'][0]) ): ?>
				    	<h6><?php echo $result['breweries'][0]['name']; ?></h6>
				    	<?php endif; ?>

				    	<span class="results-meta">
					    	<?php
					    	echo ( isset($result['style']['name']) ? '<span>'.$result['style']['name'].'</span>' : '' );
					    	echo ( isset($result['abv']) ? '<span>'.$result['abv'].'% ABV</span>' : '' );
					    	echo ( isset($result['ibu']) ? '<span>'.$result['ibu'].' IBU</span>' : '' );
					    	?>
					    </span>
						</a>
					</div>
					<div class="medium-4 columns">
						<?php
							$add_id = 'id='.$result['id'];
							$add_name = 'beer_name='.urlencode($result['nameDisplay']);
							$add_brewery = 'brewery_name='.(isset($result['breweries'][0]) ? urlencode($result['breweries'][0]['name']) : '');
							$add_style = 'style='.(isset($result['style']['name']) ? urlencode($result['style']['name']) : '');
							$add_image = 'beer_image='.$img_url;
							$add_description = 'beer_description='.(isset($result['description']) ? urlencode($result['description']) : '');
							$add_abv = 'abv='.(isset($result['abv']) ? $result['abv'] : null);
							$add_ibu = 'ibu='.(isset($result['ibu']) ? $result['ibu'] : null);
							$add_form_params = $add_id.'&'.$add_name.'&'.$add_brewery.'&'.$add_style.'&'.$add_image.'&'.$add_description.'&'.$add_abv.'&'.$add_ibu;
						?>

						<?php if ( in_array( $result['id'], $in_tab ) ): ?>
							<a href="javascript:;" class="button outline radius expand tab-action remove-from-tab" data-beerId="<?php echo $result['id']; ?>" data-params="<?php echo $add_form_params; ?>">Remove from Tab</a>
						<?php else: ?>
							<a href="/user/add_form?<?php echo $add_form_params; ?>" class="button radius expand tab-action add-to-tab" data-reveal-id="add-modal" data-beerId="<?php echo $result['id']; ?>" data-params="<?php echo $add_form_params; ?>" data-reveal-ajax="true">Add to Tab</a>
						<?php endif; ?>
					</div>
				</div>
			</div>

		<?php endfor; ?>

			<div class="search-result clearfix result-not-found" id="submit-beer">
				<div class="small-12 columns">
					<h4 class="subheader text-center">Coming up empty?<br><a href="/beer/submit" data-reveal-id="submit-modal" data-reveal-ajax="true">Add a beer to our database</a></h4>
				</div>
			</div>

		</div> <!-- /search-results-container -->

		<?php if ( $results['numberOfPages'] > 1 && $results['currentPage'] <= $results['numberOfPages'] ): ?>
		<div class="show-more-beers small-full card">
			<a href="javascript:;" data-query="<?php echo $query; ?>" data-pages="<?php echo $results['numberOfPages']; ?>" data-page="<?php echo $results['currentPage']; ?>" class="button expand show-more">Show more</a>
			<div class="cssload-container hide">
				<div class="cssload-wheel"></div>
			</div>
		</div>
		<?php endif; ?>

		<?php else: // no search results ?>

		<div class="search-result-empty clearfix">
			<div class="small-12 columns">
				<div class="tab-empty-image"></div>
				<h2>Your search returned no results</h2>
				<h4>Looks like you are the first to look for this beer!</h4>
				<p><a href="/beer/submit" data-reveal-id="submit-modal" data-reveal-ajax="true" class="button">Add a beer to our database</a></p>
			</div>
		</div>

		<?php endif; ?>
	</div>
</div>

<div class="reveal-modal small tab-modal" id="submit-modal" data-reveal aria-labelledby="modal-title" aria-hidden="true" role="dialog"></div>

<script type="text/javascript" src="/js/search.js"></script>

<?php if ( $this->ion_auth->logged_in() ): ?>
<div class="reveal-modal small tab-modal" id="add-modal" data-reveal aria-labelledby="modal-title" aria-hidden="true" role="dialog"></div>
<?php endif; ?>
