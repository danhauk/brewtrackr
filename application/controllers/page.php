<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Page extends CI_Controller {

	public function contact()
	{
		$title = 'Contact us';
		$main_content = 'pages/contact';

		$this->show_page($title, $main_content);
	}

	public function privacy()
	{
		$title = 'Privacy Policy';
		$main_content = 'pages/privacy_policy';

		$this->show_page($title, $main_content);
	}

	public function tos()
	{
		$title = 'Terms of Service';
		$main_content = 'pages/terms_of_service';

		$this->show_page($title, $main_content);
	}

	public function donate( $errors = false, $amount = null )
	{
		$data['title'] = 'Support BrewTrackr';
		$data['main_content'] = 'pages/donate';

		if ( $errors ) {
			$data['checkout_errors'] = $errors;
			$data['amount'] = $amount;
		}

		if ( $this->ion_auth->logged_in() ) {
			$this->load->view('includes/template', $data);
		}
		else {
			redirect( '/' );
		}
	}

	public function checkout()
  {
		if ( $this->input->post('stripeToken') ) {
			require_once APPPATH . 'libraries/Stripe/init.php';

			if ( ENVIRONMENT == 'development' ) {
				\Stripe\Stripe::setApiKey("sk_test_xK1trW8DpaBXIAjnm5tqA30M");
			}
			else {
				\Stripe\Stripe::setApiKey("sk_live_HBrCOdJ7F3bvPBvFSuZbEhbb");
			}

	    $token = $this->input->post('stripeToken');
			$amount_input = $this->input->post('amount');
			if (strpos($amount_input, ".") !== false) {
				$amount = str_replace('.', '', $amount_input);
			} if (strpos($amount_input, ",") !== false) {
				$amount = str_replace(',', '', $amount_input);
			} else {
				$amount = $amount_input*100;
			}

			if ( $this->ion_auth->logged_in() ) {
				$user_id = $this->ion_auth->user()->row()->id;
				$username = $this->ion_auth->user()->row()->username;
				$user_email = $this->ion_auth->user()->row()->email;
			}

			try {
				$charge = \Stripe\Charge::create(array(
				  "amount" => $amount,
				  "currency" => "usd",
				  "description" => "One-time Donation",
					"metadata" => array("user_id" => $user_id, "username" => $username),
				  "source" => $token,
					"receipt_email" => $user_email
				));
			} catch( \Stripe\Error\Card $e ) {
				$body = $e->getJsonBody();
			  $err  = $body['error'];

				$this->donate( $err, $amount_input );
			} catch (\Stripe\Error\RateLimit $e) {
			  // Too many requests made to the API too quickly
				$body = $e->getJsonBody();
			  $err  = $body['error'];
				$this->donate( $err, $amount_input );
			} catch (\Stripe\Error\InvalidRequest $e) {
			  // Invalid parameters were supplied to Stripe's API
				$body = $e->getJsonBody();
			  $err  = $body['error'];
				$this->donate( $err, $amount_input );
			} catch (\Stripe\Error\Authentication $e) {
			  // Authentication with Stripe's API failed
			  // (maybe you changed API keys recently)
				$body = $e->getJsonBody();
			  $err  = $body['error'];
				$this->donate( $err, $amount_input );
			} catch (\Stripe\Error\ApiConnection $e) {
			  // Network communication with Stripe failed
				$body = $e->getJsonBody();
			  $err  = $body['error'];
				$this->donate( $err, $amount_input );
			} catch (\Stripe\Error\Base $e) {
			  // Display a very generic error to the user, and maybe send
			  // yourself an email
				$body = $e->getJsonBody();
			  $err  = $body['error'];
				$this->donate( $err, $amount_input );
			} catch (Exception $e) {
				// Something else happened, completely unrelated to Stripe
				$body = $e->getJsonBody();
			  $err  = $body['error'];
				$this->donate( $err, $amount_input );
			}

			$this->thanks( $amount_input );
		}
		else {
			redirect( '/support' );
		}
  }

	public function thanks( $amount = null )
	{
		$data['amount'] = $amount;
		$data['title'] = 'Thank you!';
		$data['main_content'] = 'pages/thanks';

		$this->load->view('includes/template', $data);
	}

	public function show_page($title, $main_content) {
		$data = compact('main_content', 'title');
		$this->load->view('includes/template', $data);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
