<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('user_model');
		session_start();
	}

	public function index() {
		redirect('/');
	}

	public function tab($username) {
		// if user exists, get tab
		// otherwise display user 404
		if ($this->ion_auth->username_check($username)) {
			// get user's info
			$data['user_info'] = $this->user_model->get_info($username);

			$is_private = $data['user_info']->row()->private;
			$user_id = $data['user_info']->row()->id;

			$current_user_id = ( $this->ion_auth->logged_in() ? $this->ion_auth->user()->row()->id : null );

			if ( $is_private && $user_id !== $current_user_id ) {
				$this->restrictPrivateTab( $username );
			} else {
				$this->showTab( $username, $data );
			}
		}
		else {
			// $data['main_content'] = 'user_404';
			// $data['title'] = $username . ' does not exist';
			$this->session->set_flashdata('notify_error', $username . ' does not exist.');
			redirect('/', 'refresh');
		}
	}

	public function restrictPrivateTab( $username ) {
		// display page
		$data['main_content'] = 'user/user_tab_private';
		$data['title'] = $username;
		$data['username'] = $username;
		$this->load->view('includes/template', $data);
	}

	public function showTab( $username, $data ) {
		$user_id = $data['user_info']->row()->id;
		$tab_totals = $this->user_model->get_totals($user_id);

		// if sorted
		if ( isset( $_GET['sort'] ) ) {
			$sort_by = $_GET['sort'];
			$this->session->set_userdata('tab_sort', $_GET['sort']);
		}
		elseif ( $this->session->userdata('tab_sort') ) {
			$sort_by = $this->session->userdata('tab_sort');
		}
		else {
			$sort_by = 'recent';
		}

		if ($sort_by == 'rating-high') {
			$data['sort_by'] = 'Highest Rating';
		} elseif ($sort_by == 'rating-low') {
			$data['sort_by'] = 'Lowest Rating';
		} else {
			$data['sort_by'] = $sort_by;
		}

		// if searching tab, get search
		// otherwise get full tab
		if ( isset( $_GET['s'] ) ) {
			$s = $this->input->get('s');
			$tab = $this->user_model->search_tab($username, $s, $sort_by);
			$data['s'] = $s;
			// $tab = $this->tab_search($username);
		}
		elseif ( isset( $_GET['favorites'] ) ) {
			$tab = $this->user_model->get_tab($username, $sort_by, $fav = true);
		}
		else {
			$tab = $this->user_model->get_tab($username, $sort_by);
		}

		// tab info to display
		$data['tab'] = $tab;
		$data['tab_totals'] = $tab_totals;
		$data['num_pages'] = $tab_totals['total_beers']/25;
		$data['current_page'] = ( isset( $_GET['p'] ) ? $_GET['p'] : 1 );

		// display page
		if ( $tab->num_rows() < 1 ) {
			$data['main_content'] = 'user/user_tab_empty';
		}
		else {
			$data['main_content'] = 'user/user_tab';
		}

		// display page
		$data['title'] = $username;
		$data['username'] = $username;
		$this->load->view('includes/template', $data);
	}

	public function favorites($username)
	{
		// if user exists, get tab
		// otherwise display user 404
		if ($this->ion_auth->username_check($username)) {
			// get user's info
			$data['user_info'] = $this->user_model->get_info($username);
			$user_id = $data['user_info']->row()->id;
			$tab_totals = $this->user_model->get_totals($user_id);

			if ( isset( $_GET['sort'] ) ) {
				$sort_by = $_GET['sort'];
				$this->session->set_userdata('tab_sort', $_GET['sort']);
			}
			elseif ( $this->session->userdata('tab_sort') ) {
				$sort_by = $this->session->userdata('tab_sort');
			}
			else {
				$sort_by = 'recent';
			}

			if ($sort_by == 'rating-high') {
				$data['sort_by'] = 'Highest Rating';
			} elseif ($sort_by == 'rating-low') {
				$data['sort_by'] = 'Lowest Rating';
			} else {
				$data['sort_by'] = $sort_by;
			}

			// tab info to display
			$tab = $this->user_model->get_tab($username, $sort_by, $fav = true);
			$data['tab'] = $tab;
			$data['tab_totals'] = $tab_totals;
			$data['num_pages'] = $tab_totals['total_beers']/25;
			$data['current_page'] = ( isset( $_GET['p'] ) ? $_GET['p'] : 1 );

			// display page
			if ( $tab->num_rows() < 1 ) {
				$data['main_content'] = 'user/user_tab_empty';
			}
			else {
				$data['main_content'] = 'user/user_tab';
			}

			$data['title'] = $username . "'s favorite beers";
		}
		else {
			// $data['main_content'] = 'user_404';
			// $data['title'] = $username . ' does not exist';
			$this->session->set_flashdata('notify_error', $username . ' does not exist.');
			redirect('/', 'refresh');
		}

		// display page
		$data['username'] = $username;
		$this->load->view('includes/template', $data);
	}

	public function tab_more()
	{
		$page = $this->input->get('p');
		$username = $this->input->get('u');
		$offset = $page-1;

		$data['user_info'] = $this->user_model->get_info($username);

		if ( isset( $_GET['sort'] ) ) {
			$sort_by = $_GET['sort'];
			$this->session->set_userdata('tab_sort', $_GET['sort']);
		}
		elseif ( $this->session->userdata('tab_sort') ) {
			$sort_by = $this->session->userdata('tab_sort');
		}
		else {
			$sort_by = 'recent';
		}

		$tab = $this->user_model->get_tab($username, $sort_by, $fav = null, $limit = 25, $offset );

		if ( $tab->num_rows() > 0 ) {
			$data['tab'] = $tab;
			$this->load->view('user/user_tab_more', $data);
		} else {
			echo '';
		}
	}

	public function settings() {
		if ( $this->ion_auth->logged_in() ) {
			$user_id = $this->ion_auth->user()->row()->id;

			$data['info'] = $this->user_model->get_info_by_id($user_id);

			// get social connections
			$data['social'] = [];
			$social = $this->user_model->get_social_by_id($user_id);
			if ( $social->num_rows() > 0 ) {
				if ( $social->row()->twitter_id ) {
					// strip tokens from variable for security
					$twitter = array(
						'twitter_id' => $social->row()->twitter_id,
						'twitter_username' => $social->row()->twitter_username
					);
					$data['social']['twitter'] = $twitter;
				}

				if ( $social->row()->facebook_id ) {
					// strip tokens from variable for security
					$facebook = array(
						'facebook_id' => $social->row()->facebook_id,
						'facebook_name' => $social->row()->facebook_name
					);
					$data['social']['facebook'] = $facebook;
				}
			}

			$data['title'] = 'Account Settings';
			$data['main_content'] = 'user/settings';
			$this->load->view('includes/template', $data);
		}
		else {
			redirect('/', 'refresh');
		}
	}

	public function upload_avatar() {
		// get file info
		$file = $_FILES['avatar'];
		$filetype = $file['type'];
		$filename = $file['tmp_name'];
		$filesize = filesize($filename);

		if ( $filetype != 'image/jpeg' && $filetype != 'image/png' ) {
			// wrong file format
			$response = 'Your photo needs to be in JPG or PNG format.';
		} elseif ( $filesize > 8388608 ) {
			// file larger than 8MB
			$response = 'Your photo needs to be less than 8MB.';
		} else {
			// correct filesize and filetype
			// let's upload!
			$username = $this->ion_auth->user()->row()->username;
        	$public_id = $username . '_' . time();

        	$this->load->library('cloudinary');

        	$result = \Cloudinary\Uploader::upload($filename,
			    array(
			       "public_id" => $public_id,
			       "crop" => "limit", "width" => "600", "height" => "600",
			       "eager" => array(
			         array( "width" => 128, "height" => 128,
			                "crop" => "fill" )
			       ),
			       "tags" => array( "user", $username )
			    ));

        	if ( isset( $result['eager'] ) ) {
				$new_avatar = $result['eager'][0]['url'];
				$response = $new_avatar;

				// set photo url in database
				$user_id = $this->ion_auth->user()->row()->id;
				$this->user_model->update_photo($user_id, $new_avatar);
			}
			else {
				$response = 'Uh oh! Something went wrong.';
			}
        }

        echo $response;
	}

	public function update() {
		$user_id = $this->ion_auth->user()->row()->id;
		$user_email = $this->ion_auth->user()->row()->email;

		$email = $this->input->post('email');
		$bio = trim( $this->input->post('bio') );

		// validate email
		$this->load->library('form_validation');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');

		if ( $user_email != $email ) {
			$email_registered = $this->ion_auth->email_check($email);
		}

		if ($this->form_validation->run() == FALSE) {
			echo validation_errors();
		}
		elseif ( isset($email_registered) && $email_registered == true )  {
			echo 'email taken';
		}
		else {
			$this->user_model->update_profile($user_id, $email, $bio);

			echo 'success';
		}
	}

	public function change_password()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('old', 'Old Password', 'required');
		$this->form_validation->set_rules('new', 'New Password', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
		$this->form_validation->set_rules('new_confirm', 'Confirm Password', 'required');

		if (!$this->ion_auth->logged_in())
		{
			redirect('/login', 'refresh');
		}

		$user = $this->ion_auth->user()->row();

		if ($this->form_validation->run() == false)
		{
			// display the form
			// set the flash data error message if there is one
			$password_errors = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			// $this->session->set_flashdata('password_error', $password_errors);
			$response = $password_errors;
		}
		else
		{
			$identity = $this->session->userdata('identity');
			$new_password = $this->input->post('new');

			$change = $this->ion_auth->change_password($identity, $this->input->post('old'), $this->input->post('new'));

			if ($change)
			{
				//if the password was successfully changed
				$this->session->set_flashdata('notify_success', $this->ion_auth->messages());

				$response = 'success';
			}
			else
			{
				$this->session->set_flashdata('password_error', $this->ion_auth->errors());
				$response = $this->ion_auth->errors();
			}
		}

		echo $response;

		// redirect('/settings#password', 'refresh');
	}

	public function add_form() {
		if ( !$this->ion_auth->logged_in() ) {
			$this->load->view('user/noaccess_modal');
			return;
		}

		$data['beer_id'] = $this->input->get('id');
		$data['beer_name'] = $this->input->get('beer_name');
		$data['brewery_name'] = $this->input->get('brewery_name');
		$data['style'] = $this->input->get('style');
		$data['beer_image'] = $this->input->get('beer_image');
		$data['beer_description'] = $this->input->get('beer_description');
		$data['beer_abv'] = $this->input->get('abv');
		$data['beer_ibu'] = $this->input->get('ibu');
		$data['brewery_twitter'] = $this->input->get('twitter');

		// check if user is premium
		$user_id = $this->ion_auth->user()->row()->id;
		$data['premium_user'] = $this->user_model->is_premium($user_id);

		$social = $this->user_model->get_social_by_id($user_id);
		if ( $social->num_rows() > 0 ) {
			$data['social']['facebook'] = $social->row()->facebook_id;
			$data['social']['twitter'] = $social->row()->twitter_id;
		}

		$this->load->view('user/add_to_tab', $data);
	}

	public function edit_form() {
		$this->load->view('user/edit_tab_placeholder');
	}

	public function edit_form_show() {
		if ( !$this->ion_auth->logged_in() ) {
			$this->load->view('user/noaccess_modal');
			return;
		}

		$tab_id = $this->input->get('id');

		// get tab info by id
		$data['tab_info'] = $this->user_model->tab_info_by_id($tab_id);

		// check if user is premium
		$user_id = $this->ion_auth->user()->row()->id;
		$data['premium_user'] = $this->user_model->is_premium($user_id);

		$this->load->view('user/edit_tab', $data);
	}

	public function add_to_tab() {
		$this->load->model('beer_model');
		$response = array();

		// variables from post data
		$beer_name = $this->input->post('submit_name');
    $brewery_name = $this->input->post('submit_brewery');
    $abv = $this->input->post('submit_abv');
    $ibu = $this->input->post('submit_ibu');
    $style = $this->input->post('submit_style');
    $description = $this->input->post('submit_description');
    $user_id = $this->input->post('user_id');
		$notes = trim( $this->input->post('notes') );
		$rating = ( $this->input->post('rating') == '' ? 0 : $this->input->post('rating') );
		$favorite = ( $this->input->post('favorite') == 'on' ? 1 : 0 );
		$file = ( isset($_FILES['addphoto']) ? $_FILES['addphoto'] : false );
		$photo_url = null;

		// remove % from abv if entered
		$abv = explode( '%', $abv );
    $abv = $abv[0];

    // get a beer_id from bitly
    $longUrl = $_SERVER['HTTP_REFERER'] . '&timestamp=' . time();
		$bitly = $this->beer_model->get_shortlink($longUrl);
    $bitly_id = $bitly['data']['global_hash'];
    $beer_id = 'brwtk-' . $bitly_id;

		// upload user photo
		if ( $file && $file['size'] > 0 ) {
			$filename = $file['tmp_name'];
			$filetype = $file['type'];
			$filesize = filesize($filename);

			if ( $filetype != 'image/jpeg' && $filetype != 'image/png' ) {
				// wrong file format
				$response['error'] = 'Your photo needs to be in JPG or PNG format.';
				echo json_encode( $response );
				die;
			} elseif ( $filesize > 8388608 ) {
				// file larger than 8MB
				$response['error'] = 'Your photo needs to be less than 8MB.';
				echo json_encode( $response );
				die;
			} else {
				$uploaded_photo = $this->upload_photo( $file, $beer_id );

				if ( isset($uploaded_photo['success']) ) {
					// set photo url in database
					$photo_url = $uploaded_photo['success'];
					$photo_id = $uploaded_photo['photo_id'];
					$this->beer_model->upload_photo($user_id, $beer_id, $photo_url, $photo_id);
					$response['photo'] = $photo_id;
				} else {
					$response['error'] = $uploaded_photo;
					echo json_encode( $response );
					die;
				}
			}
		}

		// add to tab
		$row_id = $this->user_model->add_to_tab($user_id, $beer_id, $notes, $rating, $beer_name, $brewery_name, $style, $favorite, $description, $abv, $ibu, $photo_url);

		// send me an email
		if ( $user_id == $this->ion_auth->user()->row()->id ) {
			// send me an email
			$email_data = array(
				'username' => $this->ion_auth->user()->row()->username,
				'name' => $beer_name,
				'brewery' => $brewery_name,
				'style' => $style,
				'abv' => $abv,
				'ibu' => $ibu,
				'description' => $description,
				'photo_url' => $photo_url
			);
			$email_data['content'] = $this->load->view('email/beer_submission.php', $email_data, true);
			$message = $this->load->view('email/template/includes.tpl.php', $email_data, true);
			$this->load->library('postmark');
			$this->postmark->clear();
			$this->postmark->from('notifications@brewtrackr.com', 'BrewTrackr');
			$this->postmark->to('hauk.dan@gmail.com');
			$this->postmark->reply_to('hello@brewtrackr.com', 'Reply To');
			$this->postmark->subject($beer_name.' was added to BrewTrackr');
			$this->postmark->message_plain($message);
			$this->postmark->message_html($message);
			$this->postmark->send();
		}

		// add to beer table if it doesn't exist
		// $this->beer_model->add_to_table($beer_id, $beer_name, $brewery_name, $style, $beer_image, $description, $abv, $ibu, $user_id);

		if ( !isset($response['error']) ) {
			$response['success'] = true;
		}

		$res = json_encode( $response );
		// echo $res;

		// set notification
		$this->session->set_flashdata('notify_success', $beer_name . ' has been added to your tab!');
		redirect( '/' );
	}

	public function edit_tab() {
		$username = $this->ion_auth->user()->row()->username;
		$user_id = $this->ion_auth->user()->row()->id;
		$response = [];

		$tab_id = $this->input->post('row_id');
		$beer_id = $this->input->post('beer_id');
		$beer_name = $this->input->post('beer_name');
    $brewery_name = $this->input->post('brewery_name');
    $abv = $this->input->post('abv');
    $ibu = $this->input->post('ibu');
    $style = $this->input->post('submit_style');
    $description = $this->input->post('description');
		$notes = trim( $this->input->post('notes') );
		$rating = ( $this->input->post('rating') == '' ? 0 : $this->input->post('rating') );
		$favorite = ( $this->input->post('favorite') == 'on' ? 1 : 0 );
		$file = ( isset($_FILES['addphoto']) ? $_FILES['addphoto'] : false );
		$photo_url = null;

		// upload user photo
		if ( $file && $file['size'] > 0 ) {
			$filename = $file['tmp_name'];
			$filetype = $file['type'];
			$filesize = filesize($filename);

			if ( $filetype != 'image/jpeg' && $filetype != 'image/png' ) {
				// wrong file format
				$response['error'] = 'Your photo needs to be in JPG or PNG format.';
				echo json_encode( $response );
				die;
			} elseif ( $filesize > 8388608 ) {
				// file larger than 8MB
				$response['error'] = 'Your photo needs to be less than 8MB.';
				echo json_encode( $response );
				die;
			} else {
				$uploaded_photo = $this->upload_photo( $file, $beer_id );

				if ( isset($uploaded_photo['success']) ) {
					// set photo url in database
					$photo_url = $uploaded_photo['success'];
					$photo_id = $uploaded_photo['photo_id'];
					$this->load->model('beer_model');
					$this->beer_model->upload_photo($user_id, $beer_id, $photo_url, $photo_id);
					$response['photo'] = $photo_id;
					$response['photo_url'] = $photo_url;
				} else {
					$response['error'] = $uploaded_photo;
					echo json_encode( $response );
					die;
				}
			}
		}

		// update tab
		$updated = $this->user_model->update_tab($tab_id, $notes, $rating, $beer_name, $brewery_name, $style, $favorite, $description, $abv, $ibu, $photo_url);

		// set notification
		if ( $updated && ! isset($response['error']) ) {
			$response['success'] = true;
			echo json_encode($response);
		}
		else if ( isset($response['error']) ) {
			echo json_encode($response);
		}
		else {
			$response['error'] = 'Hmm... something went wrong. Maybe try again?';
			echo json_encode($response);
		}

		// Redirect
    // redirect('/'.$username, 'refresh');
	}

	public function tab_search($username) {
		$q = $this->input->get('s');

		$data['query'] = $this->user_model->search_tab($username, $q);

		return $data;
	}

	public function remove_from_tab() {
		if ( !$this->ion_auth->logged_in() ) {
			$this->load->view('user/noaccess_modal');
			echo 'It looks like you\'re logged out. Refresh the page to login and try again. Sorry! :(';
			return;
		}

		$beer_id = $this->input->post('beer_id');
		$user_id = $this->ion_auth->user()->row()->id;

		$removed = $this->user_model->remove_from_tab($beer_id, $user_id);

		if ($removed) {
			echo 'success';
		}
	}

	public function upload_photo( $file, $beer_id ) {
		$filename = $file['tmp_name'];
		$username = $this->ion_auth->user()->row()->username;
		$public_id = $beer_id . '_by_' . $username . '_' . time();

		$this->load->library('cloudinary');

		$result = \Cloudinary\Uploader::upload($filename,
			array(
				 "public_id" => $public_id,
				 "crop" => "limit", "width" => "1200", "height" => "1200",
				 "eager" => array(
					 array( "width" => 200, "height" => 200,
									"crop" => "fill" )
				 ),
				 "tags" => array( "beer", $username, $beer_id )
			));

		if ( isset( $result['eager'] ) ) {
			$uploaded_photo = $result['eager'][0]['secure_url'];
			$response = array(
				'success' => $uploaded_photo,
				'photo_id' => $public_id . '.' . $result['format']
			);
		}
		else {
			$response = 'Uh oh! Something went wrong.';
		}

		return $response;
	}

	// =====================
	// Twitter/Facebook Connection
	// =====================
	public function connect($account) {
		if ( $account == 'twitter' ) {
			$this->load->library('twitteroauth');
			$this->config->load('twitter');
			$consumer_token = $this->config->item('twitter_consumer_token');
			$consumer_secret = $this->config->item('twitter_consumer_secret');

			$connection = $this->twitteroauth->create(
				$consumer_token,
				$consumer_secret
			);
			$token = $this->twitteroauth->getRequestToken( site_url('user/twitter_callback') );

			// temporarily store request tokens
			$this->session->set_userdata('oauth_token', $token['oauth_token']);
			$this->session->set_userdata('oauth_token_secret', $token['oauth_token_secret']);

			$authUrl = $this->twitteroauth->getAuthorizeURL($token);

			redirect($authUrl);
		}

		if ( $account == 'facebook' ) {
			$this->config->load('facebook');
			$fb_app_id = $this->config->item('facebook_app_id');
			$fb_app_secret = $this->config->item('facebook_app_secret');

			require_once APPPATH . 'libraries/Facebook/autoload.php';

			$fb = new Facebook\Facebook([
			  'app_id' => $fb_app_id,
			  'app_secret' => $fb_app_secret,
			  'default_graph_version' => 'v2.4',
			  ]);

			$helper = $fb->getRedirectLoginHelper();
			$permissions = ['publish_actions, email, public_profile']; // optional
			$loginUrl = $helper->getLoginUrl( site_url('user/facebook_callback'), $permissions );

			redirect($loginUrl);
		}
	}

	public function twitter_callback()
	{
		$this->load->library('twitteroauth');
		$this->config->load('twitter');
		$consumer_token = $this->config->item('twitter_consumer_token');
		$consumer_secret = $this->config->item('twitter_consumer_secret');

		// pull tokens out of session
		$request_token = [];
		$request_token['oauth_token'] = $this->session->userdata('oauth_token');
		$request_token['oauth_token_secret'] = $this->session->userdata('oauth_token_secret');

		// make connection with request token
		$connection = $this->twitteroauth->create(
			$consumer_token,
			$consumer_secret,
			$request_token['oauth_token'],
			$request_token['oauth_token_secret']
		);

		// get long-term access token
		$access_token = $this->twitteroauth->getAccessToken($_REQUEST['oauth_verifier']);

		// make new connection with access token
		$connection = $this->twitteroauth->create(
			$consumer_token,
			$consumer_secret,
			$access_token['oauth_token'],
			$access_token['oauth_token_secret']
		);

		// get user info
		$user = $connection->get('account/verify_credentials');

		if ( isset( $user->errors ) ) {
			// credentials invalid, show errors
			$error = $user->errors->message;
			$this->session->set_flashdata('notify_error', 'There was a problem connecting your Twitter Account. ' . $error);
		} else {
			// credentials valid
			// check for current social connections and
			// store info in db
			$user_id = $this->ion_auth->user()->row()->id;
			$social = $this->user_model->get_social_by_id($user_id);

			$twitter_data = array(
				'twitter_id' => $user->id,
				'twitter_username' => $user->screen_name,
				'twitter_token' => $access_token['oauth_token'],
				'twitter_secret' => $access_token['oauth_token_secret']
				);

			if ( $social->num_rows() > 0 ) {
		  		// user already has social account connected
		  		$this->db->where('user_id', $user_id);
		  		$this->db->update('users_social', $twitter_data);
		  	}
		  	else {
		  		// user doesn't have any other social account
		  		$twitter_data['user_id'] = $user_id;
		  		$this->db->insert('users_social', $twitter_data);
		  	}

			$this->session->set_flashdata('notify_success', "Connected @{$user->screen_name}!");
		}

		// remove tokens from session
		$this->session->unset_userdata('oauth_token');
		$this->session->unset_userdata('oauth_token_secret');

		// set session variable to share by default
		$this->session->set_userdata('share_twitter', true);
		$this->session->set_userdata('twitter_username', $user->screen_name);

		redirect('/settings', 'refresh');
	}

	public function facebook_callback() {
		$this->config->load('facebook');
		$fb_app_id = $this->config->item('facebook_app_id');
		$fb_app_secret = $this->config->item('facebook_app_secret');

		require_once APPPATH . 'libraries/Facebook/autoload.php';

		$fb = new Facebook\Facebook([
		  'app_id' => $fb_app_id,
		  'app_secret' => $fb_app_secret,
		  'default_graph_version' => 'v2.4',
		  ]);

		$helper = $fb->getRedirectLoginHelper();
		try {
		  $accessToken = $helper->getAccessToken();
		} catch(Facebook\Exceptions\FacebookResponseException $e) {
		  // When Graph returns an error
		  echo 'Graph returned an error: ' . $e->getMessage();
		  exit;
		} catch(Facebook\Exceptions\FacebookSDKException $e) {
		  // When validation fails or other local issues
		  echo 'Facebook SDK returned an error: ' . $e->getMessage();
		  exit;
		}

		if (isset($accessToken)) {
		  	// Logged in!
		  	// extend the user token
		  	$oAuth2Client = $fb->getOAuth2Client();
		  	$longLivedAccessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);

		  	$expiresAt = $longLivedAccessToken->getExpiresAt();
		  	$expiresAtFormatted = $expiresAt->format('c');

		  	$_SESSION['facebook_access_token'] = (string) $longLivedAccessToken;

		  	// make API call to get user details
		  	$fb->setDefaultAccessToken($longLivedAccessToken);

			try {
			  $response = $fb->get('/me');
			  $userNode = $response->getGraphUser();
			  $this->session->set_flashdata('notify_success', 'Connected to Facebook as ' . $userNode['name'] . '!');
			} catch(Facebook\Exceptions\FacebookResponseException $e) {
			  // When Graph returns an error
				$this->session->set_flashdata('notify_error', 'Graph returned an error: ' . $e->getMessage());
			  // echo 'Graph returned an error: ' . $e->getMessage();
			  exit;
			} catch(Facebook\Exceptions\FacebookSDKException $e) {
			  // When validation fails or other local issues
				$this->session->set_flashdata('notify_error', 'Facebook SDK returned an error: ' . $e->getMessage());
			  // echo 'Facebook SDK returned an error: ' . $e->getMessage();
			  exit;
			}

		  	// check for current social connections and
		  	// store token and user data in database
		  	$user_id = $this->ion_auth->user()->row()->id;
		  	$social = $this->user_model->get_social_by_id($user_id);

			$fb_data = array(
				'facebook_id' => $userNode['id'],
				'facebook_name' => $userNode['name'],
				'facebook_token' => (string) $longLivedAccessToken
			);

		  	if ( $social->num_rows() > 0 ) {
		  		// user already has social accounts connected
		  		$this->db->where('user_id', $user_id);
		  		$this->db->update('users_social', $fb_data);
		  	}
		  	else {
		  		// user doesn't have any other social account
		  		$fb_data['user_id'] = $user_id;
		  		$this->db->insert('users_social', $fb_data);
		  	}

			// remove token from session
			$this->session->unset_userdata('facebook_access_token');

			// set to share by default
			$this->session->set_userdata('share_facebook', true);
			$this->session->set_userdata('facebook_name', $userNode['name']);

			redirect('/settings', 'refresh');
		}
	}

	public function disconnect( $service ) {
		if ( $this->ion_auth->logged_in() ) {
			$user_id = $this->ion_auth->user()->row()->id;
			$disconnected = $this->user_model->disconnect_social( $user_id, $service );

			if ( $disconnected ) {
				$this->session->set_flashdata( 'notify_success', 'Disconnected ' . $service );
			} else {
				$this->session->set_flashdata( 'notify_error', 'Something went wrong. Try again.');
			}

			redirect( '/settings', 'refresh' );
		}
	}

	// ===================
	// AJAX calls for checking
	// username and email on signup flow
	// ===================

	public function check_username() {
		$username = trim($this->input->post('username'));
		$username = strtolower($username);

		$restricted = array(
			'login',
			'signup',
			'search',
			'auth',
			'logout',
			'forgot_password',
			'reset_password',
			'admin',
			'beer',
			'brewery',
			'settings',
			'user',
			'contact',
			'privacy',
			'tos',
			'page',
			'welcome',
			'connect'
		);

        if ( !$username )
        {
        	$available = 'false';
        }
        elseif ( in_array( $username, $restricted ) )
        {
        	$available = 'false';
        }
        elseif ( preg_match('/[^A-Za-z0-9.\-$]/', $str) ) {
        	$available = 'false';
        }
        elseif ( $this->ion_auth->username_check($username) )
        {
        	$available = 'false';
        }
        else
        {
        	$available = 'true';
        }

        echo $available;
	}

	public function check_email( $email = null ) {
		if ( !$email ) {
			$email = trim($this->input->post('email'));
		}

    if ( !$email )
    {
    	$available = 'false';
    }
    if ( !filter_var($email, FILTER_VALIDATE_EMAIL) )
    {
    	$available = 'false';
    }
    elseif ( $this->ion_auth->email_check($email) )
    {
    	$available = 'false';
    }
    else
    {
    	$available = 'true';
    }

    echo $available;
	}

	// ===================
	// Swag signup
	// ===================

	function get_swag()
	{
		$this->load->view("/user/swag_modal");
	}

	function migration()
	{
		$uid = $this->input->post('uid');
		$userId = $this->input->post('userId');

		$message = "user_id: ".$userId."<br>uid: ".$uid;

		$this->load->library('postmark');
		$this->postmark->clear();
		$this->postmark->from('notifications@brewtrackr.com', 'BrewTrackr');
		$this->postmark->to('hauk.dan@gmail.com');
		$this->postmark->reply_to('hello@brewtrackr.com', 'Reply To');
		$this->postmark->subject('New migration to Firebase');
		$this->postmark->message_plain($message);
		$this->postmark->message_html($message);
		$this->postmark->send();

		echo 'success';
	}

	// ===================
	// Export to CSV
	// ===================

	public function export() {
		$user_id = $this->ion_auth->user()->row()->id;
		$username = $this->ion_auth->user()->row()->username;
		$now = date('Y-m-d H:i:s');
		$filename = 'brewtrackr_' . $username . '_' . $now . '.csv';

		$tab = $this->user_model->get_tab_export( $user_id );
		$csv_headers = array( 'Beer Name', 'Brewery', 'Style', 'Rating', 'Notes', 'Favorite', 'Date Added' );

		header('Content-type: text/csv');
		header('Content-Disposition: attachment; filename="'.$filename.'"');

		$export = fopen( $filename, 'w' );
		fputs($export, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
		fputcsv( $export, $csv_headers );
		foreach( $tab->result_array() as $row ) {
			fputcsv( $export, $row );
		}
		fclose( $export );

		readfile( $filename );
	}


	// ===================
	// Nuclear Option
	// delete account
	// ===================

	public function delete() {
		if ( !$this->ion_auth->logged_in() ) {
			$this->load->view('user/noaccess_modal');
			echo 'It looks like you\'re logged out. Refresh the page to login and try again. Sorry! :(';
			return;
		}
		else {
			$this->load->view('user/delete_modal');
		}
	}

	public function delete_confirm()
	{
		$username = $this->input->post("username");
		$username = strtolower($username);

		$currentUsername = $this->ion_auth->user()->row()->username;
		$currentUsername = strtolower($currentUsername);

		if ( $username != $currentUsername ) {
			echo "Error deleting account: Username did not match.";
		}
		else {
			$user_id = $this->ion_auth->user()->row()->id;
			$deleted = $this->user_model->delete_account($user_id);

			if ($deleted) {
				echo "success";
			}
			else {
				echo "There was a problem deleting your account. Refresh the page and try again?";
			}
		}
	}
}
