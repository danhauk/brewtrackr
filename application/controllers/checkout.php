<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Checkout extends CI_Controller {

  function __construct() {
    parent::__construct();
  }

  public function index()
  {
    $data['main_content'] = 'admin/index';
    $this->load->view('admin/index', $data);
  }

  public function submit()
  {
		require_once APPPATH . 'libraries/Stripe/init.php';

		if ( ENVIRONMENT == 'development' ) {
			\Stripe\Stripe::setApiKey("sk_test_xK1trW8DpaBXIAjnm5tqA30M");
		}
		else {
			\Stripe\Stripe::setApiKey("sk_live_HBrCOdJ7F3bvPBvFSuZbEhbb");
		}

    $token = $this->input->post('stripeToken');
		$amount = $this->input->post('amount');

		if ( $this->ion_auth->logged_in() ) {
			$user_id = $this->ion_auth->user()->row()->id;
			$username = $this->ion_auth->user()->row()->username;
			$user_email = $this->ion_auth->user()->row()->email;
		}

		try {
			$charge = \Stripe\Charge::create(array(
			  "amount" => $amount*100,
			  "currency" => "usd",
			  "description" => "One-time Donation",
				"metadata" => array("user_id" => $user_id, "username" => $username),
			  "source" => $token,
				"receipt_email" => $user_email
			));
		} catch( \Stripe\Error\Card $e ) {
			$body = $e->getJsonBody();
		  $err  = $body['error'];

		  print('Status is:' . $e->getHttpStatus() . "\n");
		  print('Type is:' . $err['type'] . "\n");
		  print('Code is:' . $err['code'] . "\n");
		  // param is '' in this case
		  print('Param is:' . $err['param'] . "\n");
		  print('Message is:' . $err['message'] . "\n");
		} catch (\Stripe\Error\RateLimit $e) {
		  // Too many requests made to the API too quickly
		} catch (\Stripe\Error\InvalidRequest $e) {
		  // Invalid parameters were supplied to Stripe's API
		} catch (\Stripe\Error\Authentication $e) {
		  // Authentication with Stripe's API failed
		  // (maybe you changed API keys recently)
		} catch (\Stripe\Error\ApiConnection $e) {
		  // Network communication with Stripe failed
		} catch (\Stripe\Error\Base $e) {
		  // Display a very generic error to the user, and maybe send
		  // yourself an email
		} catch (Exception $e) {
		  // Something else happened, completely unrelated to Stripe
		}



		// $this->success( json_decode($charge) );

		// echo json_decode($charge);
  }

	public function success( $info ) {
		var_dump($info);

		$title = 'Thank you!';
		$main_content = 'pages/donate';

		$data = compact('main_content', 'title');
		$this->load->view('includes/template', $data);
	}
}
