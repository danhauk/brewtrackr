<?php
/**
 * This is a sample implementation of a RESTful API. The database contains a
 * `users` table with four columns (`user_id`, `username`, `password` and
 * `email`).
 */
class Api extends CI_Controller {

  private $users, $auth;

  public function __construct(){
    parent::__construct();
    $this->load->helper('error_code');
    $this->load->helper('database');
    $this->load->helper('jwt');
    $this->load->helper('request');
    $this->load->helper('auth');
    $this->load->helper('rest_api');
    $this->users = array(
      "table" => "`users`",
      "create_fields" => ["`username`", "`password`", "`email`"],
      "create_types" => "sss",
      "read_fields" => "`username`, `user_id` AS `id`",
      "read_key" => "`username`",
      "update_fields" => ["`email`"],
      "update_key" => "`username`",
      "delete_key" => "`username`"
    );
    $this->auth = array(
      "table" => "`users`",
      "fields" => "`username`, `user_id` AS `id`, `password`",
      "username_field" => "`username`",
      "password_field" => "`password`",
      "id_field" => "`id`",
      "service_name" => "pyramids.social",
      "cookie_name" => "pyramids_social_token"
    );
  }

  public function login(){
    header('Content-Type: application/json');
		header('Access-Control-Allow-Origin: *');
    if( $_SERVER['REQUEST_METHOD'] !== "POST" ) {
      echo json_encode(array("message:"=>"Use the HTTP POST method to login to the system."));
      return;
    }
    else
      if(check_jwt_cookie($this->auth["service_name"], $this->auth["cookie_name"])){
        echo json_encode(regenerate_jwt_cookie($this->auth["service_name"], $this->auth["cookie_name"]));
        return;
      }
      else {
				echo json_encode( authorize($_POST["username"], $_POST["password"], $this->auth["service_name"], $this->auth["cookie_name"]) );
        // echo json_encode(authorize($this->auth["table"], $this->auth["fields"],
        //   $this->auth["username_field"], $this->auth["password_field"], $this->auth["id_field"],
        //   $_POST["username"], hash("sha512", $_POST["password"], true),
        //   $this->auth["service_name"], $this->auth["cookie_name"]
        // ));
        return;
      }
  }

  /**
   * Method for accessing `user` resources.
   * Parameters:
   * @param $param - (Optional) The parameter used to uniquely identify the
   *  specific resource. If nothing is specified, the root resource will be
   *  set as the target.
   */
  public function users($param=''){
    header('Content-Type: application/json');
    // Create - POST
    if($this->input->method(true) == 'POST'){
      echo json_encode(createResourceElement(
        $this->users["table"], $this->users["create_fields"], $this->users["create_types"],
        [$_POST["username"],
        password_hash(hash("sha512", $_POST["password"], true), PASSWORD_DEFAULT),
        $_POST["email"]]
      ));
      return;
    }
    // Update - PUT
    if($this->input->method(true) == 'PUT'){
      if(check_jwt_cookie($this->auth["service_name"], $this->auth["cookie_name"])){
        $jwt = get_jwt_data($this->auth["cookie_name"]);
        $_PUT = get_request_body();
        if(isset($jwt["username"]) && $jwt["username"] == $_PUT["username"]) {
          echo json_encode(updateResourceElement(
            $this->users["table"], $this->users["update_fields"], "s",
            [$_PUT["email"]], $this->users["update_key"], $_PUT["username"]
          ));
          return;
        }
        echo json_encode(array(
          "code" => BAD_CREDENTIALS,
          "message" => "Token does not match the provided credentials."
        ));
        return;
      }
      echo json_encode(array(
        "code" => NO_COOKIE,
        "message" => "Token not found or invalid."
      ));
      return;
    }
    // Delete - DELETE
    if($this->input->method(true) == 'DELETE'){
      if(check_jwt_cookie($this->auth["service_name"], $this->auth["cookie_name"])){
        $jwt = get_jwt_data($this->auth["cookie_name"]);
        $_DELETE = get_request_body();
        if(isset($jwt["username"]) && $jwt["username"] == $_DELETE["username"]) {
          echo json_encode(deleteResourceElement(
            $this->users["table"], $this->users["delete_key"], $_DELETE["username"]
          ));
          return;
        }
        echo json_encode(array(
          "code" => BAD_CREDENTIALS,
          "message" => "Token does not match the provided credentials."
        ));
        return;
      }
      echo json_encode(array(
        "code" => NO_COOKIE,
        "message" => "Token not found or invalid."
      ));
      return;
    }
    // Read - GET
    if($param == ''){
      echo json_encode(readResourceRoot( $this->users["table"], $this->users["read_fields"]));
      return;
    }
    else{
      $data = readResourceElement(
        $this->users["table"], $this->users["read_fields"], $this->users["read_key"],
        $param
      );
      if($data == false)
        echo json_encode(array(
          "code" => BAD_DATA,
          "message" => "No user with this username."
        ));
      else
        echo json_encode($data[0]);
    }
  }

	public function tab( $username ) {
		$this->load->model('user_model');

		if ($this->ion_auth->username_check($username)) {
			// get user's info
			$data['user_info'] = $this->user_model->get_info($username);
			$user_id = $data['user_info']->row()->id;
			$tab_totals = $this->user_model->get_totals($user_id);

			// if sorted
			if ( isset( $_GET['sort'] ) ) {
				$sort_by = $_GET['sort'];
				$this->session->set_userdata('tab_sort', $_GET['sort']);
			}
			elseif ( $this->session->userdata('tab_sort') ) {
				$sort_by = $this->session->userdata('tab_sort');
			}
			else {
				$sort_by = 'recent';
			}

			if ($sort_by == 'rating-high') {
				$data['sort_by'] = 'Highest Rating';
			} elseif ($sort_by == 'rating-low') {
				$data['sort_by'] = 'Lowest Rating';
			} else {
				$data['sort_by'] = $sort_by;
			}

			if ( isset( $_GET['p'] ) ) {
				$offset = $_GET['p'] - 1;
			} else {
				$offset = 0;
			}

			if ( isset( $_GET['favorites'] ) ) {
				$fav = true;
			} else {
				$fav = false;
			}

			// if searching tab, get search
			// otherwise get full tab
			if ( isset( $_GET['s'] ) ) {
				$s = $this->input->get('s');
				$tab = $this->user_model->search_tab($username, $s, $sort_by);
				$data['s'] = $s;
				// $tab = $this->tab_search($username);
			}
			else {
		    $tab = $this->user_model->get_tab($username, $sort_by, $fav, $limit = 25, $offset);
			}

			// tab info to display
			$data['tab'] = $tab;
			$data['tab_totals'] = $tab_totals;
			$data['num_pages'] = $tab_totals['total_beers']/25;
			$data['current_page'] = ( isset( $_GET['p'] ) ? $_GET['p'] : 1 );

			// display page
			if ( $tab->num_rows() < 1 ) {
				$data['main_content'] = 'user/user_tab_empty';
			}
			else {
				$data['main_content'] = 'user/user_tab';
			}

			$data['title'] = $username;
		}
		else {
			echo json_encode(array(
				"code" => BAD_DATA,
				"message" => "No user with this username."
			));
			die;
		}

		// echo '<pre>';
		header('Content-Type: application/json');
		echo json_encode( array(
			"code" => SUCCESS,
			"userInfo" => $data['user_info']->row(),
			"tabTotals" => $data['tab_totals'],
			"userTab" => $data['tab']->result_array(),
			"numPages" => $data['num_pages'],
			"currentPage" => $data['current_page'],
		), JSON_PRETTY_PRINT );
	}

	public function add() {
    header('Content-Type: application/json');
    // Create - POST
		if( $_SERVER['REQUEST_METHOD'] !== "POST" ) {
			echo json_encode(array("message:"=>"Use the HTTP POST method to login to the system."));
			return;
		}

		if( ! check_jwt_cookie($this->auth["service_name"], $this->auth["cookie_name"]) ) {
			echo json_encode(array(
        "code" => NO_COOKIE,
        "message" => "Token not found or invalid."
      ));
      return;
		}

    $jwt = get_jwt_data($this->auth["cookie_name"]);
    if( ! isset($jwt["username"]) ) {
			echo json_encode(array(
				"code" => BAD_CREDENTIALS,
				"message" => "Token does not match the provided credentials."
			));
		}

		// successful request, let's go
		// echo json_encode( var_dump($request) ); return;
		$user_id = $jwt['id'];
		$beer_id = $_POST['beerId'];
		$notes = trim( $_POST['notes'] );
		$public = ( $_POST['public'] == 'on' ? 1 : 0 );
		$rating = ( $_POST['rating'] == '' ? 0 : $_POST['rating'] );
		$beer_name = $_POST['beerName'];
		$brewery_name = $_POST['breweryName'];
		$style = $_POST['beerStyle'];
		$beer_image = ( isset($_POST['beerImage']) ? $_POST['beerId'] : null );
		$favorite = ( $_POST['favorite'] == 'on' ? 1 : 0 );
		$file = ( isset($_FILES['addphoto']) ? $_FILES['addphoto'] : false );

		//reset session variables for sharing options
		if ( $public ) {
			$this->session->set_userdata('comment_public', true);
		}
		else {
			$this->session->set_userdata('comment_public', false);
		}

		$this->load->model('user_model');
		// add to tab and grab comment id if there's a comment
		$add_response = $this->user_model->add_to_tab($user_id, $beer_id, $notes, $public, $rating, $beer_name, $brewery_name, $style, $beer_image, $favorite);
		$note_id = $add_response['note_id'];
		$row_id = $add_response['row_id'];

		// upload user photo
		if ( $file && $file['size'] > 0 ) {
			$filename = $file['tmp_name'];
			$filetype = $file['type'];
			$filesize = filesize($filename);

			if ( $filetype != 'image/jpeg' && $filetype != 'image/png' ) {
				// wrong file format
				$response['error'] = 'Your photo needs to be in JPG or PNG format.';
			} elseif ( $filesize > 8388608 ) {
				// file larger than 8MB
				$response['error'] = 'Your photo needs to be less than 8MB.';
			} else {
				$uploaded_photo = $this->upload_photo( $file, $beer_id );

				if ( isset($uploaded_photo['success']) ) {
					// set photo url in database
					$photo_url = $uploaded_photo['success'];
					$photo_id = $uploaded_photo['photo_id'];
					$this->load->model('beer_model');
					$this->beer_model->upload_photo($user_id, $row_id, $beer_id, $photo_url, $photo_id, $public);
					$response['photo'] = $photo_id;
				} else {
					$response['error'] = $uploaded_photo;
				}
			}
		}

		// return comment to append if it's public
		if ( $notes && $public ) {
			$this->load->model('beer_model');
			$comment = $this->beer_model->get_comments($beer_id, $note_id);
			$response['public_comment'] = $comment->row();
		}

		if ( isset($response['error']) ) {
			echo json_encode(array(
        "code" => BAD_DATA,
        "message" => $response['error']
      ));
      return;
		}

		if ( isset($response['public_comment']) ) {
			$success_message = $response['public_comment'];
		} else {
			$success_message = 'Beer added to your tab.';
		}

		echo json_encode(array(
			"code" => SUCCESS,
			"message" => $success_message,
		));
		return;
	}

	public function search()
	{
		header('Content-Type: application/json');

		$this->load->helper('form');

		if ( ! isset($_GET['q']) ) {
			return;
		}

		$query = $this->input->get('q');
		$page = $this->input->get('p');
		$this->load->model('search_model');
		$results = $this->search_model->search_results($query, $page);

		echo json_encode( array(
			"code" => SUCCESS,
			"status" => $results['status'],
			"query" => $query,
			"currentPage" => $results['currentPage'],
			"numberOfPages" => $results['numberOfPages'],
			"totalResults" => $results['totalResults'],
			"results" => $results['data'],
		) );

		return;
	}

	function search_more() {
		$query = $this->input->get('q');
		$page = $this->input->get('p');

		$this->load->model('search_model');
		$data['results'] = $this->search_model->search_results($query, $page);

		$this->load->view('search/search_results_more', $data);
	}

  /**
   * Index method for completeness. Returns a JSON response with an
   * error message.
   */
  public function index(){
    header('Content-Type: application/json');
    echo json_encode(array(
      "code" => BAD_DATA,
      "message"=>"No resource specified."
  ));
  }
}
?>
