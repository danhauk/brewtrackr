<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Search extends CI_Controller {

	public function index() {
		$this->load->helper('form');

		if ( isset($_GET['q']) ) {
			$query = $this->input->get('q');
			$page = $this->input->get('p');
			$this->load->model('search_model');
			$results = $this->search_model->search_results($query, $page);
			
			$data['results'] = $results;
			$data['in_tab'] = $this->check_tab( $results );
			$data['query'] = $query;
			$data['main_content'] = 'search/search_results';
		}
		else {
			$data['main_content'] = 'search/search';
		}

		$this->load->view('includes/template', $data);
	}

	function search_more() {
		$query = $this->input->get('q');
		$page = $this->input->get('p');

		$this->load->model('search_model');
		$results = $this->search_model->search_results($query, $page);

		$data['results'] = $results;
		$data['in_tab'] = $this->check_tab( $results );

		$this->load->view('search/search_results_more', $data);
	}

	function check_tab( $results ) {
		$this->load->model('user_model');
		$user_id = $user_id = $this->ion_auth->user()->row()->id;
		$in_tab = array();

		foreach( $results['data'] as $result ) {
			$beer_id = $result['id'];
			$is_in_tab = $this->user_model->search_check_tab($user_id, $beer_id);

			if ( $is_in_tab ) {
				array_push( $in_tab, $is_in_tab );
			}
		}

		return $in_tab;
	}

}