<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	function __construct()
	{
		session_start();

		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth','form_validation'));
		$this->load->helper(array('url','language'));

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');
	}

	// redirect if needed, otherwise display the user list
	function index()
	{

		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		elseif (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			// set the flash data error message if there is one
			$this->data['message-error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message-error');

			//list the users
			$this->data['users'] = $this->ion_auth->users()->result();
			foreach ($this->data['users'] as $k => $user)
			{
				$this->data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
			}

			$this->data['main_content'] = 'auth/index';
			$this->_render_page('includes/template', $this->data);
		}
	}

	function signup_modal()
	{
		//validate form input
		$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
		$this->form_validation->set_rules('email', 'Email Address', 'trim|required|xss_clean|valid_email');
		if ( !$this->input->post('facebook_id') ) { // checking if it's a facebook signup
			$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
		}

		if ($this->form_validation->run() == true)
		{
			// check to see if the user submitted the form

			$username = trim($this->input->post('username'));
			$username = strtolower($username);
			$email = $this->input->post('email');
			$additional_data = array();
			$group = null;
			if ( $this->input->post('facebook_id') ) {
				$facebook_id = $this->input->post('facebook_id');
				$facebook_name = $this->input->post('facebook_name');
				$facebook_token = $this->input->post('facebook_token');
				$password = $this->config->item('master_password', 'ion_auth');
				$facebook_login = array(
					'facebook_id' => $facebook_id,
					'facebook_name' => $facebook_name,
					'facebook_token' => $facebook_token
				);
			}
			else {
				$password = $this->input->post('password');
				$facebook_login = array();
			}

			if ($this->ion_auth->username_check($username)) {
				// check if username is taken
				$errorData = array(
					'error' => array(
						'message' => 'Username is already taken'
					)
				);
				$errorDataJSON = json_encode($errorData);
				print $errorDataJSON;
			}
			elseif ( preg_match('/[^a-z_\-0-9]/i', $username) ) {
				// username contains invalid characters
				$errorData = array(
					'error' => array(
						'message' => 'Username can only contain letters, numbers, underscores, or dashes'
					)
				);
				$errorDataJSON = json_encode($errorData);
				print $errorDataJSON;
			}
			elseif ($this->ion_auth->email_check($email)) {
				// check if email address is registered
				// username contains invalid characters
				$errorData = array(
					'error' => array(
						'message' => 'Email address is already registered'
					)
				);
				$errorDataJSON = json_encode($errorData);
				print $errorDataJSON;
			}
			else {
				// username and email are available!
				if ($this->ion_auth->register($username, $password, $email, $additional_data, $group, $facebook_login))
				{
					//if the signup is successful
					//check for gravatar or fb photo & set if available
					$this->load->model('user_model');
					$photo_url = $this->user_model->set_picture($email, $facebook_login);

					//log them in
					if ($this->ion_auth->login($username, $password, true))
					{
						//if the login is successful
						// send welcome email, slack notification, etc.
						if ( defined('ENVIRONMENT') && ENVIRONMENT === 'production' ) {
							$this->signup_hooks($email, $username, $facebook = false);
						}

						// send user data to add to firebase
						$userData = array(
							'success' => array(
								'email' => $email,
								'userId' => $this->ion_auth->user()->row()->id,
								'username' => $username,
								'password' => $password,
								'bio' => '',
								'photoUrl' => $photo_url,
								'isAdmin' => false,
								'isPremium' => false
							)
						);

						if (!empty($facebook_login)) {
							$userData['success']['fLoginId'] = $facebook_login['facebook_id'];
							$userData['success']['fLoginName'] = $facebook_login['facebook_name'];
							$userData['success']['fLoginToken'] = $facebook_login['facebook_token'];
						}

						$userDataJSON = json_encode($userData);

						// redirect to home and show welcome message
						$this->session->set_flashdata('notify_success', 'Welcome to BrewTrackr!');
						// redirect('/'.$username, 'refresh');
						// echo 'success';
						print $userDataJSON;
					}
					else {
						// if the login after signup was un-successful
						$errorData = array(
							'error' => array(
								'message' => $this->ion_auth->errors()
							)
						);
						$errorDataJSON = json_encode($errorData);
						print $errorDataJSON;
					}
				}
				else
				{
					// if the signup was un-successful
					// show errors
					// if the login was un-successful
					$errorData = array(
						'error' => array(
							'message' => $this->ion_auth->errors()
						)
					);
					$errorDataJSON = json_encode($errorData);
					print $errorDataJSON;
				}
			}
		}
		else {
			// user did not submit form
			$validation_errors = (validation_errors() ? validation_errors() : null);

			$errorData = array(
				'error' => array(
					'message' => $validation_errors
				)
			);
			$errorDataJSON = json_encode($errorData);
			print $errorDataJSON;
		}
	}

	// sign up new user
	function signup()
	{
		// $this->data['title'] = "Sign up";

		// //validate form input
		// $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
		// $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
		// $this->form_validation->set_rules('email', 'Email Address', 'trim|required|xss_clean|valid_email');

		// if ($this->form_validation->run() == true)
		// {
		// 	// check to see if the user submitted the form

		// 	$username = $this->input->post('username');
		// 	$password = $this->input->post('password');
		// 	$email = $this->input->post('email');
		// 	$additional_data = array();
		// 	$group = null;
		// 	$facebook_login = ( $this->input->post('facebook_id') ? $this->input->post('facebook_id') : null );

		// 	$url_referer = $this->input->post('url_referrer');
		// 	$url_referer = explode('?', $url_referer);
		// 	$url_referer = $url_referer[0];

		// 	if ($this->ion_auth->username_check($username)) {
		// 		// check if username is taken
		// 		$this->session->set_flashdata('auth_error', 'Username is already taken');
		// 		redirect($url_referer.'?auth=signup', 'refresh');
		// 	}
		// 	elseif ($this->ion_auth->email_check($email)) {
		// 		// check if email address is registered
		// 		$this->session->set_flashdata('auth_error', 'Email address is already registered');
		// 		redirect($url_referer.'?auth=signup', 'refresh');
		// 	}
		// 	else {
		// 		// username and email are available!
		// 		if ($this->ion_auth->register($username, $password, $email, $additional_data, $group, $facebook_login))
		// 		{
		// 			//if the signup is successful
		// 			//check for gravatar & set if available
		// 			$this->load->model('user_model');
		// 			$this->user_model->set_gravatar($email);

		// 			//log them in
		// 			if ($this->ion_auth->login($username, $password, true))
		// 			{
		// 				//if the login is successful
		// 				// send welcome email, slack notification, etc.
		// 				if ( defined('ENVIRONMENT') && ENVIRONMENT === 'production' ) {
		// 					$this->signup_hooks($email, $username);
		// 				}

		// 				// redirect to home and show welcome message
		// 				$this->session->set_flashdata('notify_success', 'Welcome to BrewTrackr!');
		// 				redirect('/'.$username, 'refresh');
		// 			}
		// 			else {
		// 				$this->session->set_flashdata('message_error', $this->ion_auth->errors());
		// 				redirect($url_referer.'?auth=signup', 'refresh');
		// 				// redirect('/signup', 'refresh');
		// 			}
		// 		}
		// 		else
		// 		{
		// 			// if the signup was un-successful
		// 			// show errors
		// 			$this->session->set_flashdata('message_error', $this->ion_auth->errors());
		// 			redirect($url_referer.'?auth=signup', 'refresh');
		// 		}
		// 	}
		// }
		// else
		// {
		// 	// the user did not submit form so display signup form
		// 	// set the flash data error message if there is one
		// 	// $this->data['message_error'] = (validation_errors() ? validation_errors() : null);
		// }

		// $this->data['main_content'] = 'auth/register';
		// $this->load->view('includes/template', $this->data);
		$this->load->view('auth/register-modal');
		$this->session->unset_userdata('facebook_auth_error');
	}

	function signup_hooks($email, $username, $facebook = null)
	{
		// welcome email
		$email_data['username'] = $username;
		$email_data['content'] = $this->load->view('email/welcome.php', $email_data, true);
		$message = $this->load->view('email/template/includes.tpl.php', $email_data, true);

		$this->load->library('postmark');
		$this->postmark->clear();
		$this->postmark->from('notifications@brewtrackr.com', 'BrewTrackr');
		$this->postmark->to($email);
		$this->postmark->reply_to('hello@brewtrackr.com', 'Reply To');
		$this->postmark->subject('Hi, '.$username.'! Welcome to BrewTrackr :)');
		$this->postmark->message_plain($message);
		$this->postmark->message_html($message);
		$this->postmark->send();

		// get number of users and
		// send Telegram message
		$num_users = $this->db->count_all('users');
		$this->load->library('curl');
		$telegram = array(
			"chat_id" => "167612443",
			"text" => "${num_users} users. Welcome {$username}!"
		);
		if ( $facebook ) {
			$telegram['text'] .= " (Facebook Login)";
		}
		$api_url = 'https://api.telegram.org/bot274951587:AAEpGjaummN5aTJsbjigv81lq7I6ehCrw2s/sendMessage';
		$this->curl->_simple_call( 'post', $api_url, $telegram );

		// add email address to MailChimp
		$mailchimp = array(
			"email_address" => strtolower($email),
			"status" => "subscribed",
			"merge_fields" => array("USERNAME" => $username)
		);
		$mailchimp_json = json_encode($mailchimp);
		$mailchimp_api = "https://us4.api.mailchimp.com/3.0/lists/c0bbe4ae63/members/";
		$this->curl->create($mailchimp_api);
		$this->curl->option(CURLOPT_BUFFERSIZE, 10);
		$this->curl->option(CURLOPT_USERPWD, "brewtrackr:972c5ca2a721b1a68aa5cf2fcb765a0f-us4");
		$this->curl->post($mailchimp_json);
		$this->curl->execute();
	}

	function sessdestroy() {
		session_start();
		session_destroy();
	}

	function facebook()
	{
		if ( $this->session->userdata('facebook_return_url') ){
			$facebook_return_url = $this->session->userdata('facebook_return_url');
		}
		else {
			$facebook_return_url = $_SERVER['HTTP_REFERER'];
		}

		$this->config->load('facebook');
		$fb_app_id = $this->config->item('facebook_app_id');
		$fb_app_secret = $this->config->item('facebook_app_secret');

		require_once APPPATH . 'libraries/Facebook/autoload.php';

		$fb = new Facebook\Facebook([
		  'app_id' => $fb_app_id,
		  'app_secret' => $fb_app_secret,
		  'default_graph_version' => 'v2.4',
		  ]);

		$helper = $fb->getRedirectLoginHelper();

		// try to get the longlived token from session
		// if not available, get short lived access token
		try {
		  $accessToken = $helper->getAccessToken();
		} catch(Facebook\Exceptions\FacebookResponseException $e) {
		  // When Graph returns an error
		  echo 'Graph returned an error: ' . $e->getMessage();
		  exit;
		} catch(Facebook\Exceptions\FacebookSDKException $e) {
		  // When validation fails or other local issues
		  echo 'Facebook SDK returned an error: ' . $e->getMessage();
		  exit;
		}

		if ( !isset($accessToken) ) {
			// user is not logged in
			// let's create a login url and redirect them
			$permissions = ['publish_actions', 'email']; // optional
			$loginUrl = $helper->getLoginUrl( site_url('auth/facebook'), $permissions );

			redirect($loginUrl);
		}
		else {
			// user is logged in!
	  	// extend the user token
		  $oAuth2Client = $fb->getOAuth2Client();
		  $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);

		  $_SESSION['facebook_longlived_token'] = (string) $accessToken;

			try {
			  $response = $fb->get('/me?fields=id,name,email', $accessToken);
			  $userNode = $response->getGraphUser();
			  // $this->session->set_flashdata('notify_success', 'Connected to Facebook as ' . $userNode['name'] . '!');
			} catch(Facebook\Exceptions\FacebookResponseException $e) {
			  // When Graph returns an error
				// $this->session->set_flashdata('notify_error', 'Graph returned an error: ' . $e->getMessage());
			  echo 'Graph returned an error: ' . $e->getMessage();
			  exit;
			} catch(Facebook\Exceptions\FacebookSDKException $e) {
			  // When validation fails or other local issues
				// $this->session->set_flashdata('notify_error', 'Facebook SDK returned an error: ' . $e->getMessage());
			  echo 'Facebook SDK returned an error: ' . $e->getMessage();
			  exit;
			}

			$this->facebook_check($userNode, $accessToken);

		}
	}

	function facebook_check($userNode, $accessToken)
	{
		$facebook_id = $userNode['id'];
		$facebook_name = $userNode['name'];
		$email = $userNode['email'];

		$this->load->model('user_model');
		$user_exists = $this->user_model->check_facebook_login($facebook_id, $email);

		if ( $user_exists ) {
			if ( $user_exists->row()->fb_login_id || $user_exists->row()->facebook_id ) {
				// using facebook login
				// login with master password
				$username = $user_exists->row()->username;
				$password = $this->config->item('master_password', 'ion_auth');
				$this->login_modal($username, $password);
			}
			else {
				// using old auth
				// return an error
				$this->session->set_userdata('facebook_auth_error', 'Your email address is already registered with a username and password authentication. Please login with your username and password. <a href="/forgot_password" class="alert-box-link">Forgot password?</a>');
				redirect('/?auth=facebook&error=user', 'refresh');
			}
		}
		else {
			// user doesn't exist
			$_SESSION['facebook_id'] = $facebook_id;
			$_SESSION['facebook_name'] = $facebook_name;
			$_SESSION['email_address'] = $email;
			$_SESSION['facebook_longlived_token'] = (string) $accessToken;
			$this->session->set_userdata('facebook_id', $facebook_id);
			$this->session->set_userdata('facebook_name', $facebook_name);
			$this->session->set_userdata('email_address', $email);
			$this->session->set_userdata('facebook_longlived_token', $accessToken);

			if ($this->facebook_register($facebook_id, $facebook_name, $email, $accessToken) ) {
				redirect('/?auth=fuser');
			}
		}
	}

	function facebook_register($facebook_id, $facebook_name, $email, $facebook_token)
	{
		$password = $this->config->item('master_password', 'ion_auth');
		$facebook_login = array(
			'facebook_id' => $facebook_id,
			'facebook_name' => $facebook_name,
			'facebook_token' => (string) $facebook_token
		);

		if ($this->ion_auth->register($facebook_id, $password, $email, $additional_data = array(), $group = null, $facebook_login))
		{
			//if the signup is successful
			//check for gravatar or fb photo & set if available
			$this->load->model('user_model');
			$photo_url = $this->user_model->set_picture($email, $facebook_login);

			return true;
		}
		else {
			// signup was unsuccessful
			echo $this->ion_auth->errors();
		}
	}

	function save_username()
	{
		$this->load->helper('security');

		$username = xss_clean( $this->input->post('username') );
		$facebook_id = xss_clean( $this->input->post('facebook_id') );

		$this->load->model('user_model');
		$saved = $this->user_model->save_username($facebook_id, $username);

		if ( $saved ) {
			$password = $this->config->item('master_password', 'ion_auth');
			$this->login_modal($username, $password, $set_username = true);
		}
		else {
			$errorData = array(
				'error' => array(
					'message' => "We couldn't save your username, but you still have an account. Try closing this pop-up and logging in with the Facebook button."
				)
			);
			$errorDataJSON = json_encode($errorData);
			print $errorDataJSON;
		}
	}

	function check_username() {
		$username = $this->input->post('username');

		if ($this->ion_auth->username_check($username)) {
			// check if username is taken
			$response = array(
				'error' => array(
					'message' => 'Username is already taken'
				)
			);
		}
		elseif ( preg_match('/[^a-z_\-0-9]/i', $username) ) {
			// username contains invalid characters
			$response = array(
				'error' => array(
					'message' => 'Username can only contain letters, numbers, underscores, or dashes'
				)
			);
		}
		else {
			$response = array( 'success' );
		}

		print json_encode($response);
	}

	function check_email() {
		$email = $this->input->post('email');

		if ($this->ion_auth->email_check($email)) {
			// check if email address is registered
			// username contains invalid characters
			$response = array(
				'error' => array(
					'message' => 'Email address is already registered'
				)
			);
		}
		else {
			$response = array( 'success' );
		}

		print json_encode($response);
	}

	function set_username()
	{
		$this->load->view('auth/set_username');
	}

	function login_modal($username = null, $password = null, $set_username = null)
	{
		if ( !$username && !$password ) {
			//validate form input
			$this->form_validation->set_rules('identity', 'Username', 'required|xss_clean');
			$this->form_validation->set_rules('password', 'Password', 'required|xss_clean');

			if ($this->form_validation->run() == true)
			{
				// check to see if the user is logging in
				// check for "remember me"
				// $remember = (bool) $this->input->post('remember');
				$remember = true;

				if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), $remember))
				{
					$response['success'] = true;
					$res = json_encode($response);

					//redirect them to their tab
					// $this->session->set_flashdata('message', $this->ion_auth->messages());
					echo $res;
					// redirect('/'.$username, 'refresh');
				}
				else
				{
					// if the login was un-successful
					$response['error'] = $this->ion_auth->errors();
					$errorDataJSON = json_encode($response);
					echo $errorDataJSON;
				}
			}
			else
			{
				// form validation is false
				// return error message
				$validation_errors = (validation_errors()) ? validation_errors() : null;
				$response['error'] = $validation_errors;
				$errorDataJSON = json_encode($response);
				echo $errorDataJSON;
			}
		}
		else {
			$remember = true;

			if ($this->ion_auth->login($username, $password, $remember))
			{
				// if the login is successful
				// add social info to session
				$this->load->model('user_model');
				$social = $this->user_model->get_social_by_id($this->ion_auth->user()->row()->id);
				if ($social->num_rows() > 0) {
					$this->session->set_userdata('twitter_username', $social->row()->twitter_username);
					$this->session->set_userdata('facebook_name', $social->row()->facebook_name);
				}

				if ( isset($_SESSION['facebook_longlived_token']) ) {
					$user_id = $this->ion_auth->user()->row()->id;
					$facebook_token = $_SESSION['facebook_longlived_token'];
					$this->user_model->update_facebook_token($user_id, $facebook_token);
				}

				if ( $set_username ) {
					// if this is a first-time Facebook user
					// and we are setting the username, send a JSON response
					// but first, signup hooks
					$this->load->model('user_model');
					$userInfo = $this->user_model->get_info($username);
					$email = $userInfo->row()->email;
					if ( defined('ENVIRONMENT') && ENVIRONMENT === 'production' ) {
						$this->signup_hooks($email, $username, $facebook = true);
					}

					$response = array( 'success' );
					$response = json_encode($response);
					print $response;
				}
				else {
					// not setting username, just regular page redirect
					redirect('/'.$username, 'refresh');
				}
			}
			else
			{
				// if the login was un-successful
				$errorData['error'] = $this->ion_auth->errors();
				$errorDataJSON = json_encode($errorData);
				echo $errorDataJSON;
			}
		}
	}

	// log the user in
	function login()
	{
		// $this->data['title'] = "Login";

		// //validate form input
		// $this->form_validation->set_rules('identity', 'Email/Username', 'required|xss_clean');
		// $this->form_validation->set_rules('password', 'Password', 'required|xss_clean');

		// if ($this->form_validation->run() == true)
		// {
		// 	// check to see if the user is logging in
		// 	// check for "remember me"
		// 	$remember = (bool) $this->input->post('remember');

		// 	if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), $remember))
		// 	{
		// 		//if the login is successful
		// 		// add social info to session
		// 		$this->load->model('user_model');
		// 		$social = $this->user_model->get_social_by_id($this->ion_auth->user()->row()->id);
		// 		if ($social->num_rows() > 0) {
		// 			$this->session->set_userdata('twitter_username', $social->row()->twitter_username);
		// 			$this->session->set_userdata('facebook_name', $social->row()->facebook_name);
		// 		}
		// 		//redirect them back to the home page
		// 		$this->session->set_flashdata('message', $this->ion_auth->messages());
		// 		redirect('/', 'refresh');
		// 	}
		// 	else
		// 	{
		// 		// if the login was un-successful
		// 		// redirect them back to the login page
		// 		$this->session->set_flashdata('message', $this->ion_auth->errors());
		// 		redirect('auth/login', 'refresh'); // use redirects instead of loading views for compatibility with MY_Controller libraries
		// 	}
		// }
		// else
		// {
		// 	// the user is not logging in so display the login page
		// 	// set the flash data error message if there is one
		// 	$this->data['message_error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

		// 	$this->data['identity'] = array('name' => 'identity',
		// 		'id'    => 'identity',
		// 		'type'  => 'text',
		// 		'value' => $this->form_validation->set_value('identity'),
		// 		'tabindex' => '1'
		// 	);
		// 	$this->data['password'] = array('name' => 'password',
		// 		'id'   => 'password',
		// 		'type' => 'password',
		// 		'tabindex' => '2'
		// 	);

			// $this->data['main_content'] = 'auth/login';
			// $this->_render_page('includes/template', $this->data);
			$this->load->view('auth/login-modal');
			$this->session->unset_userdata('facebook_auth_error');
	}

	function logout()
	{
		$logout = $this->ion_auth->logout();

		// redirect them to the login page
		$this->session->set_flashdata('message_success', $this->ion_auth->messages());
		redirect('/', 'refresh');
	}

	// log the user out
	function logout_db()
	{
		$this->data['title'] = "Logout";

		// log the user out
		$logout = $this->ion_auth->logout();

		// redirect them to the login page
		$this->session->set_flashdata('message_success', $this->ion_auth->messages());
		redirect('/', 'refresh');
	}

	// change password
	function change_password()
	{
		$this->form_validation->set_rules('old', $this->lang->line('change_password_validation_old_password_label'), 'required');
		$this->form_validation->set_rules('new', $this->lang->line('change_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
		$this->form_validation->set_rules('new_confirm', $this->lang->line('change_password_validation_new_password_confirm_label'), 'required');

		if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login', 'refresh');
		}

		$user = $this->ion_auth->user()->row();

		if ($this->form_validation->run() == false)
		{
			// display the form
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			$this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
			$this->data['old_password'] = array(
				'name' => 'old',
				'id'   => 'old',
				'type' => 'password',
			);
			$this->data['new_password'] = array(
				'name'    => 'new',
				'id'      => 'new',
				'type'    => 'password',
				'pattern' => '^.{'.$this->data['min_password_length'].'}.*$',
			);
			$this->data['new_password_confirm'] = array(
				'name'    => 'new_confirm',
				'id'      => 'new_confirm',
				'type'    => 'password',
				'pattern' => '^.{'.$this->data['min_password_length'].'}.*$',
			);
			$this->data['user_id'] = array(
				'name'  => 'user_id',
				'id'    => 'user_id',
				'type'  => 'hidden',
				'value' => $user->id,
			);

			// render
			$this->_render_page('auth/change_password', $this->data);
		}
		else
		{
			$identity = $this->session->userdata('identity');

			$change = $this->ion_auth->change_password($identity, $this->input->post('old'), $this->input->post('new'));

			if ($change)
			{
				//if the password was successfully changed
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				$this->logout();
			}
			else
			{
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect('auth/change_password', 'refresh');
			}
		}
	}

	// forgot password
	function forgot_password()
	{
		// setting validation rules by checking wheather identity is username or email
		if($this->config->item('identity', 'ion_auth') != 'email' )
		{
		   $this->form_validation->set_rules('identity', $this->lang->line('forgot_password_identity_label'), 'required');
		}
		else
		{
		   $this->form_validation->set_rules('email', $this->lang->line('forgot_password_validation_email_label'), 'required|valid_email');
		}


		if ($this->form_validation->run() == false)
		{
			// setup the input
			$this->data['identity'] = array('name' => 'identity',
				'id'    => 'identity',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('identity'),
				'tabindex' => '1'
			);

			if ( $this->config->item('identity', 'ion_auth') != 'email' ){
				$this->data['identity_label'] = $this->lang->line('forgot_password_identity_label');
			}
			else
			{
				$this->data['identity_label'] = $this->lang->line('forgot_password_email_identity_label');
			}

			// set any errors and display the form
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->data['title'] = 'Forgot password';
			$this->data['main_content'] = 'auth/forgot_password';
			$this->_render_page('includes/template', $this->data);
		}
		else
		{
			$identity_column = $this->config->item('identity','ion_auth');
			$identity = $this->ion_auth->where($identity_column, $this->input->post('identity'))->users()->row();

			if(empty($identity)) {

	            		if($this->config->item('identity', 'ion_auth') != 'email')
		            	{
		            		$this->ion_auth->set_error('forgot_password_identity_not_found');
		            	}
		            	else
		            	{
		            	   $this->ion_auth->set_error('forgot_password_email_not_found');
		            	}

		                $this->session->set_flashdata('message', $this->ion_auth->errors());
                		redirect("/forgot_password", 'refresh');
            		}

			// run the forgotten password method to email an activation code to the user
			$forgotten = $this->ion_auth->forgotten_password($identity->{$this->config->item('identity', 'ion_auth')});

			if ($forgotten)
			{
				// if there were no errors
				$this->session->set_flashdata('notify_success', 'Password reset email was sent!');
				$this->session->set_flashdata("show_login", true);
				redirect("/", 'refresh'); //we should display a confirmation page here instead of the login page
			}
			else
			{
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect("/forgot_password", 'refresh');
			}
		}
	}

	// reset password - final step for forgotten password
	public function reset_password($code = NULL)
	{
		if (!$code)
		{
			show_404();
		}

		$user = $this->ion_auth->forgotten_password_check($code);

		if ($user)
		{
			// if the code is valid then display the password reset form

			$this->form_validation->set_rules('new', $this->lang->line('reset_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
			$this->form_validation->set_rules('new_confirm', $this->lang->line('reset_password_validation_new_password_confirm_label'), 'required');

			if ($this->form_validation->run() == false)
			{
				// display the form

				// set the flash data error message if there is one
				$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

				$this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
				$this->data['new_password'] = array(
					'name' => 'new',
					'id'   => 'new',
					'type' => 'password',
					'pattern' => '^.{'.$this->data['min_password_length'].'}.*$',
				);
				$this->data['new_password_confirm'] = array(
					'name'    => 'new_confirm',
					'id'      => 'new_confirm',
					'type'    => 'password',
					'pattern' => '^.{'.$this->data['min_password_length'].'}.*$',
				);
				$this->data['user_id'] = array(
					'name'  => 'user_id',
					'id'    => 'user_id',
					'type'  => 'hidden',
					'value' => $user->id,
				);
				$this->data['csrf'] = $this->_get_csrf_nonce();
				$this->data['code'] = $code;

				// render
				$this->data['main_content'] = 'auth/reset_password';
				$this->_render_page('includes/template', $this->data);
			}
			else
			{
				// do we have a valid request?
				if ($this->_valid_csrf_nonce() === FALSE || $user->id != $this->input->post('user_id'))
				{

					// something fishy might be up
					$this->ion_auth->clear_forgotten_password_code($code);

					show_error($this->lang->line('error_csrf'));

				}
				else
				{
					// finally change the password
					$identity = $user->{$this->config->item('identity', 'ion_auth')};

					$change = $this->ion_auth->reset_password($identity, $this->input->post('new'));

					if ($change)
					{
						// if the password was successfully changed
						$this->session->set_flashdata('notify_success', $this->ion_auth->messages());
						$this->session->set_flashdata('show_login', true);
						redirect("/", 'refresh');
					}
					else
					{
						$this->session->set_flashdata('notify_error', $this->ion_auth->errors());
						redirect('/auth/reset_password/' . $code, 'refresh');
					}
				}
			}
		}
		else
		{
			// if the code is invalid then send them back to the forgot password page
			$this->session->set_flashdata('message', $this->ion_auth->errors());
			redirect("/forgot_password", 'refresh');
		}
	}


	// activate the user
	function activate($id, $code=false)
	{
		if ($code !== false)
		{
			$activation = $this->ion_auth->activate($id, $code);
		}
		else if ($this->ion_auth->is_admin())
		{
			$activation = $this->ion_auth->activate($id);
		}

		if ($activation)
		{
			// redirect them to the auth page
			$this->session->set_flashdata('message', $this->ion_auth->messages());
			redirect("auth", 'refresh');
		}
		else
		{
			// redirect them to the forgot password page
			$this->session->set_flashdata('message', $this->ion_auth->errors());
			redirect("auth/forgot_password", 'refresh');
		}
	}

	// deactivate the user
	function deactivate($id = NULL)
	{
		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}

		$id = (int) $id;

		$this->load->library('form_validation');
		$this->form_validation->set_rules('confirm', $this->lang->line('deactivate_validation_confirm_label'), 'required');
		$this->form_validation->set_rules('id', $this->lang->line('deactivate_validation_user_id_label'), 'required|alpha_numeric');

		if ($this->form_validation->run() == FALSE)
		{
			// insert csrf check
			$this->data['csrf'] = $this->_get_csrf_nonce();
			$this->data['user'] = $this->ion_auth->user($id)->row();

			$this->_render_page('auth/deactivate_user', $this->data);
		}
		else
		{
			// do we really want to deactivate?
			if ($this->input->post('confirm') == 'yes')
			{
				// do we have a valid request?
				if ($this->_valid_csrf_nonce() === FALSE || $id != $this->input->post('id'))
				{
					show_error($this->lang->line('error_csrf'));
				}

				// do we have the right userlevel?
				if ($this->ion_auth->logged_in() && $this->ion_auth->is_admin())
				{
					$this->ion_auth->deactivate($id);
				}
			}

			// redirect them back to the auth page
			redirect('auth', 'refresh');
		}
	}

	// create a new user
	function create_user()
    {
        $this->data['title'] = "Create User";

        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
        {
            redirect('auth', 'refresh');
        }

        $tables = $this->config->item('tables','ion_auth');
        $identity_column = $this->config->item('identity','ion_auth');
        $this->data['identity_column'] = $identity_column;

        // validate form input
        // $this->form_validation->set_rules('first_name', $this->lang->line('create_user_validation_fname_label'), 'required');
        // $this->form_validation->set_rules('last_name', $this->lang->line('create_user_validation_lname_label'), 'required');
        if($identity_column!=='email')
        {
            $this->form_validation->set_rules('identity',$this->lang->line('create_user_validation_identity_label'),'required|is_unique['.$tables['users'].'.'.$identity_column.']');
            $this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'required|valid_email');
        }
        else
        {
            $this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'required|valid_email|is_unique[' . $tables['users'] . '.email]');
        }
        $this->form_validation->set_rules('phone', $this->lang->line('create_user_validation_phone_label'), 'trim');
        $this->form_validation->set_rules('company', $this->lang->line('create_user_validation_company_label'), 'trim');
        $this->form_validation->set_rules('password', $this->lang->line('create_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
        // $this->form_validation->set_rules('password_confirm', $this->lang->line('create_user_validation_password_confirm_label'), 'required');

        if ($this->form_validation->run() == true)
        {
            $email    = strtolower($this->input->post('email'));
            $identity = ($identity_column==='email') ? $email : $this->input->post('identity');
            $password = $this->input->post('password');

            $additional_data = array(
                // 'first_name' => $this->input->post('first_name'),
                // 'last_name'  => $this->input->post('last_name'),
                // 'company'    => $this->input->post('company'),
                // 'phone'      => $this->input->post('phone'),
            );
        }
        if ($this->form_validation->run() == true && $this->ion_auth->register($identity, $password, $email, $additional_data))
        {
            // check to see if we are creating the user
            // redirect them back to the admin page
            $this->session->set_flashdata('message', $this->ion_auth->messages());
            redirect("auth", 'refresh');
        }
        else
        {
            // display the create user form
            // set the flash data error message if there is one
            $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

            $this->data['first_name'] = array(
                'name'  => 'first_name',
                'id'    => 'first_name',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('first_name'),
            );
            $this->data['last_name'] = array(
                'name'  => 'last_name',
                'id'    => 'last_name',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('last_name'),
            );
            $this->data['identity'] = array(
                'name'  => 'identity',
                'id'    => 'identity',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('identity
                '),
            );
            $this->data['email'] = array(
                'name'  => 'email',
                'id'    => 'email',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('email'),
            );
            $this->data['company'] = array(
                'name'  => 'company',
                'id'    => 'company',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('company'),
            );
            $this->data['phone'] = array(
                'name'  => 'phone',
                'id'    => 'phone',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('phone'),
            );
            $this->data['password'] = array(
                'name'  => 'password',
                'id'    => 'password',
                'type'  => 'password',
                'value' => $this->form_validation->set_value('password'),
            );
            $this->data['password_confirm'] = array(
                'name'  => 'password_confirm',
                'id'    => 'password_confirm',
                'type'  => 'password',
                'value' => $this->form_validation->set_value('password_confirm'),
            );

            $this->_render_page('auth/create_user', $this->data);
        }
    }

	// edit a user
	function edit_user($id)
	{
		$this->data['title'] = "Edit User";

		if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !($this->ion_auth->user()->row()->id == $id)))
		{
			redirect('auth', 'refresh');
		}

		$user = $this->ion_auth->user($id)->row();
		$groups=$this->ion_auth->groups()->result_array();
		$currentGroups = $this->ion_auth->get_users_groups($id)->result();

		// validate form input
		$this->form_validation->set_rules('first_name', $this->lang->line('edit_user_validation_fname_label'), 'required');
		$this->form_validation->set_rules('last_name', $this->lang->line('edit_user_validation_lname_label'), 'required');

		if (isset($_POST) && !empty($_POST))
		{
			// do we have a valid request?
			if ($this->_valid_csrf_nonce() === FALSE || $id != $this->input->post('id'))
			{
				show_error($this->lang->line('error_csrf'));
			}

			// update the password if it was posted
			if ($this->input->post('password'))
			{
				$this->form_validation->set_rules('password', $this->lang->line('edit_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
				$this->form_validation->set_rules('password_confirm', $this->lang->line('edit_user_validation_password_confirm_label'), 'required');
			}

			if ($this->form_validation->run() === TRUE)
			{
				$data = array(
					'first_name' => $this->input->post('first_name'),
					'last_name'  => $this->input->post('last_name')
				);

				// update the password if it was posted
				if ($this->input->post('password'))
				{
					$data['password'] = $this->input->post('password');
				}



				// Only allow updating groups if user is admin
				if ($this->ion_auth->is_admin())
				{
					//Update the groups user belongs to
					$groupData = $this->input->post('groups');

					if (isset($groupData) && !empty($groupData)) {

						$this->ion_auth->remove_from_group('', $id);

						foreach ($groupData as $grp) {
							$this->ion_auth->add_to_group($grp, $id);
						}

					}
				}

			// check to see if we are updating the user
			   if($this->ion_auth->update($user->id, $data))
			    {
			    	// redirect them back to the admin page if admin, or to the base url if non admin
				    $this->session->set_flashdata('message', $this->ion_auth->messages() );
				    if ($this->ion_auth->is_admin())
					{
						redirect('auth', 'refresh');
					}
					else
					{
						redirect('/', 'refresh');
					}

			    }
			    else
			    {
			    	// redirect them back to the admin page if admin, or to the base url if non admin
				    $this->session->set_flashdata('message', $this->ion_auth->errors() );
				    if ($this->ion_auth->is_admin())
					{
						redirect('auth', 'refresh');
					}
					else
					{
						redirect('/', 'refresh');
					}

			    }

			}
		}

		// display the edit user form
		$this->data['csrf'] = $this->_get_csrf_nonce();

		// set the flash data error message if there is one
		$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

		// pass the user to the view
		$this->data['user'] = $user;
		$this->data['groups'] = $groups;
		$this->data['currentGroups'] = $currentGroups;

		$this->data['first_name'] = array(
			'name'  => 'first_name',
			'id'    => 'first_name',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('first_name', $user->first_name),
		);
		$this->data['last_name'] = array(
			'name'  => 'last_name',
			'id'    => 'last_name',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('last_name', $user->last_name),
		);
		$this->data['password'] = array(
			'name' => 'password',
			'id'   => 'password',
			'type' => 'password'
		);
		$this->data['password_confirm'] = array(
			'name' => 'password_confirm',
			'id'   => 'password_confirm',
			'type' => 'password'
		);

		$this->data['main_content'] = 'auth/edit_user';
		$this->_render_page('includes/template', $this->data);
	}

	// create a new group
	function create_group()
	{
		$this->data['title'] = $this->lang->line('create_group_title');

		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		{
			redirect('auth', 'refresh');
		}

		// validate form input
		$this->form_validation->set_rules('group_name', $this->lang->line('create_group_validation_name_label'), 'required|alpha_dash');

		if ($this->form_validation->run() == TRUE)
		{
			$new_group_id = $this->ion_auth->create_group($this->input->post('group_name'), $this->input->post('description'));
			if($new_group_id)
			{
				// check to see if we are creating the group
				// redirect them back to the admin page
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect("auth", 'refresh');
			}
		}
		else
		{
			// display the create group form
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

			$this->data['group_name'] = array(
				'name'  => 'group_name',
				'id'    => 'group_name',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('group_name'),
			);
			$this->data['description'] = array(
				'name'  => 'description',
				'id'    => 'description',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('description'),
			);

			$this->_render_page('auth/create_group', $this->data);
		}
	}

	// edit a group
	function edit_group($id)
	{
		// bail if no group id given
		if(!$id || empty($id))
		{
			redirect('auth', 'refresh');
		}

		$this->data['title'] = $this->lang->line('edit_group_title');

		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		{
			redirect('auth', 'refresh');
		}

		$group = $this->ion_auth->group($id)->row();

		// validate form input
		$this->form_validation->set_rules('group_name', $this->lang->line('edit_group_validation_name_label'), 'required|alpha_dash');

		if (isset($_POST) && !empty($_POST))
		{
			if ($this->form_validation->run() === TRUE)
			{
				$group_update = $this->ion_auth->update_group($id, $_POST['group_name'], $_POST['group_description']);

				if($group_update)
				{
					$this->session->set_flashdata('message', $this->lang->line('edit_group_saved'));
				}
				else
				{
					$this->session->set_flashdata('message', $this->ion_auth->errors());
				}
				redirect("auth", 'refresh');
			}
		}

		// set the flash data error message if there is one
		$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

		// pass the user to the view
		$this->data['group'] = $group;

		$readonly = $this->config->item('admin_group', 'ion_auth') === $group->name ? 'readonly' : '';

		$this->data['group_name'] = array(
			'name'    => 'group_name',
			'id'      => 'group_name',
			'type'    => 'text',
			'value'   => $this->form_validation->set_value('group_name', $group->name),
			$readonly => $readonly,
		);
		$this->data['group_description'] = array(
			'name'  => 'group_description',
			'id'    => 'group_description',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('group_description', $group->description),
		);

		$this->_render_page('auth/edit_group', $this->data);
	}


	function _get_csrf_nonce()
	{
		$this->load->helper('string');
		$key   = random_string('alnum', 8);
		$value = random_string('alnum', 20);
		$this->session->set_flashdata('csrfkey', $key);
		$this->session->set_flashdata('csrfvalue', $value);

		return array($key => $value);
	}

	function _valid_csrf_nonce()
	{
		if ($this->input->post($this->session->flashdata('csrfkey')) !== FALSE &&
			$this->input->post($this->session->flashdata('csrfkey')) == $this->session->flashdata('csrfvalue'))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	function _render_page($view, $data=null, $returnhtml=false)//I think this makes more sense
	{

		$this->viewdata = (empty($data)) ? $this->data: $data;

		$view_html = $this->load->view($view, $this->viewdata, $returnhtml);

		if ($returnhtml) return $view_html;//This will return html on 3rd argument being true
	}

}
