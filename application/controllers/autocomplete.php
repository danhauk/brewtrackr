<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Autocomplete extends CI_Controller {

  function __construct() {
    parent::__construct();
  }

  public function brewery()
  {
    $brewery_name = $this->input->get('q');

    $this->load->library('brewerydb');
    $params = array('name' => '*'.$brewery_name.'*');
    try {
      $results = $this->brewerydb->request('breweries', $params, 'GET');
      if (isset($results['data'])) {
        $list = $results['data'];

        array_slice($list, 0, 5);
      } else {
        $list = null;
      }

      $response = json_encode($list);
      echo $response;
    } catch (Exception $e) {
      $results = array('error' => $e->getMessage());
    }
  }
}