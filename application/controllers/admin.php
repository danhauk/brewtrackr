<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

  function __construct() {
    parent::__construct();
  }

  public function index()
  {
    $data['main_content'] = 'admin/index';
    $this->load->view('admin/index', $data);
  }

  public function migrate()
  {
    if ($this->ion_auth->logged_in() && $this->ion_auth->user()->row()->id == 2) {

      if ($this->uri->segment(3) == "submission") {
        // get all styles to display
        $url = APPPATH . 'third_party/beerStyles.json';
        $content = file_get_contents($url);
        $styles = json_decode($content, true);

        uasort($styles['data'], function($i, $j) {
          $a = $i['name'];
          $b = $j['name'];

          if ($a == $b) {
            return 0;
          } elseif ($a > $b) {
            return 1;
          } else {
            return -1;
          }
        });

        $data['styles'] = $styles['data'];
        $data['main_content'] = 'admin/migrate_submissions';
        $this->load->view('includes/template', $data);
      }

    } else {
      redirect('/');
    }
  }

  public function do_submit()
  {
		$this->load->model('beer_model');
    $name = $this->input->post('beer_name');
    $brewery = $this->input->post('brewery');
    $breweryName = $this->input->post('brewery_name');
    $abv = $this->input->post('abv');
    $ibu = $this->input->post('ibu');
    $styleId = $this->input->post('styleId');
    $styleName = $this->input->post('style');
    $description = $this->input->post('description');
    $user_id = $this->input->post('user_id');

		$abv = explode( '%', $abv );
    $abv = $abv[0];

    // get a beer_id from bitly
    $longUrl = $_SERVER['HTTP_REFERER'] . '&timestamp=' . time();
		$bitly = $this->beer_model->get_shortlink($longUrl);
    $bitly_id = $bitly['data']['global_hash'];
    $beer_id = 'brwtk-' . $bitly_id;

    // submit beer to local database
    $submission = $this->beer_model->submit_beer($beer_id, $name, $breweryName, $abv, $ibu, $styleName, $description, $user_id);

    // add beer to user's tab
    if ( $submission ) {
      $res_status = 'success';
      $res_message = 'Success';

      if (!$user_id) {
        $user_id = $this->ion_auth->user()->row()->id;
      }

      $this->load->model('user_model');
      $db_row_id = $this->user_model->add_to_tab($user_id, $beer_id, null, false, 0, $name, $breweryName, $styleName, null, 0, true);

      if ( $user_id == $this->ion_auth->user()->row()->id ) {
        // send me an email
        $email_data = array(
          'username' => $this->ion_auth->user()->row()->username,
          'name' => $name,
          'brewery' => $breweryName,
          'style' => $styleName,
          'abv' => $abv,
          'ibu' => $ibu,
          'description' => $description
        );
        $email_data['content'] = $this->load->view('email/beer_submission.php', $email_data, true);
        $message = $this->load->view('email/template/includes.tpl.php', $email_data, true);
        $this->load->library('postmark');
        $this->postmark->clear();
        $this->postmark->from('notifications@brewtrackr.com', 'BrewTrackr');
        $this->postmark->to('hauk.dan@gmail.com');
        $this->postmark->reply_to('hello@brewtrackr.com', 'Reply To');
        $this->postmark->subject($name.' was added to BrewTrackr');
        $this->postmark->message_plain($message);
        $this->postmark->message_html($message);
        $this->postmark->send();
      }
    } else {
      $res_status = 'error';
      $res_message = 'Something went wrong submitting your beer.';
      $db_row_id = null;
    }

    $response = array(
      'status' => $res_status,
      'beerId' => $beer_id,
      'beerName' => $name,
      'rowId' => $db_row_id,
      'message' => $res_message
    );

    $res = json_encode($response);
    echo $res;
  }

	public function email() {
		if ($this->ion_auth->logged_in() && $this->ion_auth->user()->row()->id == 2) {
			$data['main_content'] = 'admin/send_email';
			$this->load->view('includes/template', $data);
		} else {
			redirect('/');
		}
	}

	public function send_email() {
		if ($this->ion_auth->logged_in() && $this->ion_auth->user()->row()->id == 2) {
			$email_subject = $this->input->post('subject');
			$this->load->library('noxssinput');
			$email_content['email_content'] = $this->noxssinput->post('content', false);
			$data['content'] = $this->load->view('email/generic', $email_content, true);
			// $this->load->view('email/template/includes', $data);
			$message = $this->load->view('email/template/includes.tpl.php', $data, true);
			$this->load->library('postmark');

			$this->load->model('user_model');
			$users = $this->user_model->get_all_users_email();
			foreach( $users->result() as $user ) {
				$this->postmark->clear();
				$this->postmark->from('hello@brewtrackr.com', 'Dan from BrewTrackr');
				$this->postmark->to($user->email);
				$this->postmark->reply_to('hello@brewtrackr.com', 'Reply To');
				$this->postmark->subject($email_subject);
				$this->postmark->message_plain($message);
				$this->postmark->message_html($message);
				$this->postmark->send();
			}

			$this->load->view('email/template/includes', $data);
		} else {
			redirect('/');
		}
	}
}
