<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Brewery extends CI_Controller {

	public function info($id) {

		// get brewery info with API call
		$this->load->library('brewerydb');
		$params = array();
		try {
		    $brewery_info = $this->brewerydb->request('brewery/'.$id, $params, 'GET'); // where $params is a keyed array of parameters to send with the API call.
		} catch (Exception $e) {
		    $brewery_info = array('error' => $e->getMessage());
		}

		// get brewery's beers with API call
		// $params = array();
		try {
		    $brewery_beers = $this->brewerydb->request('brewery/'.$id.'/beers', $params, 'GET'); // where $params is a keyed array of parameters to send with the API call.
		} catch (Exception $e) {
		    $brewery_beers = array('error' => $e->getMessage());
		}

		// display brewery info
		$data['brewery_info'] = $brewery_info['data'];
		$data['brewery_beers'] = $brewery_beers['data'];
		$data['main_content'] = 'brewery_info';
		$data['title'] = $brewery_info['data']['name'];
		$this->load->view('includes/template', $data);
	}

}