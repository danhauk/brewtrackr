<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Review extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('beer_model');
		$this->load->model('user_model');
	}

	public function delete()
	{
		$data['review_id'] = $this->input->get('id');
		$this->load->view('beer/comment_delete', $data);
	}

	public function do_delete()
	{
		$review_id = $this->input->post('reviewId');
		$rows_removed = $this->beer_model->delete_review( $review_id );

		if ( $rows_removed === 1 ) {
			$response = 'success';
		} else {
			$response = $rows_removed;
		}

		echo $response;
	}

	public function add()
	{
		
	}

}
