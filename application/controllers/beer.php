<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Beer extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('beer_model');
	}

	public function info($beer_id) {

		$beer_id = explode('-', $beer_id);
		if ( $beer_id[0] == 'brwtk' ) {
			$beer_id = $beer_id[0].'-'.$beer_id[1];
		} else {
			$beer_id = $beer_id[0];
		}

		if ( $beer_id == 'abcdef' ) {
			// get dummy beer info
			$results = $this->beer_model->dummy_beer();
		}
		else {
			// check if beer exists in local table first
			$results = $this->beer_model->get_beer_info( $beer_id );
		}

		if ( !isset($results['errorMessage']) ) {
			// beer exists!

			// check if added to user's tab
			// check if premium user
			$data['is_in_tab'] = 0;
			if ( $this->ion_auth->logged_in() ) {
				$this->load->model('user_model');

				$user_id = $this->ion_auth->user()->row()->id;
				$data['is_in_tab'] = $this->user_model->check_tab($user_id, $beer_id);

				$data['premium_user'] = $this->user_model->is_premium($user_id);
			}

			// check for reviews
			$this->load->model('beer_model');
			$data['comments'] = $this->beer_model->get_comments($beer_id);

			// display beer info
			if ( isset( $results['data'] ) ) {
				$beer_info = $results['data'];
				$brewery_name = (isset($beer_info['breweries'][0]['name']) ? $beer_info['breweries'][0]['name'] : null);
				$beer_style = (isset($beer_info['style']['name']) ? $beer_info['style']['name'] : null);
				$beer_id = $beer_info['id'];
			} else {
				$beer_info = $results[0];
				$brewery_name = (isset($beer_info['brewery']) ? $beer_info['brewery'] : null);
				$beer_style = (isset($beer_info['style']) ? $beer_info['style'] : null);
				$beer_id = $beer_info['beer_id'];
			}
			$data['info'] = $beer_info;
			$data['main_content'] = 'beer/beer_info';

			$beer_name = $beer_info['name'];
			$data['beer_id'] = $beer_id;
			$data['beer_name'] = $beer_name;
			$data['brewery_name'] = $brewery_name;
			$data['style'] = $beer_style;
			if ( $brewery_name ) {
				$page_title = $beer_name . ' by ' . $brewery_name;
			}
			else {
				$page_title = $beer_name;
			}
			$data['title'] = $page_title;
			$this->load->view('includes/template', $data);
		}
		else {
			// beer doesn't exist!
			$data['title'] = "404 Not Found";
			$data['main_content'] = "beer/404";
			$this->load->view('includes/template', $data);
		}
	}

	public function submit()
	{
		if ( !$this->ion_auth->logged_in() ) {
			$this->load->view('user/noaccess_modal');
			return;
		}

		// get all styles to display
		$url = APPPATH . 'third_party/beerStyles.json';
		$content = file_get_contents($url);
		$styles = json_decode($content, true);

		uasort($styles['data'], function($i, $j) {
			$a = $i['name'];
			$b = $j['name'];

			if ($a == $b) {
				return 0;
			} elseif ($a > $b) {
				return 1;
			} else {
				return -1;
			}
		});

		// check if user is premium
		$this->load->model('user_model');
		$user_id = $this->ion_auth->user()->row()->id;
		$data['premium_user'] = $this->user_model->is_premium($user_id);

		$data['user_id'] = $user_id;
		$data['styles'] = $styles['data'];
		$this->load->view('beer/beer_submit_form', $data);
	}

}
