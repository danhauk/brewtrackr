<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Search_model extends CI_Model
{

	public function __construct()
    {
            // Call the CI_Model constructor
            parent::__construct();
    }

    public function search_results($query, $page = NULL) {
    	$this->load->library('brewerydb');
		$params = array('q' => $query, 'type' => 'beer', 'withBreweries' => 'Y', 'p' => $page);
		try {
		    // The first argument to request() is the endpoint you want to call
		    // 'brewery/BrvKTz', 'beers', etc.
		    // The third parameter is the HTTP method to use (GET, PUT, POST, or DELETE)
		    $results = $this->brewerydb->request('search', $params, 'GET'); // where $params is a keyed array of parameters to send with the API call.
		} catch (Exception $e) {
		    $results = array('error' => $e->getMessage());
		}

		return $results;
    }

}