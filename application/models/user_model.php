<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model
{

	public function __construct()
    {
            // Call the CI_Model constructor
            parent::__construct();
    }

    //=====================
    // USER TAB DISPLAY
    //=====================

    function get_tab($username, $order_by = NULL, $fav = null, $limit = 25, $offset = 0) {
    	// get user_id
    	$this->db->where('username', $username);
        $user_query = $this->db->get('users');
        $user_id = $user_query->row()->id;

        // query beer_tab table
				$this->db->select('beer_tab.*');
        $this->db->from('beer_tab');
        // $this->db->where('users.email', $email);
    	// $this->db->from('beer_tab');
        $this->db->where('beer_tab.user_id', $user_id);
        if ( $fav ) {
            $this->db->where('beer_tab.favorite', 1);
        }
        if($order_by == 'rating-high')
        {
            $this->db->order_by('beer_tab.rating', 'desc');
            $this->db->order_by('beer_tab.favorite', 'desc');
        }
        elseif($order_by == 'rating-low')
        {
            $this->db->order_by('beer_tab.rating', 'asc');
            $this->db->order_by('beer_tab.favorite', 'asc');
        }
        elseif($order_by == 'a-z')
        {
            $this->db->order_by('beer_tab.beer_name', 'asc');
        }
        elseif($order_by == 'z-a')
        {
            $this->db->order_by('beer_tab.beer_name', 'desc');
        }
        elseif($order_by == 'brewery')
        {
            $this->db->order_by('beer_tab.brewery_name', 'asc');
            $this->db->order_by('beer_tab.beer_name', 'asc');
        }
        else
        {
            $this->db->order_by('beer_tab.id', 'desc');
        }
        $this->db->limit($limit, $offset*$limit);
        $query = $this->db->get();

        return $query;
    }

    function tab_info_by_id($id) {
				$this->db->select('beer_tab.*');
				$this->db->from('beer_tab');
        $this->db->where('id', $id);
        $query = $this->db->get();

        return $query;
    }

    function search_tab($username, $query, $order_by = NULL) {
        // get user_id
        $this->db->where('username', $username);
        $user_query = $this->db->get('users');
        $user_id = $user_query->row()->id;

        // query beer_tab table

        $this->db->where('user_id', $user_id);
        $this->db->where("(beer_name LIKE '%".$query."%' OR brewery_name LIKE '%".$query."%' OR beer_style LIKE '%".$query."%')");

        if($order_by == 'rating-high')
        {
            $this->db->order_by('rating', 'desc');
            $this->db->order_by('favorite', 'desc');
        }
        elseif($order_by == 'rating-low')
        {
            $this->db->order_by('rating', 'asc');
            $this->db->order_by('favorite', 'asc');
        }
        elseif($order_by == 'a-z')
        {
            $this->db->order_by('beer_name', 'asc');
        }
        elseif($order_by == 'z-a')
        {
            $this->db->order_by('beer_name', 'desc');
        }
        elseif($order_by == 'brewery')
        {
            $this->db->order_by('brewery_name', 'asc');
            $this->db->order_by('beer_name', 'asc');
        }
        else
        {
            $this->db->order_by('id', 'desc');
        }

        $query = $this->db->get('beer_tab');

        return $query;
    }

    function check_tab($user_id, $beer_id) {
        $this->db->where('user_id', $user_id);
        $this->db->where('beer_id', $beer_id);
        $query = $this->db->get('beer_tab');

        return $query->num_rows();
    }

    function search_check_tab($user_id, $beer_id) {
        $this->db->where('user_id', $user_id);
        $this->db->where('beer_id', $beer_id);
        $query = $this->db->get('beer_tab');

        if ( $query->num_rows() ) {
            return $query->row()->beer_id;
        } else {
            return false;
        }
    }

    function get_totals($user_id) {
        $this->db->where('user_id', $user_id);
        $totals['total_beers'] = $this->db->count_all_results('beer_tab');

        $this->db->where('user_id', $user_id);
        $this->db->where('favorite', 1);
        $totals['total_favs'] = $this->db->count_all_results('beer_tab');

        return $totals;
    }

		function get_tab_export( $user_id ) {
			$this->db->select( 'beer_name, brewery_name, beer_style, rating, notes, favorite, date_added' );
			$this->db->from( 'beer_tab' );
			$this->db->where( 'user_id', $user_id );
			$query = $this->db->get();

			return $query;
		}

    //=====================
    // USER INFO
    //=====================

    function get_info($username) {
        $this->db->select('id, email, username, photo_url, bio, private');
    	$this->db->from('users');
        // $this->db->join('users_social', 'users_social.user_id = users.id', 'left');
        // $this->db->join('users_groups', 'users.id = users_groups.user_id', 'left');
        // $this->db->join('groups', 'users_groups.group_id = groups.id', 'left');
        $this->db->where('users.username', $username);
    	$query = $this->db->get();

    	return $query;
    }

    function is_premium($user_id) {
        $this->db->where('user_id', $user_id);
        $this->db->where('group_id', 3);
        $query = $this->db->get('users_groups');

        return $query->num_rows();
    }

    function get_info_by_id($user_id) {
        $this->db->where('id', $user_id);
        $query = $this->db->get('users');

        return $query;
    }

    function get_social_by_id($user_id) {
        $this->db->where('user_id', $user_id);
        $query = $this->db->get('users_social');

        return $query;
    }

    function set_picture($email, $facebook_login = null) {
        $photo_url = null;

        if ( !empty($facebook_login) ) {
            $facebook_id = $facebook_login['facebook_id'];
            $photo_url = 'https://graph.facebook.com/'.$facebook_id.'/picture?type=square';
        }
        else {
            $email = trim($email);
            $hash = md5($email);
            $gravatar = "https://secure.gravatar.com/avatar/{$hash}?d=404";

            if ( $gravatar_exists = @getimagesize($gravatar) ) {
                $grav = "https://secure.gravatar.com/avatar/{$hash}?s=200";
                $photo_url = $grav;
            }
        }

        $this->db->set('photo_url', $photo_url);
        $this->db->where('email', $email);
        $this->db->update('users');

        return $photo_url;
    }

    function check_facebook_login($facebook_id, $email)
    {
        // check if user exists in database
        $this->db->select('users.*, users_social.user_id, users_social.facebook_id, users_social.facebook_token');
        $this->db->from('users');
        $this->db->join('users_social', 'users.id = users_social.user_id', 'left');
        $this->db->where('users.email', $email);
        $this->db->or_where('users_social.facebook_id', $facebook_id);
        $this->db->or_where('users.fb_login_id', $facebook_id);
        $query = $this->db->get();

        if ( $query->num_rows() > 0 ) {
            // user exists
            return $query;
        }
        else {
            // user doesn't exist
            return false;
        }
    }

		function save_username($facebook_id, $username) {
			$this->db->set('username', $username);
			$this->db->where('fb_login_id', $facebook_id);
			$this->db->update('users');

			return $this->db->affected_rows();
		}

		function update_facebook_token($user_id, $facebook_token)
		{
			$this->db->set('facebook_token', $facebook_token);
			$this->db->where('user_id', $user_id);
			$this->db->update('users_social');

			return $this->db->affected_rows();
		}

		function disconnect_social( $user_id, $service ) {
			if ( $service == 'Twitter' ) {
				$this->db->set('twitter_id', null);
				$this->db->set('twitter_username', null);
				$this->db->set('twitter_token', null);
				$this->db->set('twitter_secret', null);
			}

			if ( $service == 'Facebook' ) {
				$this->db->set('facebook_id', null);
				$this->db->set('facebook_name', null);
				$this->db->set('facebook_token', null);
			}

			$this->db->update('users_social');

			return $this->db->affected_rows();
		}

    //=====================
    // USER TAB ACTIONS
    //=====================

    function add_to_tab($user_id, $beer_id, $notes, $rating, $beer_name, $brewery_name, $style, $favorite, $description, $abv, $ibu, $photo_url)
    {
        $now = date('Y-m-d H:i:s');

        // insert to beer_tab table
        $insert_data_tab = array(
            'user_id' => $user_id,
            'beer_id' => $beer_id,
            'beer_name' => $beer_name,
            'beer_style' => $style,
            'brewery_name' => $brewery_name,
            'notes' => $notes,
            'rating' => $rating,
            'favorite' => $favorite,
						'description' => $description,
						'abv' => $abv,
						'ibu' => $ibu,
						'photo_url' => $photo_url,
            'date_added' => $now
        );
        $this->db->insert('beer_tab', $insert_data_tab);
        $row_id = $this->db->insert_id();
				return $row_id;
    }

    function update_tab($tab_id, $notes, $rating, $beer_name, $brewery_name, $style, $favorite, $description, $abv, $ibu, $photo_url) {
      $update = array(
          'rating' => $rating,
          'notes' => $notes,
          'favorite' => $favorite,
					'beer_name' => $beer_name,
					'beer_style' => $style,
					'brewery_name' => $brewery_name,
					'description' => $description,
					'photo_url' => $photo_url,
					'abv' => $abv,
					'ibu' => $ibu
      );
      $this->db->where('id', $tab_id);
      $this->db->update('beer_tab', $update);

      return $this->db->affected_rows();
    }

    function remove_from_tab($beer_id, $user_id) {
        $this->db->where('beer_id', $beer_id);
        $this->db->where('user_id', $user_id);
        $this->db->delete('beer_tab');

        return $this->db->affected_rows();
    }

    //=====================
    // USER PROFILE ACTIONS
    //=====================

    function update_photo($user_id, $photo_url) {
        $update = array(
            'photo_url' => $photo_url
        );
        $this->db->where('id', $user_id);
        $this->db->update('users', $update);
    }

    function update_profile($user_id, $email, $bio) {
        $this->db->set('email', $email);
        if ( strlen($bio) > 0 ) {
            $this->db->set('bio', $bio);
        }
        $this->db->where('id', $user_id);
        $this->db->update('users');
    }

    //=====================
    // DELETE ACCOUNT
    //=====================

    function delete_account($user_id) {
        $user_tables = array("beer_tab", "beer_comments", "users_groups", "users_social");
        $this->db->where('user_id', $user_id);
        $this->db->delete($user_tables);

        $this->db->where('id', $user_id);
        $this->db->delete('users');

        return $this->db->affected_rows();
    }

		//=====================
    // ADMIN
    //=====================

		function get_all_users_email() {
			$this->db->select('username, email');
			$this->db->from('users');
			$query = $this->db->get();
			return $query;
		}

}
