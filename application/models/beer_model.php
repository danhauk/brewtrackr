<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Beer_model extends CI_Model
{

	public function __construct()
    {
            // Call the CI_Model constructor
            parent::__construct();
    }

    function get_comments($beer_id = null, $comment_id = null)
    {
        $this->db->select('beer_comments.*, users.id as user_id, users.username, users.photo_url as user_photo');
        $this->db->from('beer_comments');
        $this->db->join('users', 'beer_comments.user_id = users.id', 'left');
        if ( $comment_id ) {
            $this->db->where('beer_comments.id', $comment_id);
        }
        else {
            $this->db->where('beer_comments.beer_id', $beer_id);
        }
        $this->db->order_by('beer_comments.id', 'asc');
        $query = $this->db->get();

        return $query;
    }

    function get_shortlink($longUrl = null) {
        if ( empty($longUrl) ) {
            $longUrl = current_url();
        }

        include_once(APPPATH.'libraries/Bitly.php');
        $bitly_params = array(
            'access_token' => '9d565af30efc0fe02bc6bdaaffb2889dd6cec2b6',
            'longUrl' => $longUrl,
            'domain' => 'brwtk.co'
            );
        $results = bitly_get('shorten', $bitly_params);

        return $results;
    }

    function submit_beer($beer_id, $name, $brewery, $abv, $ibu, $style, $description, $user_id)
    {
        $now = date('Y-m-d H:i:s');

        $params = array(
          'beer_id' => $beer_id,
          'name' => $name,
          'brewery' => $brewery,
          'abv' => $abv,
          'ibu' => $ibu,
          'style' => $style,
          'description' => $description,
          'user_id' => $user_id,
          'date_added' => $now
        );

        $this->db->insert('beers', $params);
        return $this->db->insert_id();
    }

    function delete_review($review_id)
    {
        $this->db->where('id', $review_id);
        $this->db->delete('beer_comments');

        return $this->db->affected_rows();
    }

		function upload_photo($user_id, $beer_id, $photo_url, $photo_id, $public = 0)
		{
			// add photo url to tab table
			// $this->db->set('photo_url', $photo_url);
			// $this->db->where('id', $row_id);
			// $this->db->update('beer_tab');

			// insert to beer_photos table
			$now = date('Y-m-d H:i:s');
			$insert_data_tab = array(
					'user_id' => $user_id,
					'beer_id' => $beer_id,
					'photo_url' => $photo_url,
					'photo_id' => $photo_id,
					'date_added' => $now
			);
			$this->db->insert('beer_photos', $insert_data_tab);
			$row_id = $this->db->insert_id();

			return $row_id;
    }

    // add beer to local table if it doesn't exist
    function add_to_table($beer_id, $beer_name, $brewery_name, $style, $beer_image, $description, $abv, $ibu, $user_id) {
        $now = date('Y-m-d H:i:s');

        // check if already in table
        $check = $this->check_local_table( $beer_id );

        if ( $check->num_rows() < 1 ) {
            // insert to beers table
            $insert_data = array(
                'beer_id' => $beer_id,
                'name' => $beer_name,
                'brewery' => $brewery_name,
                'style' => $style,
                'img' => $beer_image,
                'description' => $description,
                'abv' => $abv,
                'ibu' => $ibu,
                'user_id' => $user_id,
                'date_added' => $now
            );
            $this->db->insert('beers', $insert_data);
            $row_id = $this->db->insert_id();

            return $row_id;
        }
    }

    function check_local_table( $beer_id ) {
        $this->db->where('beer_id', $beer_id);
        $check = $this->db->get('beers');

        return $check;
    }

    // get beer from local table first
    // if it doesn't exist, send API call
    function get_beer_info( $beer_id ) {
        // check if exists local first
        $check = $this->check_local_table( $beer_id );

        if ( $check->num_rows() ) {
            return $check->result_array();
        } else {
            // get beer info with API call
			$this->load->library('brewerydb');
			$params = array('withBreweries' => 'Y');
			try {
		        $results = $this->brewerydb->request('beer/'.$beer_id, $params, 'GET');
			} catch (Exception $e) {
			    $results = array('error' => $e->getMessage());
            }

            // insert into table for future
            $user_id = $this->ion_auth->user()->row()->id;
            $result = $results['data'];
            $beer_id = $result['id'];
            $beer_name = $result['nameDisplay'];
            $brewery_name = (isset($result['breweries'][0]) ? $result['breweries'][0]['name'] : '');
            $style = (isset($result['style']['name']) ? $result['style']['name'] : '');
            $description = (isset($result['description']) ? $result['description'] : '');
            $abv = (isset($result['abv']) ? $result['abv'] : null);
            $ibu = (isset($result['ibu']) ? $result['ibu'] : null);
            if ( isset( $result['images']['medium'] ) ) {
                $beer_image = $result['images']['medium'];
            }
            elseif ( isset( $result['labels']['medium'] ) ) {
                $beer_image = $result['labels']['medium'];
            }
            else {
                $beer_image = '/img/no-beer.png';
            }

            $this->add_to_table($beer_id, $beer_name, $brewery_name, $style, $beer_image, $description, $abv, $ibu, $user_id);

            return $results;
        }
    }

    // edit comment

    // used for testing offline
    function dummy_beer() {
        $breweries = array(
            array( 'id' => 'abcdef', 'name' => 'Pennside Brewing Company' )
            );
        $labels = array( 'medium' => '/img/no-beer.png' );
        $style = array( 'name' => 'Imperial IPA' );

        $data = array(
            'id' => 'abcdef',
            'name' => 'Crash Test Dummy',
            'abv' => '9.2',
            'ibu' => 78,
            'description' => 'This is just a test beer for offline development purposes. Lorem ipsum dolor sit amet consecteteur.',
            'breweries' => $breweries,
            'labels' => $labels,
            'style' => $style
        );

        $dummy_call = array(
            'data' => $data
        );

        return $dummy_call;
    }

}
