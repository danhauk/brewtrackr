<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "welcome";
$route['404_override'] = '';
$route['search'] = 'search';
$route['search/(:any)'] = 'search/$1';
$route['test'] = 'welcome/test';
$route['admin'] = 'admin';
$route['admin/(:any)'] = 'admin/$1';
$route['api'] = 'api';
$route['api/(:any)'] = 'api/$1';

// auth
$route['login'] = 'auth/login';
$route['signup'] = 'auth/signup';
$route['logout'] = 'auth/logout';
$route['forgot_password'] = 'auth/forgot_password';
$route['reset_password'] = 'auth/reset_password';
$route['auth'] = 'auth';
$route['auth/(:any)'] = 'auth/$1';

// beer and breweries
$route['beer/submit'] = 'beer/submit';
$route['beer/delete_comment'] = 'beer/delete_comment';
$route['beer/do_delete_comment'] = 'beer/do_delete_comment';
$route['beer/(:any)'] = 'beer/info/$1';
$route['brewery/(:any)'] = 'brewery/info/$1';
$route['autocomplete/(:any)'] = 'autocomplete/$1';

// user-related
$route['settings'] = 'user/settings';
$route['settings/password'] = 'user/change_password';
$route['settings/disconnect/(:any)'] = 'user/disconnect/$1';
$route['settings/export'] = 'user/export';
$route['user/(:any)'] = 'user/$1';
$route['connect/(:any)'] = 'user/connect/$1';
$route['review/(:any)'] = 'review/$1';

// static pages
$route['contact'] = 'page/contact';
$route['privacy'] = 'page/privacy';
$route['tos'] = 'page/tos';
$route['support'] = 'page/donate';
$route['checkout'] = 'page/checkout';
$route['thanks'] = 'page/thanks';

// users
$route['(:any)/favorites'] = 'user/favorites/$1';
$route['(:any)'] = 'user/tab/$1';


/* End of file routes.php */
/* Location: ./application/config/routes.php */
