<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (ENVIRONMENT == 'production') {
  // Production key
	$config['brewerydb_api_url'] = 'https://api.brewerydb.com/v2';
	$config['brewerydb_api_key'] = '395579b8b034b2a87bdce2dee12e530b';
}
else {
  // Sandbox key
  $config['brewerydb_api_url'] = 'https://sandbox-api.brewerydb.com/v2/';
  $config['brewerydb_api_key'] = '4c8621eb3dbdb2f767ff45b7d497b376';
}