<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Noxssinput {

		protected $CI;

		public function __construct()
    {
      // Assign the CodeIgniter super-object
      $this->CI =& get_instance();
    }

    function post($index = '', $xss_clean = TRUE)
    {
        return $this->CI->input->post($index, $xss_clean);
    }
}
