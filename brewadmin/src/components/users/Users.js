var React = require('react');

var PageHeader = require('../shared/PageHeader');

var Users = React.createClass({
  render: function() {
    return (
      <div id="page-wrapper">
        <PageHeader headerText='Users' />
      </div>
    );
  }
});

module.exports = Users;