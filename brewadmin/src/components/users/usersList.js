var React = require('react'),
    Firebase = require('firebase');

var UsersList = React.createClass({
  getUserList: function() {
    var firebase = new Firebase('https://brwtk.firebaseio.com');
    var users = [];

    firebase.child('userProfile').on('child_added', function(snapshot) {
      var userList = snapshot.exportVal();
      console.log(userList);
    }, {users: users});
  },

  render: function() {
    return (
      <div>
        { this.getUserList() }
      </div>
    );
  }
});

module.exports = UsersList;