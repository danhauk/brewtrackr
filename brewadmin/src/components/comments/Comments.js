var React = require('react');

var PageHeader = require('../shared/PageHeader');

var Comments = React.createClass({
  render: function() {
    return (
      <div id="page-wrapper">
        <PageHeader headerText='Comments' />
      </div>
    );
  }
});

module.exports = Comments;