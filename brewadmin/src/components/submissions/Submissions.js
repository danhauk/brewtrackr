var React = require('react');

var PageHeader = require('../shared/PageHeader');

var Submissions = React.createClass({
  render: function() {
    return (
      <div id="page-wrapper">
        <PageHeader headerText='Beer Submissions' />
      </div>
    );
  }
});

module.exports = Submissions;