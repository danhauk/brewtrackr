var React = require('react'),
    Firebase = require('firebase');

var LoginForm = React.createClass({
  getInitialState: function() {
    return {
      email: '',
      password: '',
      authError: ''
    }
  },

  handleEmailChange: function(e) {
    this.setState({ email: e.target.value });
  },

  handlePasswordChange: function(e) {
    this.setState({ password: e.target.value });
  },

  handleSubmit: function(e) {
    e.preventDefault();

    firebase = new Firebase('https://brwtk.firebaseio.com');
    firebase.authWithPassword({
      email: this.state.email,
      password: this.state.password
    }, function(error, authData) {
      if (error) {
        this.setState({ authError: error.message });
      } else {
        this.props.handleSubmit(authData);
      }
    }.bind(this));
  },

  showLoginError: function(error) {
    if (this.state.authError) {
      return (
        <div className="alert alert-danger">
          { this.state.authError }
        </div>
      );
    }

    return;
  },

  render: function() {
    return (
      <div className="col-md-4 col-md-offset-4">
        <div className="login-panel panel panel-default">
          <div className="panel-heading">
            <h3 className="panel-title">Please Sign In</h3>
          </div>
          <div className="panel-body">
            { this.showLoginError() }
            <form role="form" onSubmit={ this.handleSubmit }>
              <fieldset>
                <div className="form-group">
                  <input className="form-control" placeholder="Email" name="email" type="email" onChange={ this.handleEmailChange } autofocus />
                </div>
                <div className="form-group">
                  <input className="form-control" placeholder="Password" name="password" type="password" onChange={ this.handlePasswordChange } />
                </div>
                <input type="submit" className="btn btn-lg btn-success btn-block" value="Login" />
              </fieldset>
            </form>
          </div>
        </div>
      </div>
    );
  }
});

module.exports = LoginForm;