var React = require('react'),
    moment = require('moment');

var DashboardNewUsersItem = React.createClass({
  render: function() {
    var signupDate = moment(this.props.createdAt).format('MMM D YYYY, h:mm:ss a');

    return (
      <tr>
        <td>
          <div className="small-2 columns">
            <img src={ this.props.photoUrl ? this.props.photoUrl : '' } className="avatar" />
          </div>
          <div className="small-10 columns">
            <div>{ this.props.username }</div>
            <span className="subtext">{ signupDate }</span>
          </div>
        </td>
      </tr>
    );
  }
});

module.exports = DashboardNewUsersItem;