var React = require('react'),
    Firebase = require('firebase');

var DashboardPanel = React.createClass({
  render: function() {
    return (
      <div className="large-3 medium-6 columns">
        <div className="stats-panel text-center">
          <span className={ this.props.icon }></span>
          <div className="stats-num">{ this.props.stat }</div>
          <div className="stats-text">{ this.props.text }</div>
        </div>
      </div>
    );
  }
});

module.exports = DashboardPanel;