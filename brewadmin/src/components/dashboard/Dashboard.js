var React = require('react'),
    Firebase = require('firebase');

var PageHeader = require('../shared/PageHeader'),
    DashboardPanel = require('./DashboardPanel'),
    NewestUsers = require('./DashboardNewUsers');

var Dashboard = React.createClass({
  getInitialState: function() {
    return {
      dashboardStats: '',
      newestUsers: ''
    }
  },

  componentWillMount: function() {
    this.getMainStats();
  },

  getMainStats: function() {
    this.stats = {
      userCount: 0,
      tabCount: 0,
      commentCount: 0,
      submissionCount: 0
    };

    this.firebase = new Firebase('https://brwtk.firebaseio.com');
    this.firebase.child('userProfile').on('child_added', function(snapshot) {
      this.stats.userCount++;
      this.setState({ dashboardStats: this.stats });
    }.bind(this));

    this.firebase.child('tab').on('child_added', function(snapshot) {
      this.stats.tabCount += snapshot.numChildren();
      this.setState({ dashboardStats: this.stats });
    }.bind(this));

    this.firebase.child('tab').on('child_added', function(snapshot) {
      this.stats.commentCount++;
      this.setState({ dashboardStats: this.stats });
    }.bind(this));

    this.firebase.child('beerSubmissions').on('child_added', function(snapshot) {
      this.stats.submissionCount++;
      this.setState({ dashboardStats: this.stats });
    }.bind(this));
  },

  render: function() {
    return (
      <div id="page-wrapper">
        <PageHeader headerText='Dashboard' />
        
        <div className="row row-fluid medium-collapse">
          <DashboardPanel key="users" icon="icon-users" text="Users" stat={ this.state.dashboardStats.userCount } />
          <DashboardPanel key="tab" icon="icon-book-open" text="Tab Items" stat={ this.state.dashboardStats.tabCount } />
          <DashboardPanel key="comments" icon="icon-speech" text="Comments" stat={ this.state.dashboardStats.commentCount } />
          <DashboardPanel key="submissions" icon="icon-paper-plane" text="Beer Submissions" stat={ this.state.dashboardStats.submissionCount } />
        </div>

        <div className="row" style={{marginTop: '30px'}}>
          <NewestUsers users={ this.state.newestUsers } />
        </div>
      </div>
    );
  }
});

module.exports = Dashboard;