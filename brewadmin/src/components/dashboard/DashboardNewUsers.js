var React = require('react'),
    Firebase = require('firebase');

var NewUserItem = require('./DashboardNewUsersItem');

var DashboardNewUsers = React.createClass({
  getInitialState: function() {
    return {
      newestUsers: ''
    }
  },

  getNewestUsers: function() {
    this.users = [];

    this.firebase = new Firebase('https://brwtk.firebaseio.com/userProfile');
    this.firebase
        .orderByChild('createdAt')
        .limitToLast(10)
        .on('child_added', function(snapshot) {
          this.users.unshift(snapshot.val());
          this.setState({ newestUsers: this.users });
        }.bind(this));
  },

  componentWillMount: function() {
    this.getNewestUsers();
  },

  getUserRows: function() {
    var users = this.state.newestUsers;

    var userList = users.map( function(user, i) {
      return (
        <NewUserItem
          key={user.i}
          createdAt={user.createdAt}
          photoUrl={user.photoUrl}
          username={user.username} />
      );
    });

    return userList;
  },

  render: function() {
    if (this.state.newestUsers.length > 9) {
      return (
        <div className="medium-6 columns">
          <div className="card">
            <table width="100%">
              <thead>
                <tr>
                  <th className="dashboard-module-heading">Newest Users</th>
                </tr>
              </thead>
              <tbody>
                { this.getUserRows() }
              </tbody>
            </table>
          </div>
        </div>
      );
    }

    return (
      <div className="medium-6 columns">
        <div className="card">
          { "this.getUserRows()" }
        </div>
      </div>
    );
  }
});

module.exports = DashboardNewUsers;