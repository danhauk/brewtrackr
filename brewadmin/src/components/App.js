var React = require('react'),
    ReactRedirect = require('react-redirect'),
    Firebase = require('firebase'),
    RouterMixin = require('react-mini-router').RouterMixin;;

var Loader = require('./shared/Loader'),
    Sidebar = require('./sidebar/Sidebar'),
    Dashboard = require('./dashboard/Dashboard'),
    Users = require('./users/Users'),
    Comments = require('./comments/Comments'),
    Submissions = require('./submissions/Submissions');

// var firebase = new Firebase('https://brwtk.firebaseio.com');

var App = React.createClass({
  
  mixins: [RouterMixin],

  getInitialState: function() {
    return {
      authData: '',
      userProfile: '',
      isLoading: true
    }
  },

  componentWillMount: function() {
    this.firebase = new Firebase('https://brwtk.firebaseio.com');
    this.authData = this.firebase.getAuth();

    if (this.authData) {
      this.setState({ authData: this.authData });
      this.setUserProfile(this.authData);
    } else {
      this.setState({ isLoading: false });
    }
  },

  setUserProfile: function(authData) {
    this.firebase = new Firebase('https://brwtk.firebaseio.com');

    this.firebase.child('userProfile/'+authData.uid).once('value', function(snapshot) {
      if (snapshot.val().isAdmin) {
        this.setState({ userProfile: snapshot.val() });
        this.setState({ isLoading: false });
      } else {
        console.log("Not admin");
      }
    }.bind(this));
  },

  routes: {
      '/': 'dashboard',
      '/dashboard': 'dashboard',
      '/users': 'users',
      '/comments': 'comments',
      '/submissions': 'submissions'
  },

  render: function() {
    if (this.state.isLoading) {
      return (
        <div className="app-loader">
          <Loader />
        </div>
      );
    }

    if (this.state.userProfile) {
      return (
        <div id="wrapper">
          <Sidebar userProfile={ this.state.userProfile } />
          { this.renderCurrentRoute() }
        </div>
      );
    }

    return (
      <ReactRedirect location='/'>
      </ReactRedirect>
    );
  },

  dashboard: function() {
    return <Dashboard userProfile={ this.state.userProfile } />;
  },

  users: function() {
    return <Users userProfile={ this.state.userProfile } />;
  },

  comments: function() {
    return <Comments userProfile={ this.state.userProfile } />;
  },

  submissions: function() {
    return <Submissions userProfile={ this.state.userProfile } />;
  }
});

module.exports = App;