var React = require('react');

var Loader = React.createClass({
  render: function() {
    return (
      <div id="circleG">
        <div id="circleG_1" className="circleG"></div>
        <div id="circleG_2" className="circleG"></div>
        <div id="circleG_3" className="circleG"></div>
      </div>
    );
  }
});

module.exports = Loader;