var React = require('react');

var PageHeader = React.createClass({
  render: function() {
    return (
      <div className="row">
        <div className="col-lg-12">
          <h1 className="page-header">{ this.props.headerText }</h1>
        </div>
      </div>
    );
  }
});

module.exports = PageHeader;