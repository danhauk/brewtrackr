var React = require('react');

var SidebarNav = require('./SidebarNav'),
    Greeting = require('./SidebarGreeting');

var Sidebar = React.createClass({
  render: function() {
    return (
      <div className="navbar-default sidebar" role="navigation">
        <Greeting userProfile={ this.props.userProfile } />
        <SidebarNav />
      </div>
    );
  }
});

module.exports = Sidebar;