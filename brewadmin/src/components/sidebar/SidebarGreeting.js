var React = require('react');

var SidebarGreeting = React.createClass({
  render: function() {
    var userProfile = this.props.userProfile;

    return (
      <div className="sidebar-greeting text-center">
        <h5 className="sidebar-greeting__heading">Hey, { userProfile.username }!</h5>
        <img src={ userProfile.photoUrl } className="avatar" />
      </div>
    );
  }
});

module.exports = SidebarGreeting;