var React = require('react');

var SidebarNav = React.createClass({
  getNavbarItems: function() {
    var items = [
      {
        key: 'dashboard',
        icon: 'speedometer',
        text: 'Dashboard'
      },
      {
        key: 'users',
        icon: 'users',
        text: 'Users'
      },
      {
        key: 'comments',
        icon: 'speech',
        text: 'Comments'
      },
      {
        key: 'submissions',
        icon: 'book-open',
        text: 'Beer Submissions'
      }
    ];

    var sidebar = items.map( function(item, i) {
      return (
        <SidebarNavItem key={item.key} location={item.key} icon={item.icon} text={item.text} />
      );
    });

    return sidebar;
  },

  render: function() {
    return (
      <div className="sidebar-nav navbar-collapse">
        <ul className="side-nav">
          { this.getNavbarItems() }
        </ul>
      </div>
    );
  }
});

var SidebarNavItem = React.createClass({
  render: function() {
    var iconClass = "icon icon-"+this.props.icon,
        location = "/admin/#!/"+this.props.location;

    return (
      <li>
        <a className={this.props.isActive ? "side-nav__link active" : "side-nav__link"} href={ location }>
          <span className={ iconClass }></span> { this.props.text }
        </a>
      </li>
    );
  }
});

module.exports = SidebarNav;