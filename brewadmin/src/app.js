var React = require('react'),
    ReactDOM = require('react-dom');

var App = require('./components/App');

ReactDOM.render(
  <App />,
  document.getElementById('admin')
);